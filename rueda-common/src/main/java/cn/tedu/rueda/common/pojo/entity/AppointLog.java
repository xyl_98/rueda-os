package cn.tedu.rueda.common.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ToString
@TableName("rueda_appoint")
public class AppointLog implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = -8855365723617417584L;
    /** 挂号记录ID */
    @ApiModelProperty(value = "挂号记录ID")
    private Long id;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    /** 挂号时间 */
    @ApiModelProperty(value = "挂号时间")
    private LocalDateTime appointTime;
    /** 创建者 */
    @ApiModelProperty(value = "创建者")
    private String createUser;
    /** 更新者 */
    @ApiModelProperty(value = "更新者")
    private String updateUser;
    /** 患者姓名 */
    @ApiModelProperty(value = "患者姓名")
    private String username;
    /** 身份证号 */
    @ApiModelProperty(value = "身份证号")
    private String userId;
    /** 联系电话 */
    @ApiModelProperty(value = "联系电话")
    private String cellPhone;
    /** 病史 */
    @ApiModelProperty(value = "病史")
    private String medicalHistory;
    /** 责任医生 */
    @ApiModelProperty(value = "责任医生")
    private String doctorName;
    /** 责任医生ID */
    @ApiModelProperty(value = "责任医生ID")
    private Long doctorId;
    /** 所在科室 */
    @ApiModelProperty(value = "所在科室")
    private String departmentName;
    /** 所在科室ID */
    @ApiModelProperty(value = "所在科室ID")
    private Long departmentId;
    /** 诊断记录ID */
    @ApiModelProperty(value = "诊断记录ID")
    private Long consultId;
    /** 支付状态 */
    @ApiModelProperty(value = "支付状态")
    private Integer payState;
    /** 状态码 */
    @ApiModelProperty(value = "状态码")
    private Integer state;

}