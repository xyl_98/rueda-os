package cn.tedu.rueda.common.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@ToString
@TableName("rueda_consult_result")
public class PatientConsultLog implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = 4542730545447327945L;
    /** 患者诊断结果ID */
    @ApiModelProperty(value = "患者ID")
    private Long id;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    /** 创建者 */
    @ApiModelProperty(value = "创建者")
    private String createUser;
    /** 更新者 */
    @ApiModelProperty(value = "更新者")
    private String updateUser;
    /** 诊断日期 */
    @ApiModelProperty(value = "诊断日期")
    private LocalDate date;
    /** 患者姓名 */
    @ApiModelProperty(value = "患者姓名")
    private String username;
    /** 患者身份证号 */
    @ApiModelProperty(value = "患者身份证号")
    private String userId;
    /** 责任医生ID */
    @ApiModelProperty(value = "责任医生ID")
    private Long doctorId;
    /** 责任医生 */
    @ApiModelProperty(value = "责任医生")
    private String doctorName;
    /** 所属科室ID */
    @ApiModelProperty(value = "所属科室ID")
    private Long departmentId;
    /** 所属科室 */
    @ApiModelProperty(value = "所属科室")
    private String departmentName;
    /** 诊断结果 */
    @ApiModelProperty(value = "诊断结果")
    private String consultResult;

}