/*
 * Copyright © 2023 Reda-register All Rights Reserved
 */
package cn.tedu.rueda.common.cache;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @ClassName: Cache
 * @Description: 本地缓存
 * @Author Wang
 * @Date 2023/8/5
 * @Version 1.0.0
 * @ModifiedBy:
 */
public class Cache {

    /** 自定义messages */
    public static Map<String, String> messageMap = new ConcurrentHashMap<>();

    /** Validated校验message信息properties文件名 */
    protected static final String VALID_MESSAGE = "validate-message.properties";

    /**
     * 从classpath路径下读取message信息加载到本地缓存
     * @param fileName 文件名
     * @return 读取到的信息
     * @throws IOException IO异常
     */
    protected Map<String, String> getMessageMap(String fileName) throws IOException {
        EncodedResource resource = new EncodedResource(
                new ClassPathResource(fileName), StandardCharsets.UTF_8);
        Properties props = PropertiesLoaderUtils.loadProperties(resource);
        List<?> codes = Collections.list(props.propertyNames());
        Map<String, String> messageMap = codes.stream()
                .collect(Collectors.toMap(k -> k.toString(), v -> props.getProperty(v.toString())));
        return messageMap;
    }

}
