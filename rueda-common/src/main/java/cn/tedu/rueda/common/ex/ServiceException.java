package cn.tedu.rueda.common.ex;


import cn.tedu.rueda.common.enumerator.ServiceCode;

/**
 * 自定义异常类，用于Service层抛出，有全局异常处理器捕获处理
 **/
public class ServiceException extends RuntimeException{

    private final ServiceCode serviceCode;

    private Object data;

    public ServiceException(ServiceCode serviceCode, String message) {
        this(serviceCode, message, null);
    }

    public ServiceException(ServiceCode serviceCode, String message, Object data) {
        super(message);
        this.serviceCode = serviceCode;
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ServiceCode getServiceCode() {
        return serviceCode;
    }
}
