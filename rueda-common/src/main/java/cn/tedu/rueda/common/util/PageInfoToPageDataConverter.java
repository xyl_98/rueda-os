package cn.tedu.rueda.common.util;

import cn.tedu.rueda.common.pojo.vo.PageData;
import com.github.pagehelper.PageInfo;

public class PageInfoToPageDataConverter {

    public static <T> PageData<T> convert(PageInfo<T> pageInfo) {
        PageData<T> pageData = new PageData<>();
        return pageData.setList(pageInfo.getList())
                .setPageNum(pageInfo.getPageNum())
                .setPageSize(pageInfo.getPageSize())
                .setTotal(pageInfo.getTotal())
                .setMaxPage(pageInfo.getPages());
    }
}
