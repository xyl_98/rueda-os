package cn.tedu.rueda.common.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@TableName("rueda_account_role")
public class AccountUserRole implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = 4378497831162670193L;
    /** 用户角色ID */
    @ApiModelProperty(value = "用户角色ID")
    private Long id;
    /** 用户ID */
    @ApiModelProperty(value = "用户ID")
    private Long accountId;
    /** 角色ID */
    @ApiModelProperty(value = "角色ID")
    private Long roleId;

}