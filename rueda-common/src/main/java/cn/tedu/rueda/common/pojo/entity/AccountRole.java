package cn.tedu.rueda.common.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ToString
@TableName("rueda_role")
public class AccountRole implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = 8430169755217986042L;
    /** 角色ID */
    @ApiModelProperty(value = "角色ID")
    private Long id;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    /** 角色名 */
    @ApiModelProperty(value = "角色名")
    private String name;
    /** 角色说明 */
    @ApiModelProperty(value = "角色说明")
    private String description;
    /** 创建者 */
    @ApiModelProperty(value = "创建者")
    private String createUser;
    /** 更新者 */
    @ApiModelProperty(value = "更新者")
    private String updateUser;
    /** 状态码 */
    @ApiModelProperty(value = "状态码")
    private Integer state;

}