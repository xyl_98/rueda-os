package cn.tedu.rueda.common.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ToString
@TableName("rueda_department")
public class DepartmentInfo implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = 8634955129207995732L;
    /** 科室ID */
    @ApiModelProperty(value = "科室ID")
    private Long id;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    /** 创建者 */
    @ApiModelProperty(value = "创建者")
    private String createUser;
    /** 更新者 */
    @ApiModelProperty(value = "更新者")
    private String updateUser;
    /** 科室名 */
    @ApiModelProperty(value = "科室名")
    private String name;
    /** 父级科室ID */
    @ApiModelProperty(value = "父级科室ID")
    private Long parentId;
    /** 层级 */
    @ApiModelProperty(value = "层级")
    private Integer level;
    /** 是否为父级科室 */
    @ApiModelProperty(value = "是否为父级科室")
    private Integer isParent;
    /** 科室说明 */
    @ApiModelProperty(value = "科室说明")
    private String description;
    /** 状态码 */
    @ApiModelProperty(value = "状态码")
    private Integer state;

}