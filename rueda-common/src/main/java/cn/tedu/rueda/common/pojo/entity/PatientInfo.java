package cn.tedu.rueda.common.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ToString
@TableName("rueda_patient")
public class PatientInfo implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = 113496401090301036L;
    /** 患者ID */
    @ApiModelProperty(value = "患者ID")
    private Long id;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    /** 创建者 */
    @ApiModelProperty(value = "创建者")
    private String createUser;
    /** 更新者 */
    @ApiModelProperty(value = "更新者")
    private String updateUser;
    /** 患者姓名 */
    @ApiModelProperty(value = "患者姓名")
    private String name;
    /** 患者年龄 */
    @ApiModelProperty(value = "患者年龄")
    private Integer age;
    /** 患者性别 */
    @ApiModelProperty(value = "患者性别")
    private Integer gender;
    /** 联系电话 */
    @ApiModelProperty(value = "联系电话")
    private String cellPhone;
    /** 诊断结果 */
    @ApiModelProperty(value = "诊断结果")
    private String consultResult;
    /** 情况说明 */
    @ApiModelProperty(value = "情况说明")
    private String description;
    /** 身份证号 */
    @ApiModelProperty(value = "身份证号")
    private String userId;
    /** 患者用户ID */
    @ApiModelProperty(value = "患者用户ID")
    private Long accountId;
    /** 所属科室ID */
    @ApiModelProperty(value = "所属科室ID")
    private Long departmentId;
    /** 所属科室 */
    @ApiModelProperty(value = "所属科室")
    private String departmentName;
    /** 病史 */
    @ApiModelProperty(value = "病史")
    private String medicalHistory;

}