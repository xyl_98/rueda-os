/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.common.util;

import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @class: ToolUtils
 * @desc: 工具类
 * @author wang-p
 * @date 2023/8/15
 * @version 1.0.0
 * @modify:
 */
public class ToolUtils {

    public static final String COMMA = ",";

    /**
     * 分割字符串返回List，使用默认的','来分割字符串
     * 同时会自动去除字符串中的空白字符
     * @param str 待分割字符串
     * @return 分割后的List对象
     */
    public static List<String> split(String str) {
        return split(str, COMMA, false);
    }

    /**
     * 分割字符串返回List，若分割符(separator)传入值为null、则默认使用','分割
     * 在不使用空白字符作为分隔符的情况下、会自动去除字符串中的空白字符
     * @param str 待分割字符串
     * @param separator 分割符
     * @return 分割后的List对象
     */
    public static List<String> split(String str, String separator) {
        return split(str, separator, false);
    }

    /**
     * 分割字符串返回List，若分割符(separator)传入值为null、则默认使用','分割
     * 若使用了空白字符作为分隔符的情况下、不会去除字符串中的空白字符
     * @param str 待分割字符串
     * @param separator 分割符
     * @param keepWhitespace 是否保留空白字符
     * @return 分割后的List对象
     */
    public static List<String> split(String str, String separator, boolean keepWhitespace) {
        List<String> list = new ArrayList<>();
        if (!StringUtils.hasText(str))
            return list;
        // 默认','分割
        if (separator == null)
            separator = COMMA;
        // 校验分割符是否包含空白字符
        boolean hasWhitespace = false;
        for (char c : separator.toCharArray()) {
            if (Character.isWhitespace(c))
                hasWhitespace = true;
            break;
        }
        // 去除字符串包含的所有单字节空格
        if (!keepWhitespace && !hasWhitespace)
            str = StringUtils.trimAllWhitespace(str);
        int i = 0;
        int len = str.length();
        int current = 0;
        boolean match = false;
        // 单字符分割
        if (separator.length() == 1) {
            char sep = separator.charAt(0);
            while (i < len) {
                if (str.charAt(i) == sep) {
                    if (match)
                        list.add(str.substring(current, i));
                    i++;
                    current = i;
                    match = false;
                } else {
                    match = true;
                    i++;
                }
            }
        } else {
            // 多字符分割
            while (i < len) {
                int target = str.indexOf(separator, i);
                // 最后一段字符串匹配
                if(target == -1)
                    target = len;
                // 截取目标字符串
                list.add(str.substring(current, target));
                // next
                i = target + separator.length();
                current = i;
            }
        }
        // 将最后一段字符串放入list
        if (match)
            list.add(str.substring(current, i));

        return list;
    }

}
