/*
 * Copyright © 2023 Reda-register All Rights Reserved
 */
package cn.tedu.rueda.common.constant;

/**
 * @ClassName: MessageCode
 * @Description: Message Code
 * @Author Wang
 * @Date 2023/8/5
 * @Version 1.0.0
 * @ModifiedBy:
 */
public class MessageCode {

    /** 私有构造方法、禁止常量类实例化 */
    private MessageCode() {
    }

    /** 未查找到医生信息 */
    public static final String DOCTOR_INFO_NOT_EXISTS = "DOCTOR_I001";
    /** 医生信息插入失败 */
    public static final String DOCTOR_INSERT_FAIL = "DOCTOR_W001";
    /** 已恢复指定的医生信息 */
    public static final String DOCTOR_INFO_RESTORE= "DOCTOR_W002";
    /** 已存在指定的医生信息 */
    public static final String DOCTOR_INFO_EXISTS = "DOCTOR_W003";
    /** 医生用户信息同步失败 */
    public static final String DOCTOR_ACCOUNT_SYNC_FAIL = "DOCTOR_W004";
    /** 医生用户信息同步更新失败 */
    public static final String DOCTOR_ACCOUNT_SYNC_UPDATE_FAIL = "DOCTOR_W005";
    /** 排班信息插入失败 */
    public static final String SCHEDULE_INSERT_FAIL = "SCHEDULE_W001";
    /** 排班信息更新失败 */
    public static final String SCHEDULE_UPDATE_FAIL = "SCHEDULE_W002";
    /** 无效的更新内容 */
    public static final String SCHEDULE_UPDATE_INVALID = "SCHEDULE_W003";
    /** 存在未登录的排班信息 */
    public static final String SCHEDULE_UNKNOWN_RECORD = "SCHEDULE_W004";
    /** 不允许重复的排班信息 */
    public static final String SCHEDULE_NOT_REPEAT = "SCHEDULE_W005";
    /** 排班结束时间必须在排班开始时间之后 */
    public static final String SCHEDULE_START_AFTER_END = "SCHEDULE_W006";
    /** 未检索到排班信息 */
    public static final String SCHEDULE_NOT_FOUND = "SCHEDULE_I001";
    /** 无待新增的排班信息 */
    public static final String SCHEDULE_NOT_NEW = "SCHEDULE_I002";



}
