package cn.tedu.rueda.common.pojo.po;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

@Data
public class UserStatePO implements Serializable {

//    private String authorities;
      private List<String> authorities;
}
