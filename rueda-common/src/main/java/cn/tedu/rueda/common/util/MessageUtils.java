/*
 * Copyright © 2023 Reda-register All Rights Reserved
 */
package cn.tedu.rueda.common.util;

import cn.tedu.rueda.common.cache.Cache;
import org.springframework.util.StringUtils;

/**
 * @ClassName: MessageUtils
 * @Description: message相关处理
 * @Author Wang
 * @Date 2023/8/5
 * @Version 1.0.0
 * @ModifiedBy:
 */
public class MessageUtils {

    /** 私有构造方法、禁止工具类实例化 */
    private MessageUtils() {
    }

    /**
     * 根据messageCode获取message信息
     * @param messageCode MessageCode
     * @return message内容
     */
    public static String getMessage(String messageCode) {
        return Cache.messageMap.get(messageCode);
    }

    /**
     * 根据messageCode与填充值获取对应message信息
     * @param messageCode MessageCodes
     * @param formats format参数值
     */
    public static String getMessage(String messageCode, Object... formats) {
        String message = Cache.messageMap.get(messageCode);
        if (!StringUtils.hasText(message))
            return messageCode;
        return String.format(message, formats);
    }

}
