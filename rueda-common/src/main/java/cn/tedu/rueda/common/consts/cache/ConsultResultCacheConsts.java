package cn.tedu.rueda.common.consts.cache;

public interface ConsultResultCacheConsts {


    String KEY_CONSULT_RESULT_LIST_ACCOUNT = "consult-result:list:account:";

    String KEY_CONSULT_RESULT_LIST_PATIENT = "consult-result:list:patient:";

    String KEY_CONSULT_RESULT="consult-result:detail:";

    String KEY_ALL_KEYS="consult-result:keys";
}
