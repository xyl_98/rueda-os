package cn.tedu.rueda.common.pojo.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ToString
@Accessors(chain = true)
@TableName("rueda_account")
public class AccountUser implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = -5225189643317381131L;
    /** 用户ID */
    @ApiModelProperty(value = "用户ID")
    private Long id;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime updateTime;
    /** 创建者 */
    @ApiModelProperty(value = "创建者")
    private String createUser;
    /** 更新者 */
    @ApiModelProperty(value = "更新者")
    private String updateUser;
    /** 姓名 */
    @ApiModelProperty(value = "姓名")
    private String username;
    /** 电话号码 */
    @ApiModelProperty(value = "电话号码")
    private String cellPhone;
    /** 密码 */
    @ApiModelProperty(value = "密码")
    private String password;
    /** 昵称 */
    @ApiModelProperty(value = "昵称")
    private String nick;
    /** 身份证号 */
    @ApiModelProperty(value = "身份证号")
    private String userId;
    /** 角色ID */
    @ApiModelProperty(value = "角色ID")
    private Integer roleId;
    /** 权限ID */
    @ApiModelProperty(value = "权限ID")
    private Integer authId;
    /** 头像 */
    @ApiModelProperty(value = "头像")
    private String sculpture;
    /** 性别 */
    @ApiModelProperty(value = "性别")
    private Integer gender;
    /** 年龄 */
    @ApiModelProperty(value = "年龄")
    private Integer age;
    /** 状态码 */
    @ApiModelProperty(value = "状态码")
    private Integer state;

}