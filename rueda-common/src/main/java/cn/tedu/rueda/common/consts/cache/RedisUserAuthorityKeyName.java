package cn.tedu.rueda.common.consts.cache;

public interface RedisUserAuthorityKeyName {

    String KEY_PREFIX_AUTHORITY = "user:authority:";
}
