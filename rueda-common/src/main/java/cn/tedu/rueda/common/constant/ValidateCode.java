/*
 * Copyright © 2023 Reda-register All Rights Reserved
 */
package cn.tedu.rueda.common.constant;

import org.springframework.util.StringUtils;

/**
 * @ClassName: ValidateCode
 * @Description: Valid校验用Message Code
 * @Author Wang
 * @Date 2023/8/5
 * @Version 1.0.0
 * @ModifiedBy:
 */
public class ValidateCode {

    /** 私有构造方法、禁止常量类实例化 */
    private ValidateCode() {
    }

    /** 该字段值不能为NULL */
    public static final String VALID_NOT_NULL = "VALID_E001";
    /** 该字段值不能为NULL或空字符串("") */
    public static final String VALID_NOT_EMPTY = "VALID_E002";
    /** 身份证号格式不正确 */
    public static final String VALID_NOT_MATCH_USERID = "VALID_E003";
    /** 电话号码格式不正确 */
    public static final String VALID_NOT_MATCH_PHONE = "VALID_E004";
    /** 姓名长度错误 */
    public static final String VALID_NOT_MATCH_NAME = "VALID_E005";
    /** 长度不能超过200 */
    public static final String VALID_ZERO_TO_TWO_H = "VALID_E006";
    /** 长度不能超过1000 */
    public static final String VALID_ZERO_TO_THOUSAND = "VALID_E007";
    /** 该字段值必须大于0 */
    public static final String VALID_GREATER_ZERO = "VALID_E008";
    /** 该字段值必须为0或1 */
    public static final String VALID_MUST_ZERO_OR_ONE = "VALID_E009";
    /** 该字段值必须为0、1或2 */
    public static final String VALID_MUST_ZERO_ONE_TWO = "VALID_E010";
    /** 该字段值必须为[yyyy-MM-dd HH]格式的时间 */
    public static final String VALID_SCHEDULE_TIME_FORMAT = "VALID_E011";
    /** 该字段值必须为[yyyy-MM-dd]格式的时间 */
    public static final String VALID_SEARCH_TIME_FORMAT = "VALID_E012";
    /** 该字段值必须为1或2 */
    public static final String VALID_MUST_ONE_OR_TWO = "VALID_E013";
    /** 该字段值必须数字 */
    public static final String VALID_MUST_NUMBER = "VALID_E014";


}
