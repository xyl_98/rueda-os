package cn.tedu.rueda.common.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ToString
@TableName("rueda_consult")
public class ConsultInfo implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = 5011362318089427072L;
    /** 诊室ID */
    @ApiModelProperty(value = "诊室ID")
    private Long id;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    /** 创建者 */
    @ApiModelProperty(value = "创建者")
    private String createUser;
    /** 更新者 */
    @ApiModelProperty(value = "更新者")
    private String updateUser;
    /** 科室ID */
    @ApiModelProperty(value = "科室ID")
    private Long departmentId;
    /** 诊室位置 */
    @ApiModelProperty(value = "诊室位置")
    private String address;
    /** 诊室名称 */
    @ApiModelProperty(value = "诊室名称")
    private String name;

}