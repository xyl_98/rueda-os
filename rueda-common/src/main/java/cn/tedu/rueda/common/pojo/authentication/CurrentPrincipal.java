package cn.tedu.rueda.common.pojo.authentication;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class CurrentPrincipal implements Serializable {

    private Long id;
    private String username;
    private String nick;
}
