package cn.tedu.rueda.common.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
@TableName("rueda_role_authority")
public class AccountRoleAuthority implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = 3965430790248874440L;
    /** 角色权限ID */
    @ApiModelProperty(value = "角色权限ID")
    private Long id;
    /** 角色权限ID */
    @ApiModelProperty(value = "角色ID")
    private Long roleId;
    /** 角色权限ID */
    @ApiModelProperty(value = "权限ID")
    private Long authorityId;

}