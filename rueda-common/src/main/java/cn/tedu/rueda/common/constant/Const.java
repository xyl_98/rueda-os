/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.common.constant;

import java.time.format.DateTimeFormatter;

/**
 * @class: Const
 * @desc: 
 * @author wang-p
 * @date 2023/8/12
 * @version 1.0.0
 * @modify: 
 */
public class Const {

    /** 私有构造方法、禁止常量类实例化 */
    private Const(){}

    /** 正则表达式：匹配身份证号（不完全严格） */
    public static final String PATTERN_USERID = "^[1-6][0-7]\\d{4}[1-9]\\d{3}[0-1][0-9][0-3][0-9]\\d{3}([0-9]|X|x)$";
    /** 正则表达式：匹配手机号码（不完全严格） */
    public static final String PATTERN_CELLPHONE = "^1[3-9]\\d{9}$";
    /** 正则表达式：匹配yyyy-MM-dd HH时间格式 */
    public static final String PATTERN_SCHEDULE_TIME_FORMAT = "^\\d{4}-\\d{2}-\\d{2} \\d{2}$";
    /** 正则表达式：匹配yyyy-MM-dd时间格式 */
    public static final String PATTERN_SEARCH_TIME_FORMAT = "^\\d{4}-\\d{2}-\\d{2}$";
    /** 正则表达式：匹配数字 */
    public static final String PATTERN_NUMBER = "^\\d+$";
    /** 默认值：(Integer) 0 */
    public static final Integer DEFAULT_ZERO = 0;
    /** Flag：true */
    public static final Integer FLAG_TRUE = 1;
    /** Flag：false */
    public static final Integer FLAG_FALSE = 0;
    /** 默认值USERNAME：system */
    public static final String DEFAULT_USER = "system";
    /** 默认时间格式 */
    public static final String DEFAULT_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    /** 排班时间格式 */
    public static final String SCHEDULE_TIME_FORMAT = "yyyy-MM-dd HH";
    /** 检索时间格式 */
    public static final String SEARCH_TIME_FORMAT = "yyyy-MM-dd";
    /** 时间格式formatter（排班时间格式） */
    public static final DateTimeFormatter SCHEDULE_FORMAT = DateTimeFormatter.ofPattern(SCHEDULE_TIME_FORMAT);
    /** 时间格式formatter（检索时间格式） */
    public static final DateTimeFormatter SEARCH_FORMAT = DateTimeFormatter.ofPattern(SEARCH_TIME_FORMAT);


}
