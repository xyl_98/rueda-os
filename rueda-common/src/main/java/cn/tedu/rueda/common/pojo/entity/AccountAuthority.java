package cn.tedu.rueda.common.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ToString
@TableName("rueda_authority")
public class AccountAuthority implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = -912661251080381804L;
    /** 权限ID */
    @ApiModelProperty(value = "权限ID")
    private Long id;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    /** 创建者 */
    @ApiModelProperty(value = "创建者")
    private String createUser;
    /** 更新者 */
    @ApiModelProperty(value = "更新者")
    private String updateUser;
    /** 权限名 */
    @ApiModelProperty(value = "权限名")
    private String name;
    /** 状态码 */
    @ApiModelProperty(value = "状态码")
    private Integer state;

}