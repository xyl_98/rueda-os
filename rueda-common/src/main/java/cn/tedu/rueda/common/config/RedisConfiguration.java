package cn.tedu.rueda.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

@Configuration
public class RedisConfiguration {

    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory){

        RedisTemplate<String,Object> redisTemplate = new RedisTemplate<>();

        //设置RedisTemplate的连接工厂，用于获取与Redis服务器的连接;也可以配置文件里配置
        redisTemplate.setConnectionFactory(redisConnectionFactory);

        //设置RedisTemplate的键序列化器，用于将键对象序列化为字节数组存储到Redis服务器中
        redisTemplate.setKeySerializer(RedisSerializer.string());

        //设置RedisTemplate的值序列化器，用于将值对象序列化为JSON格式的字符串存储到Redis服务器中
        redisTemplate.setValueSerializer(RedisSerializer.json());

        // 设置RedisTemplate的哈希键序列化器，用于将哈希键对象序列化为字节数组存储到Redis服务器中。
        redisTemplate.setHashKeySerializer(RedisSerializer.string());

        // 设置RedisTemplate的哈希值序列化器，用于将哈希值对象序列化为JSON格式的字符串存储到Redis服务器中。
        redisTemplate.setHashValueSerializer(RedisSerializer.json());

        //设置RedisTemplate的默认序列化器，用于将未指定键或哈希值的对象序列化为JSON格式的字符串存储到Redis服务器中。
//        redisTemplate.setDefaultSerializer(RedisSerializer.json());

        //启用事务支持，允许在RedisTemplate中执行事务操作。
//        redisTemplate.setEnableTransactionSupport(true);

        //启用默认序列化器，当未指定键或哈希值的序列化器时，将使用默认序列化器进行序列化。
//        redisTemplate.setEnableDefaultSerializer(true);

        //设置是否暴露底层Redis连接，可以通过`redisTemplate.getConnection()`方法获取底层连接对象。
//        redisTemplate.setExposeConnection(true);

        //在所有配置完成后，调用此方法以初始化RedisTemplate对象
//        redisTemplate.afterPropertiesSet();

        return redisTemplate;
    }

}
