package cn.tedu.rueda.common.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ToString
@TableName("rueda_schedule")
public class DoctorSchedule implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = -3591901163379402023L;
    /** 排班ID */
    @ApiModelProperty(value = "排班ID")
    private Long id;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    /** 创建者 */
    @ApiModelProperty(value = "创建者")
    private String createUser;
    /** 更新者 */
    @ApiModelProperty(value = "更新者")
    private String updateUser;
    /** 医生用户ID */
    @ApiModelProperty(value = "医生用户ID")
    private Long doctorAccountId;
    /** 工作日 */
    @ApiModelProperty(value = "工作日")
    private Integer scheduleId;
    /** 是否在班 */
    @ApiModelProperty(value = "是否在班")
    private Integer isLeave;
    /** 审核状态 */
    @ApiModelProperty(value = "审核状态")
    private Integer checkState;
    /** 审核者 */
    @ApiModelProperty(value = "审核者")
    private String checkName;
    /** 审核时间 */
    @ApiModelProperty(value = "审核时间")
    private LocalDateTime checkTime;
    /** 审核说明 */
    @ApiModelProperty(value = "审核说明")
    private String checkDescription;
    /** 开始时间 */
    @ApiModelProperty(value = "排班开始时间")
    private LocalDateTime startTime;
    /** 结束时间 */
    @ApiModelProperty(value = "排班结束时间")
    private LocalDateTime endTime;

}