package cn.tedu.rueda.common.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ToString
@TableName("rueda_doctor")
public class DoctorInfo implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = -6236970129254100470L;
    /** 医生ID */
    @ApiModelProperty(value = "医生ID")
    private Long id;
    /** 用户ID */
    @ApiModelProperty(value = "用户ID")
    private Long accountId;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    private LocalDateTime updateTime;
    /** 医生姓名 */
    @ApiModelProperty(value = "医生姓名")
    private String name;
    /** 所属科室ID */
    @ApiModelProperty(value = "所属科室ID")
    private Long departmentId;
    /** 所属诊室ID */
    @ApiModelProperty(value = "所属诊室ID")
    private Long consultId;
    /** 擅长疾病 */
    @ApiModelProperty(value = "擅长疾病")
    private String keywords;
    /** 医生说明 */
    @ApiModelProperty(value = "医生说明")
    private String description;
    /** 状态码 */
    @ApiModelProperty(value = "状态码")
    private Integer state;

}