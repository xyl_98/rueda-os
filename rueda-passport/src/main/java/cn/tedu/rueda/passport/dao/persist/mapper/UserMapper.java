package cn.tedu.rueda.passport.dao.persist.mapper;

import cn.tedu.rueda.passport.pojo.entity.Account;

import cn.tedu.rueda.passport.pojo.entity.AccountRole;
import cn.tedu.rueda.passport.pojo.param.UpdatePasswordParam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<Account> {

    int insertById(AccountRole accountRole);

    int updatePasswordById(Account account);

    String selectPasswordSuccess(Long id);
}
