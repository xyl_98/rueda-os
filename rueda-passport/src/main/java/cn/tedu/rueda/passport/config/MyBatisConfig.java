package cn.tedu.rueda.passport.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("cn.tedu.rueda.passport.dao.persist.mapper")
public class MyBatisConfig {
}
