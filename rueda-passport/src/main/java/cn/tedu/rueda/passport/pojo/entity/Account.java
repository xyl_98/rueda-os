package cn.tedu.rueda.passport.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;


/**
 * 用户表 --实体类
 */
@Data
@TableName("rueda_account")
public class Account {

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 姓名
     */
    private String username;

    /**
     * 电话号码
     */
    private String cellPhone;

    /**
     * 密码
     */
    private String password;

    /**
     * 昵称
     */
    private String nick;

    /**
     * 身份证
     */
    private String userId;

    /**
     * 角色
     * 普通用户 ：   1
     * 管理员用户：  2
     * 超级管理员：  3
     */
    private Long roleId;

    /**
     * 权限码
     * 普通用户 ：   1
     * 管理员用户：  2
     * 超级管理员：  3
     */
    private Long authId;

    /**
     * 头像
     */
    private String sculpture;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 状态
     * 删除：  0
     * 未删除：1
     */
    private Integer state;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 修改人
     */
    private String updateUser;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
