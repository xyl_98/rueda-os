package cn.tedu.rueda.passport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RuedaPassportApplication {

    public static void main(String[] args) {
        SpringApplication.run(RuedaPassportApplication.class, args);

    }

}
