package cn.tedu.rueda.passport.dao.persist.repository.impl;

import cn.tedu.rueda.passport.dao.persist.mapper.UserLoginMapper;
import cn.tedu.rueda.passport.dao.persist.repository.IUserLoginRepository;
import cn.tedu.rueda.passport.pojo.vo.UserInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserLoginRepositoryImpl implements IUserLoginRepository {
    @Autowired
    private UserLoginMapper mapper;

    /**
     * 查询用户的信息
     * @param phone 根据登录的手机号
     * @return UserLoginInfoVO
     */
    @Override
    public UserInfoVO getUserInfoByPhone(String phone) {
        return mapper.getUserInfoAndPermissionsByPhone(phone);
    }
}
