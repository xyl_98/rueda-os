package cn.tedu.rueda.passport.service.impl;

import cn.tedu.rueda.common.enumerator.ServiceCode;
import cn.tedu.rueda.common.ex.ServiceException;
import cn.tedu.rueda.passport.dao.persist.repository.UserRepository;
import cn.tedu.rueda.passport.pojo.entity.Account;
import cn.tedu.rueda.passport.pojo.entity.AccountRole;
import cn.tedu.rueda.passport.pojo.param.AdminRegParam;
import cn.tedu.rueda.passport.pojo.param.PatientRegParam;
import cn.tedu.rueda.passport.pojo.param.UpdatePasswordParam;
import cn.tedu.rueda.passport.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class UserServiceImpl implements UserService {

    public UserServiceImpl(){
        log.debug("创建业务类对象：PatientServiceImpl");
    }

    private Long ROLE_ID = 1L;

    @Autowired
    private UserRepository repository;

    @Autowired
    PasswordEncoder encoder;

    @Override
    public void PatientReg(PatientRegParam patientRegParam) {
//        if (patientRegParam.getCellPhone().length()!=11){
//            String message = "请输入正确的手机号！！！";
//            log.warn(message);
//            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST,message);
//        }
//
//        if (patientRegParam.getUserId().length()!=18){
//            String message = "请输入正确的身份证号！！！";
//            log.warn(message);
//            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST,message);
//        }

        int row = repository.select(patientRegParam.getCellPhone());

        if (row!=0){
            String message = "用户已注册！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ALREADY_EXIST,message);
        }

        Account account = new Account();
        BeanUtils.copyProperties(patientRegParam,account);
        account.setPassword(encoder.encode(account.getPassword()));
        account.setCreateUser(patientRegParam.getUsername());
        account.setUpdateUser(patientRegParam.getUsername());
        int rows = repository.insert(account);
        if (rows!=1){
            String message = "注册失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }

        AccountRole role = new AccountRole();
        role.setRoleId(ROLE_ID);
        role.setAccountId(account.getId());
        int i = repository.insertById(role);
        if (i!=1){
            String message = "注册失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }
    }


    @Override
    public void AdminReg(AdminRegParam adminRegParam ,String username) {
        System.out.println("adminRegParam = " + adminRegParam + ", username = " + username);

//        if (adminRegParam.getCellPhone().length()!=11){
//            String message = "请输入正确的手机号！！！";
//            log.warn(message);
//            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST,message);
//        }
//
//        if (adminRegParam.getUserId().length()!=18){
//            String message = "请输入正确的身份证号！！！";
//            log.warn(message);
//            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST,message);
//        }

        int row = repository.select(adminRegParam.getCellPhone());

        if (row!=0){
            String message = "用户已注册！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ALREADY_EXIST,message);
        }

        Account account = new Account();
        BeanUtils.copyProperties(adminRegParam,account);

        //设置密码 ： 手机号后四位+身份证后四位
        String cellPhone = account.getCellPhone();
        String phonePassword = cellPhone.substring(cellPhone.length() - 4);
        String userId = account.getUserId();
        String userIdPassword = userId.substring(userId.length() - 4);
        account.setPassword(encoder.encode(phonePassword+userIdPassword));

        account.setCreateUser(username);
        account.setUpdateUser(username);
        int rows = repository.insert(account);
        if (rows!=1){
            String message = "注册失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }
        AccountRole role = new AccountRole();
        role.setRoleId(account.getRoleId());
        role.setAccountId(account.getId());
        int i = repository.insertById(role);
        if (i!=1){
            String message = "注册失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }
    }

    @Override
    public void updatePassword(Long userId ,UpdatePasswordParam updatePasswordParam) {
        System.out.println("userId = " + userId + ", updatePasswordParam = " + updatePasswordParam);
        if (userId == null || userId == 0 ) {
            String message = "登录已过期，请重新登录!!!";
            throw new ServiceException(ServiceCode.ERR_JWT_EXPIRED,message);
        }
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String oldPassword = repository.selectPasswordSuccess(userId);
        if (!encoder.matches(updatePasswordParam.getPassword(),oldPassword)){
            String message = "旧密码输入错误，请重新输入！！！";
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,message);
        }
        if (!updatePasswordParam.getNewPassword().equals(updatePasswordParam.getSecondPassword())){
            String message = "两次密码不同，请重新输入！！！";
            throw new ServiceException(ServiceCode.ERROR_UPDATE,message);
        }
        Account account = new Account();
        account.setId(userId);
        account.setPassword(encoder.encode(updatePasswordParam.getNewPassword()));
        int answer = repository.updatePasswordById(account);
        if (answer!=1){
            String message = "修改密码失败，请稍后再尝试！";
            throw new ServiceException(ServiceCode.ERROR_INSERT,message);
        }

    }


}
