package cn.tedu.rueda.passport.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 登录响应用户信息
 */
@Data
@Accessors(chain = true)
public class UserLoginResult implements Serializable {

    /*
    用户id
     */
    private Long id;

    /*
    用户手机号
     */
    private String cellPhone;

    /*
    用户用户名
     */
    private String username;

    /*
    用户昵称
     */
    private String nick;

    /*
    用户头像
     */
    private String sculpture;

    /*
    用户年龄
     */
    private Integer age;

    /*
    用户性别码
     */
    private Integer gender;

    /*
    用户的jwt
     */
    private String token;

    /*
    用户权限
     */
//    private String[] authorities;
    private List<String> authorities;
}
