package cn.tedu.rueda.passport.service;

import cn.tedu.rueda.passport.pojo.param.UserLoginParam;
import cn.tedu.rueda.passport.pojo.param.UserLoginSmsCodeParam;
import cn.tedu.rueda.passport.pojo.vo.UserLoginResult;

public interface IUserLoginService {

    /**
     * 处理密码登录
     * @param loginParam 接收的登录请求对象
     * @return 返回UserLoginResult封装的用户信息
     */
    UserLoginResult userLogin(UserLoginParam loginParam);

    /**
     * 处理短信验证码登录
     * @param loginSmsCodeParam 接收的登录请求对象
     * @return 返回UserLoginResult封装的用户信息
     */
    UserLoginResult userLogin(UserLoginSmsCodeParam loginSmsCodeParam);

    /**
     * 退出功能
     * @param id 退出用户的id
     */
    void userLogout(Long id);

    void verificationCode(String phone) ;
}
