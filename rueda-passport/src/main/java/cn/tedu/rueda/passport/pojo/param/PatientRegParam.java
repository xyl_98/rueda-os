package cn.tedu.rueda.passport.pojo.param;

import cn.tedu.rueda.common.constant.Const;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import javax.validation.constraints.Pattern;
import java.util.Date;


/**
 * 所需注册类 信息
 */
@Data
@TableName("rueda_account")
public class PatientRegParam {

    /**
     * 姓名
     */
    private String username;

    /**
     * 电话号码
     */
    @Pattern(regexp =Const.PATTERN_CELLPHONE , message = "手机号码格式不正确")
    private String cellPhone;


    /**
     * 性别
     */
    private Integer gender;

    /**
     * 年龄
     */
    private Integer age;
    /**
     * 身份证
     */
    @Pattern(regexp = Const.PATTERN_USERID , message = "身份证格式不正确")
    private String userId;
    /**
     * 密码
     */
    private String password;


}
