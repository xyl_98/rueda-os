package cn.tedu.rueda.passport.pojo.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;


/**
 * 获取用户信息
 */
@Data
@TableName("rueda_account")
public class UserInfoVO {
    /*
    用户id
     */
    private Long id;
    /*
    用户用户名
     */
    private String username;
    /*
    用户密码
     */
    private String password;
    /*
    用户手机号
     */
    private String cellPhone;
    /*
    用户昵称
     */
    private String nick;

    /*
    用户头像
     */
    private String sculpture;

    /*
    用户年龄
     */
    private Integer age;

    /*
    用户性别码
     */
    private Integer gender;

    private Integer roleId;
    /*
    用户权限
     */
    private List permissions;

}
