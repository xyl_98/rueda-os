package cn.tedu.rueda.passport.controller.aop;

import cn.tedu.rueda.passport.dao.persist.repository.IUserLoginRepository;
import cn.tedu.rueda.passport.pojo.param.UserLoginParam;
import cn.tedu.rueda.passport.pojo.param.UserLoginSmsCodeParam;
import cn.tedu.rueda.passport.pojo.vo.UserInfoVO;
import cn.tedu.rueda.passport.service.IUserLoginService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@Aspect
@Component
public class AdminLogin {

    @Autowired
    private IUserLoginRepository userLoginRepository;


    @Before("execution(* cn.tedu.rueda.passport.controller.UserLoginController.userLogin(..)) && args(cn.tedu.rueda.passport.pojo.param.UserLoginParam)")
    public void adminLogin(JoinPoint joinPoint) throws RoleException {
        Object[] args = joinPoint.getArgs();
        UserLoginParam arg = (UserLoginParam)args[0];
        Integer roleId = arg.getRoleId();
        log.debug("aop角色判断前roleId:{}",roleId);
        if (roleId==null) {
            return;
        }
        UserInfoVO userInfoByPhone = userLoginRepository.getUserInfoByPhone(arg.getCellPhone());
        if (userInfoByPhone == null) {
            return;
        }
        boolean equals = userInfoByPhone.getRoleId().equals(roleId);
        if (equals) {
            return;
        }else {
            throw new RoleException("你不符合当前登录权限");
        }
    }

    @Before("execution(* cn.tedu.rueda.passport.controller.UserLoginController.userLogin(..)) && args(cn.tedu.rueda.passport.pojo.param.UserLoginSmsCodeParam)")
    public void adminSmsLogin(JoinPoint joinPoint) throws RoleException {
        Object[] args = joinPoint.getArgs();
        UserLoginSmsCodeParam arg = (UserLoginSmsCodeParam)args[0];
        Integer roleId = arg.getRoleId();
        log.debug("aop角色判断前roleId:{}",roleId);
        if (roleId==null) {
            return;
        }
        UserInfoVO userInfoByPhone = userLoginRepository.getUserInfoByPhone(arg.getCellPhone());
        if (userInfoByPhone == null) {
            return;
        }
        boolean equals = userInfoByPhone.getRoleId().equals(roleId);
        if (equals) {
            return;
        }else {
            throw new RoleException("你不符合当前登录权限");
        }
    }
}
