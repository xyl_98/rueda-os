package cn.tedu.rueda.passport.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@TableName("rueda_account_role")
public class AccountRole {
    /** 序列化ID */
    private static final long serialVersionUID = 4378497831162670193L;
    /** 用户角色ID */
    @ApiModelProperty(value = "用户角色ID")
    private Long id;
    /** 用户ID */
    @ApiModelProperty(value = "用户ID")
    private Long accountId;
    /** 角色ID */
    @ApiModelProperty(value = "角色ID")
    private Long roleId;

}
