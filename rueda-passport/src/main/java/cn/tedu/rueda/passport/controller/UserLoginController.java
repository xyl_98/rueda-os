package cn.tedu.rueda.passport.controller;

import cn.tedu.rueda.common.web.JsonResult;
import cn.tedu.rueda.passport.pojo.param.UserLoginParam;
import cn.tedu.rueda.passport.pojo.param.UserLoginSmsCodeParam;
import cn.tedu.rueda.passport.pojo.vo.UserLoginResult;
import cn.tedu.rueda.passport.service.IUserLoginService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.Ignore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@Api(tags = "2. 用户登录")
@RequestMapping("/passport/")
public class UserLoginController {
    @Autowired
    private IUserLoginService userLoginService;

    @ApiOperation("密码登录")
    @ApiOperationSupport(order = 200)
    @PostMapping("login")
    public JsonResult userLogin(UserLoginParam loginParam){
        log.debug("开始处理【用户登录】的请求，参数：{}", loginParam);
        UserLoginResult loginResult = userLoginService.userLogin(loginParam);
        return JsonResult.ok(loginResult);
    }

    @ApiOperation("短信验证码登录")
    @ApiOperationSupport(order = 201)
    @PostMapping("login-smsCode")
    public JsonResult userLogin(UserLoginSmsCodeParam loginSmsCodeParam){
        log.debug("开始处理【用户登录】的请求，参数：{}", loginSmsCodeParam);
        UserLoginResult loginResult = userLoginService.userLogin(loginSmsCodeParam);
        return JsonResult.ok(loginResult);
    }

    @ApiOperation("退出登录")
    @GetMapping("logout")
    @ApiImplicitParam(name = "id",value = "退出的用户id",required = true,dataType = "long")
    public JsonResult logout(@Ignore @AuthenticationPrincipal(expression = "id") Long id){
        log.debug("开始处理【用户退出】的请求");
        userLoginService.userLogout(id);
        log.debug("退出完成，返回开始");
        return JsonResult.ok();
    }

    @ApiOperation("发送短信验证码")
    @PostMapping("smsCode")
    public JsonResult smsCode(@RequestParam String phone){
        log.debug("开始处理【发送短信验证码】的请求");
        userLoginService.verificationCode(phone);

        return JsonResult.ok();
    }
}
