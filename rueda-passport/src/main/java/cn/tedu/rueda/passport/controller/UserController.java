package cn.tedu.rueda.passport.controller;

import cn.tedu.rueda.common.web.JsonResult;
import cn.tedu.rueda.passport.pojo.param.AdminRegParam;
import cn.tedu.rueda.passport.pojo.param.PatientRegParam;
import cn.tedu.rueda.passport.pojo.param.UpdatePasswordParam;
import cn.tedu.rueda.passport.service.UserService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/passport")
@Validated
@Api(tags = "1. 用户注册")
public class UserController {

    @Autowired
    private UserService userService;


    @PostMapping("/reg/patient")
    @ApiOperation("患者注册")
    @ApiOperationSupport(order = 100)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "姓名", required = true,  dataType = "string"),
            @ApiImplicitParam(name = "cellPhone", value = "电话号码", required = true, dataType = "string"),
            @ApiImplicitParam(name = "userId", value = "身份证",required = true, dataType = "string"),
            @ApiImplicitParam(name = "gender", value = "性别",required = true, dataType = "string"),
            @ApiImplicitParam(name = "age", value = "年龄",required = true, dataType = "string"),
            @ApiImplicitParam(name = "password", value = "密码",required = true, dataType = "string")
    })
    public JsonResult PatientReg(@Valid PatientRegParam patientRegParam){
        userService.PatientReg(patientRegParam);
        return JsonResult.ok();
    }

    @PostMapping("/reg/admin")
    @ApiOperation("医生和管理员注册")
    @ApiOperationSupport(order = 100)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "姓名", required = true,  dataType = "string"),
            @ApiImplicitParam(name = "cellPhone", value = "电话号码", required = true, dataType = "string"),
            @ApiImplicitParam(name = "userId", value = "身份证",required = true, dataType = "string"),
            @ApiImplicitParam(name = "gender", value = "性别",required = true, dataType = "string"),
            @ApiImplicitParam(name = "age", value = "年龄",required = true, dataType = "string"),
            @ApiImplicitParam(name = "roleId", value = "角色",required = true, dataType = "int"),
            @ApiImplicitParam(name = "authId", value = "权限码",required = true, dataType = "int"),
            @ApiImplicitParam(name = "sculpture", value = "头像", dataType = "string")
    })
    @PreAuthorize("hasAuthority('全部')")
    public JsonResult DoctorReg(@Valid AdminRegParam adminRegParam, @AuthenticationPrincipal(expression = "username") String username){
        log.warn("开执行创建医生流程");
        userService.AdminReg(adminRegParam ,username);
        return JsonResult.ok();
    }

    @PostMapping("/update/password")
    @ApiOperation("修改密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "password" ,value = "旧密码" ,required = true,dataType = "string"),
            @ApiImplicitParam(name = "newPassword" ,value = "新密码" ,required = true,dataType = "string"),
            @ApiImplicitParam(name = "secondPassword" ,value = "确认密码" ,required = true,dataType = "string")
    })
    public JsonResult UpdatePassword(@AuthenticationPrincipal(expression = "id") Long userId, @Valid @RequestBody UpdatePasswordParam updatePasswordParam){
        log.debug("接收的修改密码参数{} userId{}",updatePasswordParam,userId);
        userService.updatePassword(userId,updatePasswordParam);
        return JsonResult.ok();
    }

}
