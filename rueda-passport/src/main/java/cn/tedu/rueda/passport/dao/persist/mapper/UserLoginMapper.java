package cn.tedu.rueda.passport.dao.persist.mapper;


import cn.tedu.rueda.passport.pojo.vo.UserInfoVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface UserLoginMapper extends BaseMapper<UserInfoVO> {

    /**
     * 查询用户的登录信息和权限
     * @param phone 用户登录的手机号
     * @return UserLoginInfoVO
     */
    UserInfoVO getUserInfoAndPermissionsByPhone(String phone);
}
