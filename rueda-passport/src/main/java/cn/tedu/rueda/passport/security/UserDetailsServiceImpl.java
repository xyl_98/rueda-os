package cn.tedu.rueda.passport.security;

import cn.tedu.rueda.passport.dao.cache.IUserCacheRepository;
import cn.tedu.rueda.passport.dao.persist.repository.IUserLoginRepository;
import cn.tedu.rueda.passport.pojo.vo.UserInfoVO;
import cn.tedu.rueda.passport.util.ThreadLocalUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * 进行security认证
 */
@Slf4j
@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private IUserLoginRepository userLoginRepository;

    @Autowired
    private ThreadLocalUtil threadLocalUtil;

    @Autowired
    private IUserCacheRepository userCacheRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String phone) throws UsernameNotFoundException {
        log.debug("进入UserDetailsServiceImpl类");
        UserInfoVO userInfo = userLoginRepository.getUserInfoByPhone(phone);
        log.debug("验证查询的数据:"+userInfo);
        if (userInfo == null) {
            return null;
        }

        List<String> permissions = userInfo.getPermissions();
        threadLocalUtil.set(permissions);
        Collection<GrantedAuthority> authorities ;
        authorities = AuthorityUtils.createAuthorityList(permissions.stream().toArray(String[]::new));
//        for (String permission : permissions) {
//            GrantedAuthority authority = new SimpleGrantedAuthority(permission);
//            authorities.add(authority);
//        }

        //获取reids里的短信验证码
        String smsCode = userCacheRepository.getSmsCode(phone);
        log.debug("redis读取的短信验证码数据"+smsCode);

        //设置是用密码还是验证码登录需要传入验证的信息
        String token;
        if (smsCode != null) {
            String smsCodeEncode = passwordEncoder.encode(smsCode);
            token = smsCodeEncode;
        }else{
            token = userInfo.getPassword();
        }
        log.debug("验证的短信验证码:或者 验证的密码"+token);

        log.debug("开始验证");
        CustomUserDetails userDetails = new CustomUserDetails(
                userInfo.getId(),
                userInfo.getNick(),
                userInfo.getSculpture(),
                userInfo.getAge(),
                userInfo.getGender(),
                userInfo.getUsername(),
                phone,
                token,
                authorities
        );
        return userDetails;
    }
}
