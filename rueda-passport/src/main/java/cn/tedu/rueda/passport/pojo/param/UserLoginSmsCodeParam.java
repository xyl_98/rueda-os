package cn.tedu.rueda.passport.pojo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserLoginSmsCodeParam implements Serializable {
    @ApiModelProperty(value = "手机号")
    private String cellPhone;
    @ApiModelProperty(value = "短信验证码")
    private String smsCode;

    private Integer roleId;
}
