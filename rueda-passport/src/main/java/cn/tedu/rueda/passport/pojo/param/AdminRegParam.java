package cn.tedu.rueda.passport.pojo.param;

import cn.tedu.rueda.common.constant.Const;
import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;

@Data
@TableName("rueda_account")
public class AdminRegParam {
    /**
     * 姓名
     */
    private String username;

    /**
     * 电话号码
     */
    @Pattern(regexp =Const.PATTERN_CELLPHONE , message = "手机号码格式不正确")
    private String cellPhone;

    /**
     * 身份证
     */
    @Pattern(regexp =Const.PATTERN_USERID , message = "身份证格式不正确")
    private String userId;

    /**
     * 角色
     * 普通用户 ：   1
     * 管理员用户：  2
     * 超级管理员：  3
     */
    private Long roleId;

    /**
     * 权限码
     * 普通用户 ：   1
     * 管理员用户：  2
     * 超级管理员：  3
     */
    private Long authId;

    /**
     * 头像
     */
    private String sculpture;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 年龄
     */
    private Integer age;


}
