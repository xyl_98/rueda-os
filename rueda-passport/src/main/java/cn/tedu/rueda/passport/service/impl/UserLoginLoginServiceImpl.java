package cn.tedu.rueda.passport.service.impl;

import cn.tedu.rueda.common.enumerator.ServiceCode;
import cn.tedu.rueda.common.ex.ServiceException;
import cn.tedu.rueda.common.pojo.po.UserStatePO;
import cn.tedu.rueda.passport.dao.cache.IUserCacheRepository;
import cn.tedu.rueda.passport.dao.persist.repository.IUserLoginRepository;
import cn.tedu.rueda.passport.pojo.param.UserLoginParam;
import cn.tedu.rueda.passport.pojo.param.UserLoginSmsCodeParam;
import cn.tedu.rueda.passport.pojo.vo.UserInfoVO;
import cn.tedu.rueda.passport.pojo.vo.UserLoginResult;
import cn.tedu.rueda.passport.sample.Sample;
import cn.tedu.rueda.passport.security.CustomUserDetails;
import cn.tedu.rueda.passport.service.IUserLoginService;
import cn.tedu.rueda.passport.util.ThreadLocalUtil;
import com.alibaba.fastjson.JSON;
import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.tea.TeaException;
import com.aliyun.teautil.Common;
import com.aliyun.teautil.models.RuntimeOptions;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class UserLoginLoginServiceImpl implements IUserLoginService {

    @Value("${tmall.jwt.secret-key}")
    private String secretKey;

    @Value("${tmall.jwt.duration-in-minute}")
    private Long time;

    @Value("${tmall.sample.signName}")
    private String signName;

    @Value("${tmall.sample.templateCode}")
    private String templateCode;

    @Autowired
    private IUserLoginRepository userLoginRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserCacheRepository userCacheRepository;

    @Autowired
    private ThreadLocalUtil threadLocalUtil;

    /**
     * 处理密码登录
     * @param loginParam 接收的登录请求对象
     * @return 返回UserLoginResult封装的用户信息
     */
    @Override
    public UserLoginResult userLogin(UserLoginParam loginParam) {
        log.debug("进入Service处理密码登录逻辑");

            //传入用户输入的账号信息
            Authentication authentication =
                    new UsernamePasswordAuthenticationToken(
                            loginParam.getCellPhone(),
                            loginParam.getPassword()
                    );
        UserLoginResult loginResult = loginResponse(authentication);

        return loginResult;
    }

    /**
     * 处理短信验证码登录
     * @param loginSmsCodeParam 接收的登录请求对象
     * @return 返回UserLoginResult封装的用户信息
     */
    @Override
    public UserLoginResult userLogin(UserLoginSmsCodeParam loginSmsCodeParam) {
        log.debug("进入Service处理短信验证码登录逻辑");

        //传入用户输入的账号信息
        Authentication authentication =
                new UsernamePasswordAuthenticationToken(
                        loginSmsCodeParam.getCellPhone(),
                        loginSmsCodeParam.getSmsCode()
                );
        UserLoginResult loginResult = loginResponse(authentication);

        return loginResult;
    }

    /**
     * 验证登录成功后打包响应
     * @param authentication
     * @return 响应数据
     */
    private UserLoginResult loginResponse(Authentication authentication){
        log.debug("运行后自动调用");
        //进行认证，认证成功这返回认证的相关用户信息
        Authentication authenticate = authenticationManager.authenticate(authentication);
        System.out.println("aaaaaaaa"+authenticate);
        //获取认证userDetails的返回信息
        Object principal = authenticate.getPrincipal();
        //获取用户的信息
        CustomUserDetails userDetails = (CustomUserDetails) principal;
        log.debug("返回的userDetails:"+userDetails);
        Long id = userDetails.getId();
        String cellPhone = userDetails.getUsername();
        String username = userDetails.getUserName();
        String nick = userDetails.getNick();

        //获取用户的权限信息并转成JSON的字符串格式
        Collection<GrantedAuthority> authorities = userDetails.getAuthorities();
        List<String> authorityList = new ArrayList<>();
        for (GrantedAuthority authority : authorities) {
            authorityList.add(authority.getAuthority());
        }
        log.debug("权限转换List格式"+authorityList);
//        String jsonStringAuthorityList = JSON.toJSONString(authorities);

        //设置jwt的有效时间
        Date date = new Date(System.currentTimeMillis()+ 1L * time * 60 * 1000);

        //jwt封装的用户信息
        Map<String, Object> claims = new HashMap<>();
        claims.put("id", id);
        claims.put("username", username);
        claims.put("nick", nick);
//        claims.put("JsonStringAuthorityList", JsonStringAuthorityList);

        //把用户对应权限写如redis
        UserStatePO userStatePO = new UserStatePO();
        userStatePO.setAuthorities(authorityList);
        log.debug("写入redis权限格式"+userStatePO);
        userCacheRepository.saveState(id,userStatePO);

        //生成jwt
        String jwt = Jwts.builder()
                .setHeaderParam("alg", "HS256")
                .setHeaderParam("typ", "JWT")
                .setClaims(claims)
                .setExpiration(date)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();


        List<String> authority = (List<String>) threadLocalUtil.get();
        log.debug("线程隔离输出:"+authority);

        //打包返回当前登录用户的信息
        UserLoginResult loginResult = new UserLoginResult()
                .setCellPhone(cellPhone)
                .setUsername(username)
                .setNick(nick)
                .setId(id)
                .setAge(userDetails.getAge())
                .setGender(userDetails.getGender())
                .setSculpture(userDetails.getSculpture())
                .setToken(jwt)
                .setAuthorities(authority);

        return loginResult;
    }

    /**
     * 退出功能
     * @param id 退出用户的id
     */
    @Override
    public void userLogout(Long id) {
        boolean rows = userCacheRepository.deleteAuthority(id);
    }

    /**
     * 生成验证码，并通过阿里云服务发送给用户手机
     * @param phone
     */
    @Override
    public void verificationCode(String phone) {
        UserInfoVO userInfo = userLoginRepository.getUserInfoByPhone(phone);
        log.debug("验证查询的数据:"+userInfo);
        if (userInfo == null) {
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,"用户不存在");
        }

        try {

            Client client = Sample.createClient();

            //生成随机4位验证码
            Random random = new Random();
            int code = random.nextInt(9000)+1000;
            String redisCode = code+"";
            //发送给aliyun的格式
            String smsCode = "{\"code\":\""+code+"\"}";

            //传入给aliyun的请求数据
            SendSmsRequest sendSmsRequest = new SendSmsRequest()
                    .setSignName(signName)
                    .setTemplateCode(templateCode)
                    .setPhoneNumbers(phone)
                    .setTemplateParam(smsCode);

            RuntimeOptions runtime = new RuntimeOptions();

            // 复制代码运行请自行打印 API 的返回值
            SendSmsResponse sendSmsResponse = client.sendSmsWithOptions(sendSmsRequest, runtime);
            //短信发送成功就存入redis
            if (sendSmsResponse.getStatusCode() == 200) {
                log.debug("phone:"+phone+"code:"+smsCode);
                userCacheRepository.saveVerificationCode(phone,redisCode);
            }
        } catch (TeaException error) {
            // 如有需要，请打印 error
            Common.assertAsString(error.message);
            log.debug("向阿里云提交发送短信验证码错误",error);
            throw new ServiceException(ServiceCode.ERROR_INSERT,"未知的错误，请稍后再试");
        } catch (Exception _error) {
            TeaException error = new TeaException(_error.getMessage(), _error);
            // 如有需要，请打印 error
            Common.assertAsString(error.message);
            log.debug("向阿里云提交发送短信验证码错误",_error);
            throw new ServiceException(ServiceCode.ERROR_INSERT,"未知的错误，请稍后再试");
        }
    }


}
