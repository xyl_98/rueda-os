package cn.tedu.rueda.passport.dao.persist.repository.impl;

import cn.tedu.rueda.passport.dao.persist.mapper.UserMapper;
import cn.tedu.rueda.passport.dao.persist.repository.UserRepository;
import cn.tedu.rueda.passport.pojo.entity.Account;
import cn.tedu.rueda.passport.pojo.entity.AccountRole;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private UserMapper userMapper;

//    @Autowired
//    private UserRegMapper userRegMapper;

    /**
     * 查询患者账号是否存在
     * @param cellPhone
     * @return
     */
    @Override
    public int select(String cellPhone) {
        QueryWrapper<Account> wrapper = new QueryWrapper<>();
        wrapper.eq("cell_phone",cellPhone);
        return userMapper.selectCount(wrapper);
    }

    /**
     * 新增患者
     * @param account
     * @return
     */
    @Override
    public int insert(Account account) {
        return userMapper.insert(account);
    }

    @Override
    public int insertById(AccountRole accountRole) {
        return userMapper.insertById(accountRole);
    }

    @Override
    public int updateById(Account account) {
        return userMapper.updateById(account);
    }

    @Override
    public int updatePasswordById(Account account) {
        return userMapper.updatePasswordById(account);
    }

    @Override
    public String selectPasswordSuccess(Long id) {
        return userMapper.selectPasswordSuccess(id);
    }


}
