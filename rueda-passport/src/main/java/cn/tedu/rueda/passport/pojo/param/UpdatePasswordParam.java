package cn.tedu.rueda.passport.pojo.param;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UpdatePasswordParam {
    @NotNull
    private String password;
    @NotNull
    private String newPassword;
    @NotNull
    private String secondPassword;
}
