package cn.tedu.rueda.passport.dao.persist.repository;

import cn.tedu.rueda.passport.pojo.vo.UserInfoVO;

public interface IUserLoginRepository {

    /**
     * 查询用户的信息
     * @param phone 根据登录的手机号
     * @return UserLoginInfoVO
     */
    UserInfoVO getUserInfoByPhone(String phone);
}
