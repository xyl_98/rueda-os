package cn.tedu.rueda.passport.dao.persist.repository;


import cn.tedu.rueda.passport.pojo.entity.Account;
import cn.tedu.rueda.passport.pojo.entity.AccountRole;
import cn.tedu.rueda.passport.pojo.param.UpdatePasswordParam;


public interface UserRepository {

    int select(String cellPhone);

    int insert(Account account);

    int insertById(AccountRole accountRole);

    int updateById(Account account);

    int updatePasswordById(Account account);

    String selectPasswordSuccess(Long id);

}
