package cn.tedu.rueda.passport.security;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

/**
 * 自定义的UserDetails
 */
@Getter
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class CustomUserDetails extends User {
    private Long id;
    private String nick;
    private String sculpture;
    private Integer age;
    private Integer gender;
    private String userName;


    public CustomUserDetails(Long id,String nick ,String sculpture,Integer age,Integer gender,String userName, String cellPhone, String password, Collection<? extends GrantedAuthority> authorities) {
        super(cellPhone, password, true, true, true, true, authorities);
        this.id = id;
        this.nick = nick;
        this.sculpture = sculpture;
        this.age = age;
        this.gender = gender;
        this.userName = userName;
    }
}
