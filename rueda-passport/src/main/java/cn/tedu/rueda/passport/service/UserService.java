package cn.tedu.rueda.passport.service;


import cn.tedu.rueda.passport.pojo.param.AdminRegParam;
import cn.tedu.rueda.passport.pojo.param.PatientRegParam;
import cn.tedu.rueda.passport.pojo.param.UpdatePasswordParam;
import org.springframework.transaction.annotation.Transactional;

@Transactional(rollbackFor = Exception.class)
public interface UserService {

    /**
     * 患者注册功能
     * @param patientRegParam
     * @return
     */
    void PatientReg(PatientRegParam patientRegParam);

    void AdminReg(AdminRegParam adminRegParam ,String username);


    void updatePassword(Long userId,UpdatePasswordParam updatePasswordParam);
}
