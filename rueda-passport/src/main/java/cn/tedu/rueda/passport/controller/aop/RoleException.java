package cn.tedu.rueda.passport.controller.aop;

import java.lang.reflect.UndeclaredThrowableException;

public class RoleException extends RuntimeException {
    public RoleException(String message){
        super(message);
    }
}
