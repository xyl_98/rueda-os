package cn.tedu.rueda.passport.repositoryTest;

import cn.tedu.rueda.passport.dao.cache.IUserCacheRepository;
import cn.tedu.rueda.passport.dao.persist.mapper.UserLoginMapper;
import cn.tedu.rueda.passport.dao.persist.repository.IUserLoginRepository;
import cn.tedu.rueda.passport.pojo.vo.UserInfoVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
public class UserLoginRepository {
    @Autowired
    private IUserLoginRepository userLoginRepository;

    @Autowired
    private IUserCacheRepository userCacheRepository;

    @Autowired
    private UserLoginMapper mapper;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Test
    void xxx(){
        UserInfoVO infov = mapper.getUserInfoAndPermissionsByPhone("12233334444");
        System.out.println(infov);
    }

    @Test
    void password(){
        String encode = passwordEncoder.encode("123456");
        System.out.println(encode);
    }

    @Test
    void redi(){
        String smsCode = userCacheRepository.getSmsCode("17844683827");
        System.out.println(smsCode);
    }

}
