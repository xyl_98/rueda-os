package cn.tedu.rueda.passport.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private IUserLoginService userService;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private UserService userService1;

//    @Test
//    void xxx(){
//        CountDownLatch latch = new CountDownLatch(1);
//
//        Thread thread1 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                // 线程1的任务
//                UserLoginParam userLoginParam = new UserLoginParam();
//                userLoginParam.setPhone("12233334444");
//                userLoginParam.setPassword("123456");
//                UserLoginResult loginResult = userService.userLogin(userLoginParam);
//                System.out.println("线程1执行完成"+loginResult);
//                latch.countDown();
//            }
//        });
//
//        Thread thread2 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                // 线程2的任务
//                UserLoginParam userLoginParam = new UserLoginParam();
//                userLoginParam.setPhone("13333334444");
//                userLoginParam.setPassword("123456");
//                UserLoginResult loginResult = userService.userLogin(userLoginParam);
//                System.out.println("线程2执行完成"+loginResult);
//                latch.countDown();
//            }
//        });
//
//        thread1.start();
//        thread2.start();
//
//        try {
//            latch.await(); // 等待两个线程都执行完毕
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        System.out.println("两个线程都执行完毕");
//    }

    @Test
    void verificationCode(){
        userService.verificationCode("17844683827");
    }

    @Test
    void zzzz(){
        String encode = passwordEncoder.encode("123456");
        System.out.println(encode);
    }

}
