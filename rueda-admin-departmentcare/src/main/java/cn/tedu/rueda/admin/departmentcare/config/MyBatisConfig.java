package cn.tedu.rueda.admin.departmentcare.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("cn.tedu.rueda.admin.departmentcare.dao.persist.mapper")
public class MyBatisConfig {
}
