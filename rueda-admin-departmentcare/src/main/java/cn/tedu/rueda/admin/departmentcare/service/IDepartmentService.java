package cn.tedu.rueda.admin.departmentcare.service;

import cn.tedu.rueda.admin.departmentcare.pojo.entity.Department;
import cn.tedu.rueda.admin.departmentcare.pojo.param.DepartmentAddNewParam;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentStandardVO;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentlistVO;
import cn.tedu.rueda.common.pojo.vo.PageData;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface IDepartmentService {
    /**
     * 显示状态：隐藏
     */
    int DISPLAY_STATE_OFF = 0;
    /**
     * 显示状态：显示
     */
    int DISPLAY_STATE_ON = 1;
    /**
     * 数据“是否显示”的状态文本
     */
    String[] DISPLAY_STATE_TEXT = {"隐藏", "显示"};

    /**
     * 新增类别
     *
     * @param departmentAddNewParam 类别数据
     */
    void addNew(DepartmentAddNewParam departmentAddNewParam, String username);

    /**
     * 根据ID删除类别
     *
     * @param id 尝试删除的类别数据的ID
     */
    void delete(Long id);

    DepartmentStandardVO findDepartmentById(Long id);

    PageData<DepartmentlistVO> listByParent(Long parentId, Integer pageNum);

    /**
     * 根据父级类别查询其子级类别列表
     *
     * @param parentId 父级类别的ID
     * @param pageNum  页码
     * @param pageSize 每页数据量
     * @return 类别列表
     */
    PageData<DepartmentlistVO> listByParent(Long parentId, Integer pageNum, Integer pageSize);




    PageData<DepartmentlistVO> listByParent1(Long parentId, Integer pageNum);

    /**
     * 根据父级类别查询其子级类别列表
     *
     * @param parentId 父级类别的ID
     * @param pageNum  页码
     * @param pageSize 每页数据量
     * @return 类别列表
     */
    PageData<DepartmentlistVO> listByParent1(Long parentId, Integer pageNum, Integer pageSize);

    List<DepartmentlistVO> list();
}
