package cn.tedu.rueda.admin.departmentcare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RuedaAdminDepartmentcareApplication {

    public static void main(String[] args) {
        SpringApplication.run(RuedaAdminDepartmentcareApplication.class, args);
    }

}
