package cn.tedu.rueda.admin.departmentcare.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;
@Data
public class DepartmentDoctorVO implements Serializable {

    /**
     * 编号
     */
    private Long id;

    /**88
     * 科室名
     */
    private String name;

    /**
     * 层级，最顶级类别的层级为1，次级为2，以此类推
     */
    private Integer level;

    /**
     * 描述
     */
    private String description;

    /**
     * 状态
     */
    private Integer state;

    private List<String> doctor;
}
