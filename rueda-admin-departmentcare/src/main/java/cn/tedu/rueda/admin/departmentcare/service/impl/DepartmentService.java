package cn.tedu.rueda.admin.departmentcare.service.impl;

import cn.tedu.rueda.admin.departmentcare.dao.persist.repository.IDepartmentRepository;
import cn.tedu.rueda.admin.departmentcare.pojo.entity.Department;
import cn.tedu.rueda.admin.departmentcare.pojo.param.DepartmentAddNewParam;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentStandardVO;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentlistVO;
import cn.tedu.rueda.admin.departmentcare.service.IDepartmentService;
import cn.tedu.rueda.common.enumerator.ServiceCode;
import cn.tedu.rueda.common.ex.ServiceException;
import cn.tedu.rueda.common.pojo.vo.PageData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class DepartmentService implements IDepartmentService {
    @Value("${tmall.dao.default-query-page-size}")
    private Integer defaultQueryPageSize;
    @Autowired
    private IDepartmentRepository departmentRepository;


    public DepartmentService() {
        log.debug("创建业务类对象：DepartmentService");
    }

    /**
     *
     * @param departmentAddNewParam 新增科室数据
     */
    @Override
    @Transactional
    public void addNew(DepartmentAddNewParam departmentAddNewParam,String username) {
        String name = departmentAddNewParam.getName();

        int countByName = departmentRepository.countByName(name);
        if (countByName > 0) {
            String message = "添加科室失败，科室已经存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }

        //查询父级信息
        Long parentId = departmentAddNewParam.getParentId();

        Integer level;
        if (parentId != 0) {
            DepartmentStandardVO standardById = departmentRepository.getStandardById(parentId);
            level = standardById.getLevel()+1;
        }else {
            level = 1;
        }


        Department department = new Department();
        BeanUtils.copyProperties(departmentAddNewParam, department);
        department.setCreateTime(LocalDateTime.now());
        department.setLevel(level);
        department.setIsParent(0);
        department.setCreateUser(username);
        int rows = departmentRepository.insert(department);
        if (rows != 1) {
            String message = "添加科室失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_INSERT, message);
        }
        if (parentId!=0) {
            int i = departmentRepository.updateById(parentId);
            if (i!=1) {
                String message = "添加科室失败，服务器忙，请稍后再尝试！";
                log.warn(message);
                throw new ServiceException(ServiceCode.ERROR_INSERT, message);
            }
        }
    }

    @Override
    @Transactional
    public void delete(Long id) {
        log.debug("开始处理【根据ID删除类别】的业务，参数：{}", id);
        int count = departmentRepository.countDoctorByDepId(id);
        if (count > 0){
            String message = "当前科室下有医生不允许删除";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_DELETE, message);
        }

        int num = departmentRepository.countIsParent(id);
        if (num>0) {
            String message = "当前科室下有子科室不允许删除";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_DELETE, message);
        }

        DepartmentStandardVO standardById = departmentRepository.getStandardById(id);
        Long parentId = standardById.getParentId();
        int rows = departmentRepository.deleteById(id);
        if (rows != 1) {
            String message = "删除类别失败，服务器忙，请稍后再尝试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_DELETE, message);
        }

        int i = departmentRepository.countIsParent(parentId);
        if (i < 1) {
            int row = departmentRepository.setIsParent(parentId);
            if (row!=1) {
                String message = "删除类别失败，服务器忙，请稍后再尝试！";
                log.warn(message);
                throw new ServiceException(ServiceCode.ERROR_DELETE, message);
            }
        }

    }

    @Override
    public DepartmentStandardVO findDepartmentById(Long id) {
        DepartmentStandardVO departmentStandardVO = departmentRepository.getStandardById(id);
        if (departmentStandardVO == null) {
            String message = "查询失败，尝试访问的数据不存在！";
            log.error(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        return departmentStandardVO;
    }

    @Override
    public PageData<DepartmentlistVO> listByParent(Long parentId, Integer pageNum, Integer pageSize) {
        return departmentRepository.listByParent(parentId, pageNum, pageSize);
    }

    @Override
    public PageData<DepartmentlistVO> listByParent(Long parentId, Integer pageNum) {
        return departmentRepository.listByParent(parentId, pageNum,defaultQueryPageSize);
    }


    @Override
    public PageData<DepartmentlistVO> listByParent1(Long parentId, Integer pageNum, Integer pageSize) {
        return departmentRepository.listByParent(parentId, pageNum, pageSize);
    }

    @Override
    public List<DepartmentlistVO> list() {
        return departmentRepository.list();
    }

    @Override
    public PageData<DepartmentlistVO> listByParent1(Long parentId, Integer pageNum) {
        return departmentRepository.listByParent(parentId, pageNum,defaultQueryPageSize);
    }
}