package cn.tedu.rueda.admin.departmentcare.pojo.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 标准VO类：商品-类别
 *
 * @author java@tedu.cn
 * @version 2.0
 */
@Data
public class DepartmentStandardVO implements Serializable {

    /**
     * 编号
     */
    private Long id;

    /**
     * 创建人
     */
    private String createUser;


    /**
     * 修改人
     */
    private String updateUser;


    /**88
     * 科室名
     */
    private String name;

    /**
     * 父级类别ID，如果无父级，则为0
     */
    private Long parentId;


    /**
     * 深度，最顶级类别的深度为1，次级为2，以此类推
     */
    private Integer level;

    /**
     * 是否为父级（是否包含子级），1=是父级，0=不是父级
     */
    private Integer isParent;


    /**
     * 描述
     */
    private String description;

    /**
     * 状态
     */
    private Integer state;

    /**
     * 是否显示在导航栏中，1=启用，0=未启用
     */
    private Integer isDisplay;


}