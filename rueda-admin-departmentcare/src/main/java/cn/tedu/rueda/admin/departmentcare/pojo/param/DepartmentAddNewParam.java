package cn.tedu.rueda.admin.departmentcare.pojo.param;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 新增类别的参数类
 *
 * @author java@tedu.cn
 * @version 2.0
 */
@Data
public class DepartmentAddNewParam implements Serializable {

    /**
     * 类别名称
     */
    @ApiModelProperty(value = "科室名称", required = true)
    @NotNull(message = "请提交科室名称")
    private String name;

    /**
     * 父级科室ID，如果无父级，则为0
     */
    @ApiModelProperty(value = "父级科室ID，如果无父级，则为0", required = true)
    @NotNull(message = "请选择父级科室")
    @Range(message = "请提交有效的父级科室")
    private Long parentId;

    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 数据创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 数据最后修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    private String createUser;

    /**
     * 修改人
     */
    private String updateUser;

    /**
     * 深度，最顶级类别的深度为1，次级为2，以此类推
     */
    private Integer level;

    /**
     * 是否为父级（是否包含子级），1=是父级，0=不是父级
     */
    private Integer isParent;


    /**
     * 描述
     */
    private String description;

    /**
     * 状态
     */
    private Integer state;

}
