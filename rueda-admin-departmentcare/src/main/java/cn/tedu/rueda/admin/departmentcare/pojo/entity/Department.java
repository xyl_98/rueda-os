package cn.tedu.rueda.admin.departmentcare.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("rueda_department")
public class Department implements Serializable {
    /**
     * 编号
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 数据创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 数据最后修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    private String createUser;


    /**
     * 修改人
     */
    private String updateUser;


    /**88
     * 科室名
     */
    private String name;

    /**
     * 父级类别ID，如果无父级，则为0
     */
    private Long parentId;


    /**
     * 深度，最顶级类别的深度为1，次级为2，以此类推
     */
    private Integer level;

    /**
     * 是否为父级（是否包含子级），1=是父级，0=不是父级
     */
    private Integer isParent;


    /**
     * 描述
     */
    private String description;

    /**
     * 状态
     */
    private Integer state;


}
