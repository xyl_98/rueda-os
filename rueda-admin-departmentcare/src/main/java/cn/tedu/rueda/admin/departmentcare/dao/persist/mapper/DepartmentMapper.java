package cn.tedu.rueda.admin.departmentcare.dao.persist.mapper;

import cn.tedu.rueda.admin.departmentcare.pojo.entity.Department;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentDoctorVO;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentStandardVO;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentlistVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author
 * 处理账户相关
 */
@Repository
public interface DepartmentMapper extends BaseMapper<Department> {
    /**
     * 根据ID查询类别数据详情
     *
     * @param
     * @return 匹配的类别数据详情，如果没有匹配的数据，则返回null
     */
    DepartmentStandardVO getStandardById(Long id);


    /**
     * 根据父级类别查询其子级类别列表
     *
     * @param parentId 父级类别的ID
     * @return 类别列表
     */
    List<DepartmentlistVO> listByParent(Long parentId);



    /**
     * 根据父级类别查询其子级类别列表
     *
     * @param parentId 父级类别的ID
     * @return 类别列表
     */
    List<DepartmentlistVO> listByParent1(Long parentId);

    List<DepartmentlistVO> list();

    int updateIsParentById(Long parentId);

    int contDoctorByDepId(Long id);

    int countIsParent(Long id);

    int setIsParent(Long parentId);
}
