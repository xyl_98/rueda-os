package cn.tedu.rueda.admin.departmentcare.dao.persist.repository.impl;

import cn.tedu.rueda.admin.departmentcare.dao.persist.mapper.DepartmentDoctorMapper;
import cn.tedu.rueda.admin.departmentcare.dao.persist.mapper.DepartmentMapper;
import cn.tedu.rueda.admin.departmentcare.dao.persist.repository.IDepartmentRepository;
import cn.tedu.rueda.admin.departmentcare.pojo.entity.Department;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentDoctorVO;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentStandardVO;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentlistVO;
import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.common.util.PageInfoToPageDataConverter;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

//import static javafx.scene.input.KeyCode.L;

@Slf4j
@Repository
public class DepartmentRepository implements IDepartmentRepository {




    @Autowired
    private DepartmentMapper departmentMapper;

    public DepartmentRepository() {
        log.info("创建存储库对象：DepartmentRepository");
    }

    @Override
    public int insert(Department department) {
        log.debug("开始执行【插入类别】的数据访问，参数：{}", department);
        return departmentMapper.insert(department);
    }

    @Override
    public int deleteById(Long id) {
        log.debug("开始执行【根据ID删除类别】的数据访问，参数：{}", id);
        return departmentMapper.deleteById(id);
    }

    @Override
    public DepartmentStandardVO getStandardById(Long id) {
        log.debug("开始执行【根据ID查询类别信息】的数据访问，参数：{}", id);
        return departmentMapper.getStandardById(id);
    }

    @Override
    public int countByName(String name) {
        log.debug("开始执行【统计匹配名称的科室的数量】的数据访问，name参数：{}",name);
        QueryWrapper<Department> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", name);
        return departmentMapper.selectCount(queryWrapper);
    }

    @Override
    public int update(Department department) {
        log.debug("开始执行【更新类别】的数据访问，参数：{}", department);
        return departmentMapper.updateById(department);
    }

    @Override
    public PageData<DepartmentlistVO> listByParent(Long parentId, Integer pageNum, Integer pageSize) {
        log.debug("开始执行【根据父级类别查询其子级类别列表】的数据访问，父级类别：{}，页码：{}，每页数据量：{}", parentId, pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        List<DepartmentlistVO> list = departmentMapper.listByParent(parentId);
        PageInfo<DepartmentlistVO> pageInfo = new PageInfo<>(list);
        return PageInfoToPageDataConverter.convert(pageInfo);
    }



    @Autowired
    DepartmentDoctorMapper departmentDoctorMapper;


    /**
     * 根据科室名称查询医生
     */
    @Override
    public DepartmentDoctorVO getDepartmentDoctorName(Long id) {
        return departmentDoctorMapper.getDepartmentDoctorName(id);
    }

    @Override
    public List<DepartmentlistVO> list() {
        return departmentMapper.list();
    }

    @Override
    public int updateById(Long parentId) {
        return departmentMapper.updateIsParentById(parentId);
    }

    @Override
    public int countDoctorByDepId(Long id) {
        return departmentMapper.contDoctorByDepId(id);
    }

    @Override
    public int countIsParent(Long id) {
       return departmentMapper.countIsParent(id);
    }

    @Override
    public int setIsParent(Long parentId) {
        return departmentMapper.setIsParent(parentId);
    }
}