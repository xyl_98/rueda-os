package cn.tedu.rueda.admin.departmentcare.dao.persist.mapper;

import cn.tedu.rueda.admin.departmentcare.pojo.entity.Department;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentDoctorVO;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentStandardVO;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentlistVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author
 * 处理账户相关
 */
@Repository
public interface DepartmentDoctorMapper{
    DepartmentDoctorVO getDepartmentDoctorName(Long id);


}
