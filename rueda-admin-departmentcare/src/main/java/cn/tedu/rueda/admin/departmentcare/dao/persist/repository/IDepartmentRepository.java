package cn.tedu.rueda.admin.departmentcare.dao.persist.repository;

import cn.tedu.rueda.admin.departmentcare.pojo.entity.Department;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentDoctorVO;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentStandardVO;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentlistVO;
import cn.tedu.rueda.common.pojo.vo.PageData;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface IDepartmentRepository {
    /**
     * 插入类别数据
     *
     * @param department 类别数据
     * @return 受影响的行数
     */
    int insert(Department department);

    /**
     * 根据id删除类别数据
     *
     * @param id 类别ID
     * @return 受影响的行数
     */
    int deleteById(Long id);

    /**
     * 根据名称统计类别数据的数量
     *
     * @param name 科室名称
     * @return 匹配名称的类别数据的数量
     */
    int countByName(String name);

    /**
     * 根据ID查询类别
     *
     * @param id 类别ID
     * @return 匹配的类别，如果没有匹配的数据，则返回null
     */
    DepartmentStandardVO getStandardById(Long id);


    /**
     * 根据ID修改类别
     *
     * @param department 封装了类别ID和新数据的对象
     * @return 受影响的行数
     */
    int update(Department department);

    /**
     * 根据父级类别查询其子级类别列表
     *
     * @param parentId 父级类别的ID
     * @return 类别列表
     */
    PageData<DepartmentlistVO> listByParent(Long parentId, Integer pageNum, Integer pageSize);

    /**
     * 根据科室名称查询医生
     */
    DepartmentDoctorVO getDepartmentDoctorName(Long id);

    List<DepartmentlistVO> list();

    int updateById(Long parentId);

    int countDoctorByDepId(Long id);

    int countIsParent(Long id);

    int setIsParent(Long parentId);
}

