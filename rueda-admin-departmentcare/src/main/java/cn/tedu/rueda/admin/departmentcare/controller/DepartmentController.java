package cn.tedu.rueda.admin.departmentcare.controller;


import cn.tedu.rueda.admin.departmentcare.pojo.entity.Department;
import cn.tedu.rueda.admin.departmentcare.pojo.param.DepartmentAddNewParam;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentStandardVO;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentlistVO;
import cn.tedu.rueda.admin.departmentcare.service.IDepartmentService;
import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.common.web.JsonResult;
import com.github.pagehelper.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.Ignore;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/department/")
@Api(tags = "1. 类别管理")
@Validated
public class DepartmentController {
    @Autowired
    private IDepartmentService departmentService;

    public DepartmentController() {
        log.debug("创建控制器类对象：DepartmentController");
    }

    @PostMapping("/add-new")
    @ApiOperation("新增科室")
    @ApiOperationSupport(order = 100)
    public JsonResult addNew(@Valid DepartmentAddNewParam departmentAddNewParam, @AuthenticationPrincipal(expression = "username") String username) {
        departmentService.addNew(departmentAddNewParam,username);
        return JsonResult.ok();
    }

    @PostMapping("/{id:[0-9]+}/delete")
    @ApiOperation("根据ID删除类别")
    @ApiOperationSupport(order = 200)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "类别ID", required = true, dataType = "long")
    })
    public JsonResult delete(@Range(min = 1, message = "请提交合法的ID值")
                             @PathVariable Long id) {
        log.debug("开始处理【根据ID删除类别】的请求，参数：{}", id);
        departmentService.delete(id);
        return JsonResult.ok();
    }

    @ApiOperation(value = "根据ID查询类别")
    @ApiOperationSupport(order = 400)
    @ApiImplicitParam(name = "id", value = "类别ID", required = true, dataType = "Long")
    @GetMapping("/{id:[0-9]+}")
    public JsonResult findCategoryById(@Range(min = 1, message = "请提交合法的ID值")
                                       @PathVariable Long id) {
        log.debug("开始处理【根据ID查询类别】的请求，参数：{}", id);
        DepartmentStandardVO data = departmentService.findDepartmentById(id);
        return JsonResult.ok(data);
    }

    @GetMapping("/list-by-parent")
    @ApiOperation("根据父级查询子级列表")
    @ApiOperationSupport(order = 420)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parentId", value = "父级类别ID", required = true, paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "query", defaultValue = "1", dataType = "int"),
            @ApiImplicitParam(name = "queryType", value = "查询类型", paramType = "query", defaultValue = "all")
    })
    public JsonResult listByParent(@Range(message = "请提交有效的父级类别ID值！") Long parentId,
                                   @Range(min = 1, message = "请提交有效的页码值！") Integer page,
                                   String queryType) {
        Integer pageNum = page == null ? 1 : page;
        PageData<DepartmentlistVO> pageData;
        if ("all".equals(queryType)) {
            pageData = departmentService.listByParent(parentId, pageNum, Integer.MAX_VALUE);
        } else {
            pageData = departmentService.listByParent(parentId, pageNum);
        }
        return JsonResult.ok(pageData);
    }


    @GetMapping("/list-by-parent1")
    @ApiOperation("根据父级查询子级列表")
    @ApiOperationSupport(order = 420)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "parentId", value = "父级类别ID", required = true, paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "query", defaultValue = "1", dataType = "int"),
            @ApiImplicitParam(name = "queryType", value = "查询类型", paramType = "query", example = "all")
    })
    public JsonResult listByParent1(@Range(message = "请提交有效的父级类别ID值！") Long parentId,
                                    @Range(min = 1, message = "请提交有效的页码值！") Integer page,
                                    String queryType) {
        Integer pageNum = page == null ? 1 : page;
        PageData<DepartmentlistVO> pageData;
        if ("all".equals(queryType)) {
            pageData = departmentService.listByParent(parentId, pageNum, Integer.MAX_VALUE);
        } else {
            pageData = departmentService.listByParent(parentId, pageNum);
        }
        return JsonResult.ok(pageData);
    }

    @GetMapping("/list")
    public JsonResult list() {
        return JsonResult.ok(departmentService.list());
    }

}
