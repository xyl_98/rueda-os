package cn.tedu.rueda.admin.departmentcare;

import cn.tedu.rueda.admin.departmentcare.dao.persist.mapper.DepartmentDoctorMapper;
import cn.tedu.rueda.admin.departmentcare.dao.persist.mapper.DepartmentMapper;
import cn.tedu.rueda.admin.departmentcare.dao.persist.repository.IDepartmentRepository;
import cn.tedu.rueda.admin.departmentcare.dao.persist.repository.impl.DepartmentRepository;
import cn.tedu.rueda.admin.departmentcare.pojo.entity.Department;

import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentDoctorVO;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentStandardVO;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class DepartmentMapperTest {

    @Autowired
    DepartmentMapper mapper;

    @Test
    void insert() {
        Department department = new Department();
        department.setId(11L);
        department.setIsParent(0);
        department.setName("妇科");
        int rows = mapper.insert(department);
        System.out.println("新增成功"+rows);
    }

    @Test
    void deleteById() {
        Long id = 12L;
        int rows = mapper.deleteById(id);
        System.out.println("受影响的行数：" + rows);
    }

    @Autowired
    DepartmentDoctorMapper departmentDoctorMapper;

    @Test
    void getDepartmentDoctorName() {
        Long id=10L;
        DepartmentDoctorVO loginInfo = departmentDoctorMapper.getDepartmentDoctorName(id);
        System.out.println(loginInfo);
    }

    @Test
    void testList(){
        mapper.list();
    }



}
