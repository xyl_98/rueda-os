package cn.tedu.rueda.admin.departmentcare;

import cn.tedu.rueda.admin.departmentcare.pojo.entity.Department;
import cn.tedu.rueda.admin.departmentcare.pojo.param.DepartmentAddNewParam;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentStandardVO;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentlistVO;
import cn.tedu.rueda.admin.departmentcare.service.IDepartmentService;
import cn.tedu.rueda.common.ex.ServiceException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

@SpringBootTest
public class DepartmentServiceTests {

    @Autowired
    IDepartmentService service;

    @Test
    void addNew() {
        DepartmentAddNewParam departmentAddNewParam = new DepartmentAddNewParam();
        departmentAddNewParam.setCreateTime(LocalDateTime.now());
        departmentAddNewParam.setParentId(12L);
        departmentAddNewParam.setDescription("描述");
        departmentAddNewParam.setCreateUser("管理员一号");
        departmentAddNewParam.setName("急诊科一");
        try {
            service.addNew(departmentAddNewParam,"");
            System.out.println("新增类别，成功！");
        } catch (ServiceException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    void delete() {
        Long id = 12L;
        try {
            service.delete(id);
            System.out.println("删除类别，成功！");
        } catch (ServiceException e) {
            System.out.println(e.getMessage());
        }
    }

//    @Test
//    void getStandardById() {
//        String name ="内科";
//        DepartmentStandardVO departmentlistVO =service.getStandardById(id);
//        System.out.println("查询完成，结果：" + departmentlistVO);
//    }


}
