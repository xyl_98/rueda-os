package cn.tedu.rueda.admin.departmentcare;

import cn.tedu.rueda.admin.departmentcare.dao.persist.mapper.DepartmentMapper;
import cn.tedu.rueda.admin.departmentcare.pojo.entity.Department;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentlistVO;
import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.common.util.PageInfoToPageDataConverter;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class PageHelperTests {

    @Autowired
    DepartmentMapper mapper;

    @Test
    void listByParent() {
        Integer pageNum = 1; // 第几页
        Integer pageSize = 10; // 每页几条
        Long parentId = 0L;
        PageHelper.startPage(pageNum, pageSize); // 设置分页，注意：这句话必须直接出现在查询之前，否则可能导致线程安全问题
        List<DepartmentlistVO> list = mapper.listByParent(parentId); // Page类型，包含了分页的相关数据，但不便于访问
        System.out.println(list.getClass().getName());
        for (Object item : list) {
            System.out.println(item);
        }

        System.out.println("---------------");
        PageInfo<DepartmentlistVO> pageInfo = new PageInfo<>(list);
        System.out.println(pageInfo);
        System.out.println("数据总量：" + pageInfo.getTotal());
        System.out.println("总页数：" + pageInfo.getPages());

        System.out.println("---------------");
        PageData<DepartmentlistVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        System.out.println(pageData);
    }


    @Test
    void listByParent1() {
        Integer pageNum = 1; // 第几页
        Integer pageSize = 10; // 每页几条
        Long parentId = 0L;
        PageHelper.startPage(pageNum, pageSize); // 设置分页，注意：这句话必须直接出现在查询之前，否则可能导致线程安全问题
        List<DepartmentlistVO> list = mapper.listByParent1(parentId); // Page类型，包含了分页的相关数据，但不便于访问
        System.out.println(list.getClass().getName());
        for (Object item : list) {
            System.out.println(item);
        }

        System.out.println("---------------");
        PageInfo<DepartmentlistVO> pageInfo = new PageInfo<>(list);
        System.out.println(pageInfo);
        System.out.println("数据总量：" + pageInfo.getTotal());
        System.out.println("总页数：" + pageInfo.getPages());

        System.out.println("---------------");
        PageData<DepartmentlistVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        System.out.println(pageData);
    }



}
