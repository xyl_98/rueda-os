package cn.tedu.rueda.admin.departmentcare;

import cn.tedu.rueda.admin.departmentcare.dao.persist.repository.IDepartmentRepository;
import cn.tedu.rueda.admin.departmentcare.pojo.vo.DepartmentStandardVO;
import cn.tedu.rueda.common.pojo.vo.PageData;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;

@SpringBootTest
public class DepartmentRepositoryTests {

    @Autowired
    IDepartmentRepository repository;


    /*
    查询父级id不为0的数据
     */
    @Test
    void listByParent() {
        Long parentId=0L;
        Integer pageNum = 1;
        Integer pageSize = 10;
            PageData<?> pageData = repository.listByParent(parentId, pageNum, pageSize);
            System.out.println("页码：" + pageData.getPageNum());
            System.out.println("每页数据量：" + pageData.getPageSize());
            System.out.println("总数据量：" + pageData.getTotal());
            System.out.println("总页数：" + pageData.getMaxPage());
            System.out.println("列表：" + pageData.getList());
    }

    @Test
    void countTest(){
        int countByName = repository.countByName("急诊科");
        System.out.println(countByName);
    }

    @Test
    void byId(){
        DepartmentStandardVO standardById = repository.getStandardById(1L);
        System.out.println(standardById);
    }
}
