/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare;

import cn.tedu.rueda.common.util.ToolUtils;

import java.time.LocalDateTime;
import java.time.temporal.TemporalUnit;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * @class: Tests
 * @desc: 
 * @author wang-p
 * @date 2023/8/16
 * @version 1.0.0
 * @modify: 
 */
public class Tests {

    public static void main(String[] args) {
        // String str = "12345测试6789xa测试试x9876试54321xa_pr测ef试ix测试x_suffix_t测est_xa测试xxxx\nxxx测 试aaa";
        // // System.out.println(str.indexOf("测试x"));
        // // 未使用空白字符作为分隔符的情况下、会自动去除字符串中的空白字符（换行符、空格符）
        // // [12345, 试6789xa, 试试x9876试54321xa_pr, ef试ix, 试x_suffix_t, est_xa, 试xxxxxxx, 试aaa]
        // System.out.println(ToolUtils.split(str, "测", false));
        // // 未使用空白字符作为分隔符、但keepWhitespace=true的情况下、不会去除字符串中的空白字符（换行符、空格符）
        // // [12345, 试6789xa, 试试x9876试54321xa_pr, ef试ix, 试x_suffix_t, est_xa, 试xxxx\nxxx,  试aaa]
        // System.out.println(ToolUtils.split(str, "测", true));
        // // 使用了空白字符（空格符）作为分隔符的情况下、不会去除字符串中的空白字符（换行符保留）
        // // [12345测试6789xa测试试x9876试54321xa_pr测ef试ix测试x_suffix_t测est_xa测试xxxx\nxxx测, 试aaa]
        // System.out.println(ToolUtils.split(str, " ", false));
        // // 使用了空白字符（换行符）作为分隔符的情况下、不会去除字符串中的空白字符（空格符保留）
        // // [12345测试6789xa测试试x9876试54321xa_pr测ef试ix测试x_suffix_t测est_xa测试xxxx, xxx测 试aaa]
        // System.out.println(ToolUtils.split(str, "\n", false));

        // String regex = "^\\d{4}-\\d{2}-\\d{2} \\d{2}$";
        // String target = "2023-08-12 15";
        // System.out.println(target.matches(regex));
        // System.out.println(Pattern.matches(regex, target));

        System.out.println(LocalDateTime.now().plusDays(1L).getDayOfWeek().getValue());
    }

}
