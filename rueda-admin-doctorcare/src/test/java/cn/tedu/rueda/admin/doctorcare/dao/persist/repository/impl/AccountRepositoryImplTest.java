package cn.tedu.rueda.admin.doctorcare.dao.persist.repository.impl;

import cn.tedu.rueda.admin.doctorcare.dao.persist.repository.IAccountRepository;
import cn.tedu.rueda.common.pojo.entity.AccountUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


/**
 * @author wang-p
 * @version 1.0.0
 * @class: AccountRepositoryImplTest
 * @desc:
 * @date 2023/8/11
 * @modify:
 */
@Slf4j
@SpringBootTest
class AccountRepositoryImplTest {

    @Autowired
    private IAccountRepository repository;

    @Test
    public void addNewAccountTest() {
        AccountUser user = new AccountUser()
                .setUserId("511622199502241019")
                .setUsername("王鹏")
                .setCellPhone("18328816494")
                .setGender(0)
                .setAge(28)
                .setRoleId(1)
                .setState(0);
        Long accountId = repository.addNewAccount(user);
        log.debug("用户ID = {}", accountId);
    }
}