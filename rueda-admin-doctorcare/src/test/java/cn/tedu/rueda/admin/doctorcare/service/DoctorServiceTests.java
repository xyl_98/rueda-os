package cn.tedu.rueda.admin.doctorcare.service;

import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorInfoParam;
import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorUpdateInfoParam;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author rwjyy
 * @ClassName DoctorSericeTests
 * @description: TODO
 * @date 2023年08月05日
 * @version: 1.0
 */
@SpringBootTest
public class DoctorServiceTests {
    @Autowired
    IDoctorService doctorService;

    @Test
    void setState(){
        Long id = 1L;
        doctorService.setStateOff(id);
    }

    @Test
    public void addNewDoctorInfoTest(){
        DoctorInfoParam param = new DoctorInfoParam()
                .setName("X医生")
                .setCellPhone("14725836902")
                .setAge(28)
                .setGender(0)
                .setUserId("111122223333444456");
        doctorService.addNewDoctorInfo(param);
    }

    @Test
    public void update() {
        Long id = 7L;
        DoctorUpdateInfoParam doctorUpdateInfoParam = new DoctorUpdateInfoParam();
        doctorUpdateInfoParam.setId(id);
        doctorUpdateInfoParam.setConsultId(6L);
        doctorUpdateInfoParam.setCellPhone("13477778888");
        doctorService.updateInfoById(doctorUpdateInfoParam);

    }
}
