package cn.tedu.rueda.admin.doctorcare.dao.persist.mapper;

import cn.tedu.rueda.admin.doctorcare.pojo.entity.Doctor;
import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorUpdateInfoParam;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorStandardVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author rwjyy
 * @ClassName DoctorMapperTests
 * @description: TODO
 * @date 2023年08月04日
 * @version: 1.0
 */
@SpringBootTest
public class DoctorMapperTests {

    @Autowired
    DoctorMapper doctorMapper;

    @Test
    void getStandardById(){
        Long id = 1L;
        DoctorStandardVO standardById = doctorMapper.getStandardById(id);
        System.out.println("standardById = " + standardById);
    }


}
