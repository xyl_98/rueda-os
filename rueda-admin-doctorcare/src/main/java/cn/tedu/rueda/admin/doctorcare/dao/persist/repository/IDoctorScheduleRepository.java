/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.dao.persist.repository;

import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorScheduleInfoVO;
import cn.tedu.rueda.common.pojo.entity.DoctorSchedule;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @class: IDoctorScheduleRepository
 * @desc: 医生排班数据操作接口
 * @author wang-p
 * @date 2023/8/15
 * @version 1.0.0
 * @modify: 
 */
public interface IDoctorScheduleRepository {

    /** 获取未来7天以内的排班信息 */
    List<DoctorScheduleInfoVO> getScheduleInfoForWeek();

    /** 获取指定医生的历史排班信息 */
    List<DoctorScheduleInfoVO> getScheduleInfoForPast(Long accountId);

    /** 根据用户ID集合检索未来排班信息 */
    List<DoctorSchedule> getSchedulesForAccount(List<Long> accountIds);

    /** 根据指定条件查询排班信息 */
    List<DoctorScheduleInfoVO> getScheduleInfoForCondition(Long accountId, LocalDateTime current, LocalDateTime target);

    /** 根据ID集合查询排班信息 */
    List<DoctorSchedule> findByIdList(List<Long> idList);

    /** 新增排班信息 */
    int addSchedules(List<DoctorSchedule> schedules);

    /** 修改&删除排班信息 */
    int updateSchedule(DoctorSchedule schedule);

}
