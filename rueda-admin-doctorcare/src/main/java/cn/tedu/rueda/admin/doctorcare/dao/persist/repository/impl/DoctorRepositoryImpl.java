package cn.tedu.rueda.admin.doctorcare.dao.persist.repository.impl;

import cn.tedu.rueda.admin.doctorcare.dao.persist.mapper.DoctorMapper;
import cn.tedu.rueda.admin.doctorcare.dao.persist.repository.IDoctorRepository;
import cn.tedu.rueda.admin.doctorcare.pojo.entity.Doctor;
import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorUpdateInfoParam;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorInfoVO;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorStandardVO;
import cn.tedu.rueda.common.pojo.entity.DoctorInfo;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Slf4j
@Repository

public class DoctorRepositoryImpl implements IDoctorRepository {

    @Autowired
    private DoctorMapper doctorMapper;

//    @Override
//    public int update(Doctor doctor) {
//        return doctorMapper.updateById(doctor);
//    }

    @Override
    public int updateDoctor(Doctor doctor) {
        int rows = doctorMapper.update(doctor, Wrappers.<Doctor>lambdaUpdate()
                //更新科室id
                .set(doctor.getDepartmentId() != null, Doctor::getDepartmentId, doctor.getDepartmentId())
                //更新诊室id
                .set(doctor.getConsultId()!=null, Doctor::getConsultId,doctor.getConsultId())
                //更新关键词
                .set(doctor.getKeywords()!=null,Doctor::getKeywords,doctor.getKeywords())
                //更新医生简介
                .set(doctor.getDescription()!=null,Doctor::getDescription,doctor.getDescription())
                .setSql("UPDATE_TIME = NOW()")
                .eq(Doctor::getId,doctor.getId())
        );
        return rows;
    }


    @Override
    public DoctorStandardVO getStandardById(Long id) {
        return doctorMapper.getStandardById(id);
    }

    /**
     * 新增医生信息
     *
     * @param entity 医生信息
     * @return 新增数据条数
     */
    @Override
    public int addNewDoctorInfo(DoctorInfo entity) {
        log.debug("新增医生信息开始...");
        int rows = doctorMapper.addDoctorInfo(entity);
        log.debug("新增医生信息结束...新增数据条数：{}", rows);
        return rows;
    }

    /**
     * 检索所有医生数据
     *
     * @return 医生数据列表
     */
    @Override
    public List<DoctorInfoVO> getAllDoctorInfo() {
        log.debug("检索所有医生信息开始...");
        List<DoctorInfoVO> list = doctorMapper.selectAll();
        log.debug("检索所有医生信息结束...检索到数据条数：{}", list.size());
        return list;
    }

    /**
     * 检索指定医生数据
     *
     * @param userId 身份证号
     * @return 指定医生数据
     */
    @Override
    public DoctorInfoVO getDoctorInfo(String userId) {
        log.debug("检索指定医生信息开始、身份证号：{}", userId);
        DoctorInfoVO doctorInfo = doctorMapper.selectByUserId(userId);
        log.debug("检索指定医生信息结束、检索到的信息为：{}", doctorInfo);
        return doctorInfo;
    }

}
