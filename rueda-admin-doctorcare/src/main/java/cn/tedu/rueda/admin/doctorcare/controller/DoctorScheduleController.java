/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.controller;

import cn.tedu.rueda.admin.doctorcare.pojo.param.ScheduleInfoParam;
import cn.tedu.rueda.admin.doctorcare.pojo.param.ScheduleSearchParam;
import cn.tedu.rueda.admin.doctorcare.pojo.param.ScheduleUpdateParam;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorScheduleUpdateVO;
import cn.tedu.rueda.admin.doctorcare.service.IDoctorScheduleService;
import cn.tedu.rueda.admin.doctorcare.service.impl.AsyncService;
import cn.tedu.rueda.common.web.JsonResult;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @class: DoctorScheduleController
 * @desc: 医生排班管理Controller
 * @author wang-p
 * @date 2023/8/25
 * @version 1.0.0
 * @modify: 
 */
@Slf4j
@RestController
@RequestMapping("/doctor/schedule")
@Api(tags = "2.1 医生排班管理")
@Validated
public class DoctorScheduleController {

    @Autowired
    private IDoctorScheduleService service;
    @Autowired
    private AsyncService asyncService;

    @PostMapping("/add-new")
    @ApiOperation("新增排班信息")
    @ApiOperationSupport(order = 100)
    public JsonResult addSchedules(@RequestBody @Valid List<ScheduleInfoParam> request) throws Exception {
        List<DoctorScheduleUpdateVO> updateList = service.addScheduleRecords(request);
        // 新增排班信息后刷新本地缓存
        asyncService.refreshDoctorInfos();
        return JsonResult.ok(updateList);
    }

    @PostMapping("/update")
    @ApiOperation("更新排班信息")
    @ApiOperationSupport(order = 200)
    public JsonResult updateSchedule(@RequestBody @Valid List<ScheduleUpdateParam> request) throws Exception {
        service.updateRecord(request);
        // 更新排班信息后刷新本地缓存
        asyncService.refreshDoctorInfos();
        return JsonResult.ok();
    }

    @GetMapping("/search")
    @ApiOperation("检索排班信息")
    @ApiOperationSupport(order = 300)
    public JsonResult updateSchedule(@Valid ScheduleSearchParam request) {
        return JsonResult.ok(service.findByConditions(request));
    }

}
