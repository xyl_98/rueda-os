/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.pojo.vo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @class: DoctorDetailedInfoVO
 * @desc: 医生详细信息类
 * @author wang-p
 * @date 2023/8/11
 * @version 1.0.0
 * @modify:
 */
@Data
@ToString(callSuper = true)
public class DoctorDetailedInfoVO extends DoctorInfoVO {

    /** 序列化ID */
    private static final long serialVersionUID = -3158575669499030870L;
    /** 医生排班信息 */
    private List<DoctorScheduleInfoVO> schedules;

}
