package cn.tedu.rueda.admin.doctorcare.pojo.param;

import cn.tedu.rueda.common.constant.ValidateCode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class DoctorUpdateInfoParam implements Serializable {
    /**
     * 医生ID
     */
    private Long id;

    /**
     * 电话号码
     */
    @ApiModelProperty(value = "电话号码")
    private String cellPhone;

    /**
     * 所属科室ID
     */
    @ApiModelProperty(value = "科室ID")
    private Long departmentId;

    /**
     * 诊室ID
     */
    @ApiModelProperty(value = "诊室ID")
    private Long consultId;

    /** 擅长疾病 */
    @ApiModelProperty(value = "擅长疾病，（多个擅长类型以逗号隔开）")
    @Length(max = 200, message = ValidateCode.VALID_ZERO_TO_TWO_H)
    private String keywords;

    /** 医生说明 */
    @ApiModelProperty(value = "医生简介")
    @Length(max = 1000, message = ValidateCode.VALID_ZERO_TO_THOUSAND)
    private String description;

}
