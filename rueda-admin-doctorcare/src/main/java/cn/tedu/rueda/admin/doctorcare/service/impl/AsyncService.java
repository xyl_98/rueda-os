/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.service.impl;

import cn.tedu.rueda.admin.doctorcare.cache.LocalCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @class: AsyncService
 * @desc: 异步请求服务类
 * @author wang-p
 * @date 2023/8/15
 * @version 1.0.0
 * @modify: 
 */
@Service
public class AsyncService {

    @Autowired
    private LocalCache cache;

    /** 异步刷新医生信息 */
    @Async
    public void refreshDoctorInfos() throws Exception {
        cache.refreshDoctorInfo();
    }

}
