/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.pojo.param;

import cn.tedu.rueda.common.constant.Const;
import cn.tedu.rueda.common.constant.ValidateCode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @class: ScheduleUpdateParam
 * @desc: 排班更新参数
 * @author wang-p
 * @date 2023/8/18
 * @version 1.0.0
 * @modify: 
 */

@Data
@ToString
public class ScheduleUpdateParam implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = -255421469906829637L;
    /** 排班ID */
    @ApiModelProperty(value = "排班ID")
    @NotNull(message = ValidateCode.VALID_NOT_NULL)
    @Min(value = 1, message = ValidateCode.VALID_GREATER_ZERO)
    private Long id;
    /** 是否在班 */
    @ApiModelProperty(value = "是否在班（0：在班，1：休假）")
    @Range(max = 1, message = ValidateCode.VALID_MUST_ZERO_OR_ONE)
    private Integer isLeave;
    /** 审核状态 */
    @ApiModelProperty(value = "审核状态（1：审核通过，2：审核拒绝）")
    @Range(min = 1, max = 2, message = ValidateCode.VALID_MUST_ONE_OR_TWO)
    private Integer checkState;
    /** 审核说明 */
    @ApiModelProperty(value = "审核说明")
    @Length(max = 1000, message = ValidateCode.VALID_ZERO_TO_THOUSAND)
    private String checkDescription;
    /** 开始时间 */
    @ApiModelProperty(value = "排班开始时间")
    @Pattern(regexp = Const.PATTERN_SCHEDULE_TIME_FORMAT, message = ValidateCode.VALID_SCHEDULE_TIME_FORMAT)
    private String startTime;
    /** 结束时间 */
    @ApiModelProperty(value = "排班结束时间")
    @Pattern(regexp = Const.PATTERN_SCHEDULE_TIME_FORMAT, message = ValidateCode.VALID_SCHEDULE_TIME_FORMAT)
    private String endTime;

}
