/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.pojo.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * @class: DoctorScheduleUpdateVO
 * @desc: 
 * @author wang-p
 * @date 2023/8/25
 * @version 1.0.0
 * @modify: 
 */
@Data
@ToString
public class DoctorScheduleUpdateVO implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = -5035462578428134460L;
    /** 排班ID */
    @ApiModelProperty(value = "排班ID")
    private Long id;
    /** 医生用户ID */
    @ApiModelProperty(value = "医生用户ID")
    private Long doctorAccountId;
    /** 医生姓名 */
    @ApiModelProperty(value = "医生姓名")
    private String name;
    /** 排班开始时间 */
    @ApiModelProperty(value = "排班开始时间")
    private String startTime;
    /** 排班结束时间 */
    @ApiModelProperty(value = "排班结束时间")
    private String endTime;

}
