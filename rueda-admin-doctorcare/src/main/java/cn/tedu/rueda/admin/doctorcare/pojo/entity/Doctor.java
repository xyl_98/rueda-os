package cn.tedu.rueda.admin.doctorcare.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * 实体类：医生
 */
@Data
@TableName("rueda_doctor")
public class Doctor implements Serializable {
    /**
     * 医生编号ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 数据创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 数据最后修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 医生姓名
     */
    private String name;

    /**
     * 所属科室ID
     */
    private Long departmentId;

    /**
     * 诊室ID
     */
    private Long consultId;

    /**
     * 关键词
     */
    private String keywords;

    /**
     * 描述
     */
    private String description;

    /**
     * 状态
     */
    private Integer state;

}
