/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.dao.persist.repository;

import cn.tedu.rueda.common.pojo.entity.AccountUser;

/**
 * @class: IAccountRepository
 * @desc: 用户信数据操作接口
 * @author wang-p
 * @date 2023/8/11
 * @version 1.0.0
 * @modify: 
 */
public interface IAccountRepository {

    /** 根据身份证号判断是否用户数据 */
    AccountUser getAccountIdByUserId(String userId);

    /** 新增用户信息 */
    Long addNewAccount(AccountUser user);

    /** 更行用户角色和状态 */
    int updateRoleIdAndState(AccountUser entity);

    /**
     * 更新用户数据
     */
    int updateUser(AccountUser accountUser);

}
