package cn.tedu.rueda.admin.doctorcare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication(scanBasePackages = "cn.tedu")
@EnableAsync // 启动异步方法
@EnableScheduling // 启动定时任务
public class RuedaAdminDoctorcareApplication {

    public static void main(String[] args) {
        SpringApplication.run(RuedaAdminDoctorcareApplication.class, args);
    }

}
