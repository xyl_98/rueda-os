/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.pojo.param;

import cn.tedu.rueda.common.constant.Const;
import cn.tedu.rueda.common.constant.ValidateCode;
import cn.tedu.rueda.common.pojo.entity.DoctorSchedule;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @class: ScheduleInfoParam
 * @desc: 新增排班信息
 * @author wang-p
 * @date 2023/8/18
 * @version 1.0.0
 * @modify:
 */

@Data
@ToString
public class ScheduleInfoParam implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = -7074688306355052377L;
    /** 医生用户ID */
    @ApiModelProperty(value = "医生用户ID")
    @NotNull(message = ValidateCode.VALID_NOT_NULL)
    @Min(value = 1, message = ValidateCode.VALID_GREATER_ZERO)
    private Long doctorAccountId;
    /** 医生姓名 */
    @ApiModelProperty(value = "医生姓名")
    private String name;
    /** 排班开始时间 */
    @ApiModelProperty(value = "排班开始时间")
    @NotNull(message = ValidateCode.VALID_NOT_NULL)
    @Pattern(regexp = Const.PATTERN_SCHEDULE_TIME_FORMAT, message = ValidateCode.VALID_SCHEDULE_TIME_FORMAT)
    private String startTime;
    /** 排班结束时间 */
    @ApiModelProperty(value = "排班结束时间")
    @NotNull(message = ValidateCode.VALID_NOT_NULL)
    @Pattern(regexp = Const.PATTERN_SCHEDULE_TIME_FORMAT, message = ValidateCode.VALID_SCHEDULE_TIME_FORMAT)
    private String endTime;
    // TODO 后续再考虑是否实现该功能
    /** 是否允许过期数据登录 */
    // private Integer allowExpire;
    /** 已登录排班信息（数据库检索结果） */
    @ApiModelProperty(hidden = true)
    private DoctorSchedule schedule;

}
