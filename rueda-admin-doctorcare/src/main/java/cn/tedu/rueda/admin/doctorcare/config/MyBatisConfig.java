package cn.tedu.rueda.admin.doctorcare.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("cn.tedu.rueda.admin.doctorcare.dao.persist.mapper")
public class MyBatisConfig {
}
