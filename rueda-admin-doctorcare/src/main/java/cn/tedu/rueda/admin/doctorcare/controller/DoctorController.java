package cn.tedu.rueda.admin.doctorcare.controller;

import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorInfoParam;
import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorSearchParam;
import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorUpdateInfoParam;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorStandardVO;
import cn.tedu.rueda.admin.doctorcare.service.IDoctorService;
import cn.tedu.rueda.admin.doctorcare.service.impl.AsyncService;
import cn.tedu.rueda.common.web.JsonResult;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/doctor/")
@Api(tags = "2. 医生管理")
@Validated
public class DoctorController {

    @Autowired
    private IDoctorService doctorService;
    @Autowired
    private AsyncService asyncService;

    @PostMapping("/add-new")
    @ApiOperation("新增医生信息")
    @ApiOperationSupport(order = 100)
    public JsonResult addNewDoctorInfo(@RequestBody @Valid DoctorInfoParam request) throws Exception {
        // 新增医生信息
        doctorService.addNewDoctorInfo(request);
        // 异步刷新缓存
        asyncService.refreshDoctorInfos();
        return JsonResult.ok();
    }

    @GetMapping(value = "/search")
    @ApiOperation("检索医生信息")
    @ApiOperationSupport(order = 200)
    public JsonResult searchDoctorInfo(@Valid DoctorSearchParam request) throws Exception {
        return JsonResult.ok(doctorService.getDoctorInfos(request));
    }

    @PostMapping("/update")
    @ApiOperation("修改医生信息")
    @ApiOperationSupport(order = 300)
    public JsonResult update(@RequestBody @Valid DoctorUpdateInfoParam doctorUpdateInfoParam) throws Exception {
        doctorService.updateInfoById(doctorUpdateInfoParam);
        // 异步刷新缓存
        asyncService.refreshDoctorInfos();
        return JsonResult.ok();
    }

    @PostMapping("/{id:[0-9]+}/deleteById")
    @ApiOperation("删除医生信息")
    @ApiOperationSupport(order = 400)
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "医生ID", required = true, dataType = "long")
    )
    public JsonResult deleteById(@Range(min = 1, message = "请提交合法的ID值") @PathVariable Long id) throws Exception {
        log.debug("开始处理【不存在状态】的请求，参数：{}", id);
        doctorService.setStateOff(id);
        // 异步刷新缓存
        asyncService.refreshDoctorInfos();
        return JsonResult.ok();
    }

    @GetMapping("/{id:[0-9]+}")
    @ApiOperation("根据ID查询医生")
    @ApiOperationSupport(order = 310)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "医生ID", required = true, dataType = "long")
    })
    public JsonResult getStandardById(
            @PathVariable @Range(min = 1, message = "请提交有效的类别ID值！") Long id) throws Exception {
        log.debug("开始处理【根据ID查询类别】的请求，参数：{}", id);
        DoctorStandardVO queryResult = doctorService.getStandardById(id);
        // 异步刷新缓存
        asyncService.refreshDoctorInfos();
        return JsonResult.ok(queryResult);
    }

}
