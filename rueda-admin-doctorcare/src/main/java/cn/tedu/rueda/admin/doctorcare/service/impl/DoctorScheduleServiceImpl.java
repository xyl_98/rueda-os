/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.service.impl;

import cn.tedu.rueda.admin.doctorcare.dao.persist.repository.IDoctorScheduleRepository;
import cn.tedu.rueda.admin.doctorcare.pojo.param.ScheduleInfoParam;
import cn.tedu.rueda.admin.doctorcare.pojo.param.ScheduleSearchParam;
import cn.tedu.rueda.admin.doctorcare.pojo.param.ScheduleUpdateParam;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorScheduleInfoVO;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorScheduleUpdateVO;
import cn.tedu.rueda.admin.doctorcare.service.IDoctorScheduleService;
import cn.tedu.rueda.common.constant.Const;
import cn.tedu.rueda.common.constant.MessageCode;
import cn.tedu.rueda.common.constant.ValidateCode;
import cn.tedu.rueda.common.enumerator.ServiceCode;
import cn.tedu.rueda.common.ex.ServiceException;
import cn.tedu.rueda.common.pojo.entity.DoctorSchedule;
import cn.tedu.rueda.common.util.MessageUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @class: DoctorScheduleServiceImpl
 * @desc: 医生排班操作Service
 * @author wang-p
 * @date 2023/8/18
 * @version 1.0.0
 * @modify: 
 */
@Slf4j
@Service
public class DoctorScheduleServiceImpl implements IDoctorScheduleService {

    @Autowired
    private IDoctorScheduleRepository repository;

    /**
     * 新增排班记录（可批量新增）
     * @param schedules 排班记录
     * @return
     */
    @Override
    public List<DoctorScheduleUpdateVO> addScheduleRecords(List<ScheduleInfoParam> schedules) {
        // 获取医生用户ID集合
        List<Long> accountIds = schedules.stream().map(s -> s.getDoctorAccountId()).collect(Collectors.toList());
        // 根据医生用户ID集合读取已存在且未过期的排班信息
        List<DoctorSchedule> exists = repository.getSchedulesForAccount(accountIds);
        // 过滤出已存在且未过期的排班信息
        List<ScheduleInfoParam> updateList = schedules.stream()
                .filter(s -> {
                    boolean match = false;
                    DoctorSchedule schedule = exists.stream()
                            .filter(e -> s.getDoctorAccountId() == e.getDoctorAccountId()
                                    && s.getStartTime().equals(Const.SCHEDULE_FORMAT.format(e.getStartTime())))
                            .findFirst().orElse(null);
                    // 保存数据库检索到的数据
                    s.setSchedule(schedule);
                    // 数据库匹配结果不为null
                    if (schedule != null)
                        match = true;
                    return match;
                })
                .collect(Collectors.toList());

        // 处理待插入数据
        List<DoctorSchedule> addInfos = schedules.stream()
                // 过滤掉已存在且未过期的排班信息
                .filter(s -> !updateList.stream().anyMatch(u -> u.equals(s)))
                // 过滤掉无效（当前时间已经超过排班开始时间）的数据
                .filter(s -> LocalDateTime.now().isBefore(LocalDateTime.parse(s.getStartTime(), Const.SCHEDULE_FORMAT)))
                // 编辑排班信息
                .map(s -> {
                    DoctorSchedule schedule = new DoctorSchedule();
                    // 医生用户ID
                    schedule.setDoctorAccountId(s.getDoctorAccountId());
                    // 排班开始时间
                    schedule.setStartTime(LocalDateTime.parse(s.getStartTime(), Const.SCHEDULE_FORMAT));
                    // 排班结束时间
                    schedule.setEndTime(LocalDateTime.parse(s.getEndTime(), Const.SCHEDULE_FORMAT));
                    // 工作日
                    schedule.setScheduleId(schedule.getStartTime().getDayOfWeek().getValue());
                    // 是否在班（默认、0：在班）
                    schedule.setIsLeave(Const.DEFAULT_ZERO);
                    // 审核状态（默认、0：未审核）
                    schedule.setCheckState(Const.DEFAULT_ZERO);
                    // 更新者（默认：system）
                    schedule.setUpdateUser(Const.DEFAULT_USER);
                    // 创建者（默认：system）
                    schedule.setCreateUser(Const.DEFAULT_USER);
                    return schedule;
                })
                .collect(Collectors.toList());

        // 校验是否存在"排班开始时间"在"排班结束时间"之后的数据
        boolean logicError = addInfos.stream().anyMatch(s -> !s.getStartTime().isBefore(s.getEndTime()));
        if (logicError)
            throw new ServiceException(ServiceCode.ERROR_CONFLICT,
                    MessageUtils.getMessage(MessageCode.SCHEDULE_START_AFTER_END));
        // 编辑要返回的待更新数据
        List<DoctorScheduleUpdateVO> updateInfos = updateList.stream()
                .map(s -> {
                    DoctorScheduleUpdateVO update = new DoctorScheduleUpdateVO();
                    BeanUtils.copyProperties(s, update);
                    // 获取排班ID
                    update.setId(s.getSchedule().getId());
                    return update;
                }).collect(Collectors.toList());

        // 传入的数据无待新增数据
        if (addInfos == null || addInfos.size() == 0)
            throw new ServiceException(ServiceCode.ERROR_CONFLICT,
                    MessageUtils.getMessage(MessageCode.SCHEDULE_NOT_NEW), updateInfos);
        // 执行数据插入
        int rows = repository.addSchedules(addInfos);
        // 数据插入失败
        if (rows <= 0)
            throw new ServiceException(ServiceCode.ERROR_INSERT,
                    MessageUtils.getMessage(MessageCode.SCHEDULE_INSERT_FAIL));
        // 返回待更新对象
        return updateInfos;
    }

    /**
     * 修改排班信息
     * @param updates 待修改排班信息集合
     */
    @Override
    public void updateRecord(List<ScheduleUpdateParam> updates) {
        List<Long> idList = updates.stream().map(s -> s.getId()).collect(Collectors.toList());
        // 传入数据排班ID重复检查
        if (idList.stream().distinct().count() != updates.size())
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST,
                MessageUtils.getMessage(MessageCode.SCHEDULE_NOT_REPEAT));
        // 检索对应的排班信息集合
        List<DoctorSchedule> exists = repository.findByIdList(idList);
        // 传入的待更新数据ID与数据库不匹配
        if (exists.size() != updates.size())
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST,
                    MessageUtils.getMessage(MessageCode.SCHEDULE_UNKNOWN_RECORD));
        // 无需更新的排班信息集合
        List<ScheduleUpdateParam> invalidList = new ArrayList<>();
        // 循环更新排班数据
        updates.stream().forEach(update -> {
            // 数据库数据
            DoctorSchedule current = exists.stream().filter(s -> s.getId() == update.getId()).findFirst().get();
            boolean isUpdate = false;
            DoctorSchedule schedule = new DoctorSchedule();
            schedule.setId(update.getId());
            // 请假
            if (update.getIsLeave() != null && current.getIsLeave() != update.getIsLeave()){
                isUpdate = true;
                schedule.setIsLeave(update.getIsLeave());
            }
            // 审核排班
            if (update.getCheckState() != null && current.getCheckState() != update.getCheckState()) {
                isUpdate = true;
                // 审核状态
                schedule.setCheckState(update.getCheckState());
                // TODO 待整合security之后处理
                // 审核人
                // schedule.setCheckName("");
                // 审核描述
                schedule.setCheckDescription(update.getCheckDescription());
            }
            // 时间类型数据格式转换并校验
            LocalDateTime endTime = null;
            LocalDateTime startTime = null;
            LocalDateTime now = LocalDateTime.now();
            if (StringUtils.hasText(update.getEndTime())) {
                endTime = LocalDateTime.parse(update.getEndTime(), Const.SCHEDULE_FORMAT);
                current.setEndTime(endTime);
            }
            if (StringUtils.hasText(update.getStartTime())) {
                startTime = LocalDateTime.parse(update.getStartTime(), Const.SCHEDULE_FORMAT);
                current.setStartTime(startTime);
            }
            // 更新"排班结束时间"
            // 待更新的"排班结束时间"在当前时间且在"排班开始时间"之后才允许更新
            if (endTime!= null && endTime.isAfter(now) && endTime.isAfter(current.getStartTime())) {
                isUpdate = true;
                schedule.setEndTime(endTime);
            }
            // 更新"排班开始时间"
            // 待更新的"排班开始时间"在当前时间之后且在"排班结束时间"之前才允许更新
            if (startTime != null && startTime.isAfter(now) && startTime.isBefore(current.getEndTime()) ) {
                isUpdate = true;
                schedule.setStartTime(startTime);
            }

            // 执行数据更新
            if (isUpdate) {
                // TODO 待整合security之后处理
                // 更新者设置
                // schedule.setUpdateUser("");
                int rows = repository.updateSchedule(schedule);
                // 数据更新失败
                if (rows <= 0)
                    throw new ServiceException(ServiceCode.ERROR_UPDATE,
                            MessageUtils.getMessage(MessageCode.SCHEDULE_UPDATE_FAIL));
            } else {
                invalidList.add(update); // 无需更新的数据
            }
        });

        // 无效的更新请求（所有数据皆无需更新）
        if (invalidList.size() == updates.size())
            throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST,
                    MessageUtils.getMessage(MessageCode.SCHEDULE_UPDATE_INVALID));

    }

    /**
     * 根据指定条件查找排班信息
     * @param search 指定条件
     * @return 检索到的排班信息
     */
    @Override
    public List<DoctorScheduleInfoVO> findByConditions(ScheduleSearchParam search) {
        List<DoctorScheduleInfoVO> result = new ArrayList<>();
        // 检索历史排班
        if(Const.FLAG_TRUE == search.getSearchHistory()) {
            // 检索历史排班必须指定医生用户ID
            if (search.getDoctorAccountId() == null)
                throw new ServiceException(ServiceCode.ERROR_BAD_REQUEST,
                        MessageUtils.getMessage(ValidateCode.VALID_NOT_NULL, "doctorAccountId"));
            // 检索历史排班数据
            result = repository.getScheduleInfoForPast(search.getDoctorAccountId());
        } else {
            // 其他条件检索
            LocalDateTime current = null;
            LocalDateTime target = null;
            // 指定检索起始时间
            if (StringUtils.hasText(search.getSearchStartTime()))
                current = LocalDate.parse(search.getSearchStartTime(), Const.SEARCH_FORMAT).atTime(LocalTime.MIDNIGHT);
            // 指定检索结束时间
            if (StringUtils.hasText(search.getSearchEndTime()))
                target = LocalDate.parse(search.getSearchEndTime(), Const.SEARCH_FORMAT).atTime(LocalTime.MIDNIGHT);
            // 若所有条件均不指定、则检索全部排班信息
            result = repository.getScheduleInfoForCondition(search.getDoctorAccountId(), current, target);
        }
        // 未检索到数据
        if (result == null || result.size() == 0)
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,
                    MessageUtils.getMessage(MessageCode.SCHEDULE_NOT_FOUND));

        return result;
    }

}
