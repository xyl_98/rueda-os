package cn.tedu.rueda.admin.doctorcare.service;

import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorInfoParam;
import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorSearchParam;
import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorUpdateInfoParam;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorDetailedInfoVO;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorStandardVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional(rollbackFor = Throwable.class)
public interface IDoctorService {

    //int STATE_ON = 0;
    int STATE_OFF = 1;

    /**
     * 禁用状态，根据ID删除数据，此删除为逻辑删除，修改state的值即可
     */
    void setStateOff(Long id);

    /**
     * 修改数据
     */
    void updateInfoById(DoctorUpdateInfoParam doctorUpdateInfoParam);

    /** 新增医生信息 */
    void addNewDoctorInfo(DoctorInfoParam param);

    /** 检索医生信息 */
    List<DoctorDetailedInfoVO> getDoctorInfos(DoctorSearchParam param);

    /** 根据id查询医生信息 */
    DoctorStandardVO getStandardById(Long id);

}
