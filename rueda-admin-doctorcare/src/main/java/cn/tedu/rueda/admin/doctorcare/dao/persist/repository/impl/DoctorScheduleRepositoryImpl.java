/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.dao.persist.repository.impl;

import cn.tedu.rueda.admin.doctorcare.dao.persist.mapper.DoctorScheduleMapper;
import cn.tedu.rueda.admin.doctorcare.dao.persist.repository.IDoctorScheduleRepository;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorScheduleInfoVO;
import cn.tedu.rueda.common.pojo.entity.DoctorSchedule;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

/**
 * @class: DoctorScheduleRepositoryImpl
 * @desc: 医生排班数据操作类
 * @author wang-p
 * @date 2023/8/15
 * @version 1.0.0
 * @modify:
 */
@Slf4j
@Repository
public class DoctorScheduleRepositoryImpl implements IDoctorScheduleRepository {

    @Autowired
    private DoctorScheduleMapper mapper;

    /**
     * 获取未来7天以内的排班信息
     * @return 排班信息集合
     */
    @Override
    public List<DoctorScheduleInfoVO> getScheduleInfoForWeek() {
        LocalDateTime currentTime = LocalDateTime.now().with(LocalTime.MIDNIGHT);
        LocalDateTime lastTime = currentTime.plusDays(7);
        log.debug("检索未来7天排班信息开始、当日时间：{}", currentTime);
        List<DoctorScheduleInfoVO> list = mapper.selectAllForWeek(currentTime, lastTime);
        log.debug("检索未来7天排班信息结束、检索到数据条数：{}", list.size());
        return list;
    }

    /**
     * 获取指定医生的历史排班信息
     * @param accountId 医生用户ID
     * @return 排班信息集合
     */
    @Override
    public List<DoctorScheduleInfoVO> getScheduleInfoForPast(Long accountId) {
        LocalDateTime currentTime = LocalDateTime.now().with(LocalTime.MIDNIGHT);
        log.debug("检索指定医生的历史排班信息开始、医生用户ID：{},当日时间：{}", accountId, currentTime);
        List<DoctorScheduleInfoVO> list = mapper.selectAllForPast(currentTime, accountId);
        log.debug("检索指定医生的历史排班信息结束、检索到数据条数：{}", list.size());
        return list;
    }

    /**
     * 获取指定医生集合的未来排班信息
     * @param accountIds 指定的医生用户ID集合
     * @return 排班信息集合
     */
    @Override
    public List<DoctorSchedule> getSchedulesForAccount(List<Long> accountIds) {
        LocalDateTime currentTime = LocalDateTime.now().with(LocalTime.MIDNIGHT);
        log.debug("获取指定医生ID集合的未来排班信息开始、医生用户ID集合：{},当日时间：{}", accountIds, currentTime);
        List<DoctorSchedule> list = mapper.selectAllForAccounts(currentTime, accountIds);
        log.debug("获取指定医生ID集合的未来排班信息结束、检索到数据条数：{}", list.size());
        return list;
    }

    /**
     * 根据指定条件查询排班信息
     * @param accountId 医生用户ID
     * @param current 指定开始时间
     * @param target 指定结束时间
     * @return 排班信息集合
     */
    @Override
    public List<DoctorScheduleInfoVO> getScheduleInfoForCondition(Long accountId, LocalDateTime current, LocalDateTime target) {
        log.debug("根据指定条件查询排班信息开始、条件-用户ID：{}，指定开始时间：{}，指定结束时间：{}", accountId, current, target);
        List<DoctorScheduleInfoVO> list = mapper.selectForConditions(accountId, current, target);
        log.debug("根据指定条件查询排班信息结束、检索到数据条数：{}", list.size());
        return list;
    }

    /**
     * 根据ID集合查询排班信息
     * @param idList ID集合
     * @return 排班信息集合
     */
    @Override
    public List<DoctorSchedule> findByIdList(List<Long> idList) {
        log.debug("根据ID集合查询排班信息开始、ID-LIST：", idList);
        List<DoctorSchedule> list = mapper.selectList(Wrappers.<DoctorSchedule>lambdaQuery()
                .in(idList != null && idList.size() > 0, DoctorSchedule::getId, idList));
        log.debug("根据ID集合查询排班信息结束、检索到数据条数：{}", list.size());
        return list;
    }

    /**
     * 新增排班信息
     * @param schedules 待新增排班信息集合
     * @return 新增数据条数
     */
    @Override
    public int addSchedules(List<DoctorSchedule> schedules) {
        log.debug("新增排班信息开始、待新增数据条数：{}", schedules.size());
        int rows = mapper.insertList(schedules);
        log.debug("新增排班信息结束、实际新增数据条数：{}", rows);
        return rows;
    }

    /**
     * 修改排班信息
     * @param schedule 待修改排班信息
     * @return 修改数据条数
     */
    @Override
    public int updateSchedule(DoctorSchedule schedule) {
        log.debug("更新排班信息开始、待更新数据：{}", schedule);
        int rows = mapper.update(schedule, Wrappers.<DoctorSchedule>lambdaUpdate()
                // 更新审核状态
                .set(schedule.getCheckState() != null, DoctorSchedule::getCheckState, schedule.getCheckState())
                // 更新审核者姓名
                .set(StringUtils.hasText(schedule.getCheckName()), DoctorSchedule::getCheckName, schedule.getCheckName())
                // 更新审核时间
                .setSql(schedule.getCheckState() != null, "CHECK_TIME = NOW()")
                // 更新审核描述
                .set(StringUtils.hasText(schedule.getCheckDescription()),
                        DoctorSchedule::getCheckDescription, schedule.getCheckDescription())
                // 更新是否在班状态
                .set(schedule.getIsLeave() != null, DoctorSchedule::getIsLeave, schedule.getIsLeave())
                // 更新排班工作日
                .set(schedule.getScheduleId() != null, DoctorSchedule::getScheduleId, schedule.getScheduleId())
                // 更新排班开始时间
                .set(schedule.getStartTime() != null, DoctorSchedule::getStartTime, schedule.getStartTime())
                // 更新排班结束时间
                .set(schedule.getEndTime() != null, DoctorSchedule::getEndTime, schedule.getEndTime())
                .setSql("UPDATE_TIME = NOW()")
                .eq(DoctorSchedule::getId, schedule.getId()));
        log.debug("更新排班信息结束、实际新增数据条数：{}", rows);
        return rows;
    }

}
