/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.pojo.vo;

import cn.tedu.rueda.common.constant.Const;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @class: DoctorScheduleInfoVO
 * @desc: 医生排班信息
 * @author wang-p
 * @date 2023/8/14
 * @version 1.0.0
 * @modify: 
 */
@Data
@ToString
public class DoctorScheduleInfoVO implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = -8537394427166472561L;
    /** 排班ID */
    @ApiModelProperty(value = "排班ID")
    private Long id;
    /** 医生用户ID */
    @ApiModelProperty(value = "医生用户ID")
    private Long doctorAccountId;
    /** 工作日 */
    @ApiModelProperty(value = "工作日")
    private Integer scheduleId;
    /** 开始时间 */
    @ApiModelProperty(value = "排班开始时间")
    @JsonFormat(pattern = Const.DEFAULT_TIME_FORMAT)
    private LocalDateTime startTime;
    /** 结束时间 */
    @JsonFormat(pattern = Const.DEFAULT_TIME_FORMAT)
    @ApiModelProperty(value = "排班结束时间")
    private LocalDateTime endTime;
    /** 是否在班 */
    @ApiModelProperty(value = "是否在班")
    private Integer isLeave;
    /** 审核状态 */
    @ApiModelProperty(value = "审核状态")
    private Integer checkState;
    /** 审核者 */
    @ApiModelProperty(value = "审核者")
    private String checkName;
    /** 审核时间 */
    @ApiModelProperty(value = "审核时间")
    @JsonFormat(pattern = Const.DEFAULT_TIME_FORMAT)
    private LocalDateTime checkTime;
    /** 审核说明 */
    @ApiModelProperty(value = "审核说明")
    private String checkDescription;
    /** 更新者 */
    @ApiModelProperty(value = "更新者")
    private String updateUser;
    /** 创建者 */
    @ApiModelProperty(value = "创建者")
    private String createUser;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = Const.DEFAULT_TIME_FORMAT)
    private LocalDateTime updateTime;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = Const.DEFAULT_TIME_FORMAT)
    private LocalDateTime createTime;

}
