/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.pojo.param;

import cn.tedu.rueda.common.constant.Const;
import cn.tedu.rueda.common.constant.ValidateCode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @class: ScheduleSearchParam
 * @desc: 检索排班信息条件
 * @author wang-p
 * @date 2023/8/18
 * @version 1.0.0
 * @modify: 
 */
@Data
@ToString
public class ScheduleSearchParam implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = -374743090443010524L;
    /** 医生用户ID */
    @ApiModelProperty(value = "指定检索医生用户ID")
    @Min(value = 1, message = ValidateCode.VALID_GREATER_ZERO)
    private Long doctorAccountId;
    /** 指定检索开始时间 */
    @ApiModelProperty(value = "指定检索开始时间")
    @Pattern(regexp = Const.PATTERN_SEARCH_TIME_FORMAT, message = ValidateCode.VALID_SEARCH_TIME_FORMAT)
    private String searchStartTime;
    /** 指定检索结束时间 */
    @ApiModelProperty(value = "指定检索结束时间")
    @Pattern(regexp = Const.PATTERN_SEARCH_TIME_FORMAT, message = ValidateCode.VALID_SEARCH_TIME_FORMAT)
    private String searchEndTime;
    /** 指定检索区分，1：检索历史排班，0：其他检索方式 */
    @ApiModelProperty(value = "指定是否检索历史排班，1：检索历史排班，0：检索其他")
    @Range(max = 1,message = ValidateCode.VALID_MUST_ZERO_OR_ONE)
    private Integer searchHistory;

}
