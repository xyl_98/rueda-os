/*
 * Copyright © 2023 Reda-register All Rights Reserved
 */
package cn.tedu.rueda.admin.doctorcare.dao.persist.mapper;

import cn.tedu.rueda.common.pojo.entity.AccountUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @ClassName: AccountMapper
 * @Description: 用户数据相关处理
 * @Author Wang
 * @Date 2023/8/5
 * @Version 1.0.0
 * @ModifiedBy:
 */
@Repository
public interface AccountMapper extends BaseMapper<AccountUser> {
}
