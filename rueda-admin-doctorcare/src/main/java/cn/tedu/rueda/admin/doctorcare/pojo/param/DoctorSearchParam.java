/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.pojo.param;

import cn.tedu.rueda.common.constant.ValidateCode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.Min;
import java.io.Serializable;

/**
 * @class: DoctorSearchParam
 * @desc: 检索医生信息类
 * @author wang-p
 * @date 2023/8/11
 * @version 1.0.0
 * @modify:
 */
@Data
@ToString
public class DoctorSearchParam implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = -3573475582570607816L;
    /** 医生ID */
    @ApiModelProperty(value = "医生ID（指定检索医生信息）")
    @Min(value = 1, message = ValidateCode.VALID_GREATER_ZERO)
    private Long id;
    /** 所属科室ID */
    @ApiModelProperty(value = "所属科室ID（根据科室检索医生信息）")
    @Min(value = 1, message = ValidateCode.VALID_GREATER_ZERO)
    private Long departmentId;
    /** 所属诊室ID */
    @ApiModelProperty(value = "所属诊室ID（根据诊室检索医生信息）")
    @Min(value = 1, message = ValidateCode.VALID_GREATER_ZERO)
    private Long consultId;
    /** 医生姓名 */
    @ApiModelProperty(value = "医生姓名（根据医生姓名检索对应的医生信息）")
    private String doctorName;
    /** 擅长疾病 */
    @ApiModelProperty(value = "擅长疾病（根据病症检索对应医生信息）")
    private String keywords;


}
