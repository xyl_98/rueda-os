/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.dao.persist.mapper;

import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorScheduleInfoVO;
import cn.tedu.rueda.common.pojo.entity.DoctorSchedule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @class: DoctorScheduleMapper
 * @desc: 医生排班数据相关处理
 * @author wang-p
 * @date 2023/8/14
 * @version 1.0.0
 * @modify:
 */
@Repository
public interface DoctorScheduleMapper extends BaseMapper<DoctorSchedule> {

    /** 检索未来一周以内的医生排班信息 */
    List<DoctorScheduleInfoVO> selectAllForWeek(@Param("currentTime") LocalDateTime currentTime,
                                                @Param("lastTime") LocalDateTime lastTime);

    /** 检索指定医生的历史排班信息 */
    List<DoctorScheduleInfoVO> selectAllForPast(@Param("currentTime") LocalDateTime currentTime,
                                                @Param("accountId") Long accountId);

    /** 获取指定医生集合的未来排班信息 */
    List<DoctorSchedule> selectAllForAccounts(@Param("currentTime") LocalDateTime currentTime,
                                              @Param("accountIds") List<Long> accountIds);

    /** 根据条件检索排班信息 */
    List<DoctorScheduleInfoVO> selectForConditions(@Param("accountId") Long accountId,
                                                   @Param("current") LocalDateTime current,
                                                   @Param("target") LocalDateTime target);

    /** 新增排班信息 */
    int insertList(List<DoctorSchedule> schedules);

}
