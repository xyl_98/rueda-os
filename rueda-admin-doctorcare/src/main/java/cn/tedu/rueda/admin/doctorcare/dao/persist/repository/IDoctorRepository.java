package cn.tedu.rueda.admin.doctorcare.dao.persist.repository;

import cn.tedu.rueda.admin.doctorcare.pojo.entity.Doctor;
import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorUpdateInfoParam;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorInfoVO;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorStandardVO;
import cn.tedu.rueda.common.pojo.entity.DoctorInfo;

import java.util.List;

public interface IDoctorRepository {
    /**
     * 根据ID修改信息
     */
    //int update(Doctor doctor);

    int updateDoctor(Doctor doctor);

    /**
     * 根据ID查询信息
     */
    DoctorStandardVO getStandardById(Long id);

    /** 新增医生信息 */
    int addNewDoctorInfo(DoctorInfo entity);

    /** 检索所有医生数据 */
    List<DoctorInfoVO> getAllDoctorInfo();

    /** 检索指定医生数据 */
    DoctorInfoVO getDoctorInfo(String userId);

}
