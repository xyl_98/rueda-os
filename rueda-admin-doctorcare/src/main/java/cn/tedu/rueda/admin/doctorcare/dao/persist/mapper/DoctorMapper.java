package cn.tedu.rueda.admin.doctorcare.dao.persist.mapper;

import cn.tedu.rueda.admin.doctorcare.pojo.entity.Doctor;
import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorUpdateInfoParam;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorInfoVO;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorStandardVO;
import cn.tedu.rueda.common.pojo.entity.DoctorInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 医生信息数据处理接口
 */
@Repository
public interface DoctorMapper extends BaseMapper<Doctor> {

    /**
     * 根据ID查询数据详情
     * @param id 医生id
     * @return 匹配的医生数据详情，如果没有则返回null
     */
    DoctorStandardVO getStandardById(Long id);

    /** 新增医生信息 */
    int addDoctorInfo(DoctorInfo entity);

    /** 检索所有医生信息 */
    List<DoctorInfoVO> selectAll();

    /** 检索指定医生信息 */
    DoctorInfoVO selectByUserId(String userId);

}
