/*
 * Copyright © 2023 Reda-register All Rights Reserved
 */
package cn.tedu.rueda.admin.doctorcare.pojo.vo;

import cn.tedu.rueda.common.constant.Const;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @ClassName: DoctorInfoVO
 * @Description: 医生情报
 * @Author Wang
 * @Date 2023/8/5
 * @Version 1.0.0
 * @ModifiedBy:
 */
@Data
@ToString
public class DoctorInfoVO implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = -8306598988111940432L;
    /** 医生ID */
    @ApiModelProperty(value = "医生ID")
    private Long id;
    /** 医生姓名 */
    @ApiModelProperty(value = "医生姓名")
    private String name;
    /** 用户ID */
    @ApiModelProperty(value = "用户ID")
    private Long accountId;
    /** 身份证号 */
    @ApiModelProperty(value = "身份证号")
    private String userId;
    /** 电话号码 */
    @ApiModelProperty(value = "电话号码")
    private String cellPhone;
    /** 性别 */
    @ApiModelProperty(value = "性别")
    private Integer gender;
    /** 年龄 */
    @ApiModelProperty(value = "年龄")
    private Integer age;
    /** 所属科室ID */
    @ApiModelProperty(value = "所属科室ID")
    private Long departmentId;
    /** 所属科室 */
    @ApiModelProperty(value = "所属科室")
    private String departmentName;
    /** 所属诊室ID */
    @ApiModelProperty(value = "所属诊室ID")
    private Long consultId;
    /** 所属诊室 */
    @ApiModelProperty(value = "所属诊室")
    private String consultName;
    /** 擅长疾病 */
    @ApiModelProperty(value = "擅长疾病")
    private String keywords;
    /** 医生说明 */
    @ApiModelProperty(value = "医生说明")
    private String description;
    /** 状态码 */
    @ApiModelProperty(value = "状态码")
    private Integer state;
    /** 更新时间 */
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = Const.DEFAULT_TIME_FORMAT)
    private LocalDateTime updateTime;
    /** 创建时间 */
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = Const.DEFAULT_TIME_FORMAT)
    private LocalDateTime createTime;

}
