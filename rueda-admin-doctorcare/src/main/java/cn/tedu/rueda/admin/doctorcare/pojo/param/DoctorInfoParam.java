/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.pojo.param;

import cn.tedu.rueda.common.constant.Const;
import cn.tedu.rueda.common.constant.ValidateCode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author wang-p
 * @version 1.0.0
 * @class: DoctorInfoParam
 * @desc: 接收前端传入的医生信息
 * @date 2023/8/11
 * @modify:
 */
@Data
@ToString
@Accessors(chain = true)
public class DoctorInfoParam implements Serializable {

    /** 序列化ID */
    private static final long serialVersionUID = 1736121781183217259L;
    /** 医生姓名 */
    @ApiModelProperty(value = "医生姓名", required = true)
    @NotNull(message = ValidateCode.VALID_NOT_EMPTY)
    @Pattern(regexp = "^.{1,6}$",message = ValidateCode.VALID_NOT_MATCH_NAME)
    private String name;
    /** 身份证号 */
    @NotNull(message = ValidateCode.VALID_NOT_EMPTY)
    @Pattern(regexp = Const.PATTERN_USERID, message = ValidateCode.VALID_NOT_MATCH_USERID)
    @ApiModelProperty(value = "身份证号", required = true)
    private String userId;
    /** 电话号码 */
    @NotNull(message = ValidateCode.VALID_NOT_EMPTY)
    @Pattern(regexp = Const.PATTERN_CELLPHONE, message = ValidateCode.VALID_NOT_MATCH_PHONE)
    @ApiModelProperty(value = "电话号码", required = true)
    private String cellPhone;
    /** 性别 */
    @ApiModelProperty(value = "性别")
    private Integer gender;
    /** 年龄 */
    @ApiModelProperty(value = "年龄")
    private Integer age;
    /** 所属科室ID */
    @ApiModelProperty(value = "所属科室ID")
    @Min(value = 1, message = ValidateCode.VALID_GREATER_ZERO)
    private Long departmentId;
    /** 所属诊室ID */
    @ApiModelProperty(value = "所属诊室ID")
    @Min(value = 1, message = ValidateCode.VALID_GREATER_ZERO)
    private Long consultId;
    /** 擅长疾病 */
    @ApiModelProperty(value = "擅长疾病，（多个擅长类型以逗号隔开）")
    @Length(max = 200, message = ValidateCode.VALID_ZERO_TO_TWO_H)
    private String keywords;
    /** 医生说明 */
    @ApiModelProperty(value = "医生简介")
    @Length(max = 1000, message = ValidateCode.VALID_ZERO_TO_THOUSAND)
    private String description;
    /** 状态码 */
    @ApiModelProperty(value = "状态码", hidden = true)
    @Range(max = 1,message = ValidateCode.VALID_MUST_ZERO_OR_ONE)
    private Integer state = 0;

}
