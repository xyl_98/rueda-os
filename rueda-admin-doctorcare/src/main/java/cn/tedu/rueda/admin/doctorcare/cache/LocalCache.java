/*
 * Copyright © 2023 Reda-register All Rights Reserved
 */
package cn.tedu.rueda.admin.doctorcare.cache;

import cn.tedu.rueda.admin.doctorcare.dao.persist.repository.IDoctorRepository;
import cn.tedu.rueda.admin.doctorcare.dao.persist.repository.IDoctorScheduleRepository;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorInfoVO;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorScheduleInfoVO;
import cn.tedu.rueda.common.cache.Cache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @ClassName: LocalCache
 * @Description: 本地缓存
 * @Author Wang
 * @Date 2023/8/5
 * @Version 1.0.0
 * @ModifiedBy:
 */
@Slf4j
@Component
public class LocalCache extends Cache {

    /** 当前项目message信息properties文件名 */
    private static final String CURRENT_MESSAGE = "doctor_message.properties";

    /** 医生信息 */
    public static List<DoctorInfoVO> doctors = new CopyOnWriteArrayList<>();
    /** 排班信息 */
    public static List<DoctorScheduleInfoVO> schedules = new CopyOnWriteArrayList<>();

    @Autowired
    private IDoctorRepository doctorDao;
    @Autowired
    private IDoctorScheduleRepository scheduleDao;

    /**
     * 加载本地缓存、系统启动时自动运行
     * @throws IOException IO异常
     */
    @PostConstruct
    public void loadMessage() throws IOException {
        // 读取validate校验相关message
        messageMap.putAll(getMessageMap(VALID_MESSAGE));
        // 读取当前项目相关message
        messageMap.putAll(getMessageMap(CURRENT_MESSAGE));
        log.debug("已成功缓存message信息。");
    }

    @PostConstruct
    public void loadDoctorInfos() throws Exception {
        // 从数据库加载所有医生信息
        doctors.addAll(doctorDao.getAllDoctorInfo());
        log.debug("已成功缓存所有医生信息。");
        // 从数据库加载未来7天排班信息
        schedules.addAll(scheduleDao.getScheduleInfoForWeek());
        log.debug("已成功缓存未来7天排班信息。");
    }

    @PreDestroy
    public void destroy() {
        // 系统运行结束前执行
    }

    @Scheduled(cron = "0 0 * * * ?")
    public void refreshMessage() throws IOException {
        // 每个整点执行一次、刷新本地缓存中message数据
        messageMap.clear();
        loadMessage();
    }

    @Scheduled(cron = "0 0 0 * * ?")
    public void refreshDoctorInfo() throws Exception {
        // 每天0:00分执行一次、刷新本地缓存中医生及排班数据
        doctors.clear();
        schedules.clear();
        loadDoctorInfos();
    }

}
