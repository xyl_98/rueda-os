/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.dao.persist.repository.impl;

import cn.tedu.rueda.admin.doctorcare.dao.persist.mapper.AccountMapper;
import cn.tedu.rueda.admin.doctorcare.dao.persist.repository.IAccountRepository;
import cn.tedu.rueda.common.pojo.entity.AccountUser;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author wang-p
 * @version 1.0.0
 * @class: AccountRepositoryImpl
 * @desc: 用户信数据操作类
 * @date 2023/8/11
 * @modify:
 */
@Slf4j
@Repository
public class AccountRepositoryImpl implements IAccountRepository {

    @Autowired
    private AccountMapper mapper;


    /**
     * 根据身份证号判断是否用户数据
     * @return 判断结果
     */
    @Override
    public AccountUser getAccountIdByUserId(String userId) {
        Long accountId = null;
        log.debug("根据身份证号判断是否用户数据开始...");
        AccountUser user = mapper.selectOne(Wrappers.<AccountUser>lambdaQuery()
                .eq(AccountUser::getUserId, userId));
        log.debug("根据身份证号判断是否用户数据结束、检索到的数据：{}", user);
        return user;
    }

    /**
     * 新增用户信息
     * @param user 用户信息
     * @return 新增用户ID
     */
    @Override
    public Long addNewAccount(AccountUser user) {
        log.debug("新增用户信息开始...");
        int rows = mapper.insert(user);
        log.debug("新增用户信息结束、新增数据条数{}", rows);
        return user.getId();
    }

    /**
     * 更新用户角色和状态信息
     * @param entity 用户角色和状态信息
     * @return 更新数据条数
     */
    @Override
    public int updateRoleIdAndState(AccountUser entity) {
        log.debug("更新用户角色和状态信息开始...");
        int rows = mapper.update(new AccountUser(), Wrappers.<AccountUser>lambdaUpdate()
                .set(entity.getRoleId() != null, AccountUser::getRoleId, entity.getRoleId())
                .set(entity.getState() != null, AccountUser::getState, entity.getState())
                .setSql("UPDATE_TIME = NOW()")
                .eq(AccountUser::getId, entity.getId()));
        log.debug("更新用户角色和状态信息结束、更新数据条数{}", rows);
        return rows;
    }

    @Override
    public int updateUser(AccountUser accountUser) {
        log.debug("更新用户数据开始...");
        int rows = mapper.update(new AccountUser(), Wrappers.<AccountUser>lambdaUpdate()
                .set(accountUser.getCellPhone() != null, AccountUser::getCellPhone, accountUser.getCellPhone())
                .setSql("UPDATE_TIME = NOW()")
                .eq(AccountUser::getId, accountUser.getId()));
        log.debug("更新用户角色和状态信息结束、更新数据条数{}", rows);
        return rows;
    }

}
