/*
 * Copyright © 2023 By wang-p,All rights reserved.
 */
package cn.tedu.rueda.admin.doctorcare.service;

import cn.tedu.rueda.admin.doctorcare.pojo.param.ScheduleInfoParam;
import cn.tedu.rueda.admin.doctorcare.pojo.param.ScheduleSearchParam;
import cn.tedu.rueda.admin.doctorcare.pojo.param.ScheduleUpdateParam;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorScheduleInfoVO;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorScheduleUpdateVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @class: IDoctorScheduleService
 * @desc: 医生排班操作Service接口
 * @author wang-p
 * @date 2023/8/18
 * @version 1.0.0
 * @modify: 
 */
@Transactional(rollbackFor = Throwable.class)
public interface IDoctorScheduleService {

    /** 批量新增排班记录
     * @return*/
    List<DoctorScheduleUpdateVO> addScheduleRecords(List<ScheduleInfoParam> schedules);

    /** 修改排班信息 */
    void updateRecord(List<ScheduleUpdateParam> updates);

    /** 根据指定条件查找排班信息 */
    List<DoctorScheduleInfoVO> findByConditions(ScheduleSearchParam search);

}
