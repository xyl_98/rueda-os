package cn.tedu.rueda.admin.doctorcare.service.impl;

import cn.tedu.rueda.admin.doctorcare.cache.LocalCache;
import cn.tedu.rueda.admin.doctorcare.dao.persist.repository.IAccountRepository;
import cn.tedu.rueda.admin.doctorcare.dao.persist.repository.IDoctorRepository;
import cn.tedu.rueda.admin.doctorcare.pojo.entity.Doctor;
import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorInfoParam;
import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorSearchParam;
import cn.tedu.rueda.admin.doctorcare.pojo.param.DoctorUpdateInfoParam;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorDetailedInfoVO;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorInfoVO;
import cn.tedu.rueda.admin.doctorcare.pojo.vo.DoctorStandardVO;
import cn.tedu.rueda.admin.doctorcare.service.IDoctorService;
import cn.tedu.rueda.common.constant.MessageCode;
import cn.tedu.rueda.common.enumerator.ServiceCode;
import cn.tedu.rueda.common.ex.ServiceException;
import cn.tedu.rueda.common.pojo.entity.AccountUser;
import cn.tedu.rueda.common.pojo.entity.DoctorInfo;
import cn.tedu.rueda.common.util.MessageUtils;
import cn.tedu.rueda.common.util.ToolUtils;
import com.alibaba.druid.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DoctorServiceImpl implements IDoctorService {

    @Autowired
    private IDoctorRepository doctorRepository;
    @Autowired
    private IAccountRepository accountRepository;

    @Override
    public void setStateOff(Long id) {
        updateStateById(id, STATE_OFF);
    }

    @Override
    public void updateInfoById(DoctorUpdateInfoParam param) {
        //如果数据不存在(查询),则不修改
        DoctorStandardVO queryResult = doctorRepository.getStandardById(param.getId());
        if (queryResult == null) {
            String message = "修改信息失败，尝试访问的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        boolean isUpdate=false;
        Doctor doctor = new Doctor();
        if(param.getDepartmentId()!=null && param.getDepartmentId()!=queryResult.getDepartmentId()){
            isUpdate=true;
            doctor.setDepartmentId(param.getDepartmentId());
        }
        if(param.getConsultId()!=null && param.getConsultId()!=queryResult.getConsultId()){
            isUpdate=true;
            doctor.setConsultId(param.getConsultId());
        }
        if(param.getKeywords()!=null && !param.getKeywords().equals(queryResult.getKeywords())){
            isUpdate=true;
            doctor.setKeywords(param.getKeywords());
        }
        if(param.getDescription()!=null && !param.getDescription().equals(queryResult.getDescription())){
            isUpdate=true;
            doctor.setDescription(param.getDescription());
        }

        AccountUser user = new AccountUser();

        if(param.getCellPhone()!=null && !param.getCellPhone().equals(queryResult.getCellPhone())){
            isUpdate=true;
            user.setCellPhone(param.getCellPhone());
        }

        //执行修改
        if(isUpdate){
            doctor.setId(param.getId());
            int updateDoctor = doctorRepository.updateDoctor(doctor);
            if(updateDoctor!=1){
                String message = "修改信息失败，服务器忙，请稍后再试！";
                log.warn(message);
                throw new ServiceException(ServiceCode.ERROR_UPDATE, message);
            }
            user.setId(queryResult.getAccountId());
            int updateUser = accountRepository.updateUser(user);
            if(updateUser!=1){
                String message = "修改信息失败，服务器忙，请稍后再试！";
                log.warn(message);
                throw new ServiceException(ServiceCode.ERROR_UPDATE, message);
            }
        }

    }

    /**
     * 新增医生信息
     * @param param 医生信息
     */
    @Override
    public void addNewDoctorInfo(DoctorInfoParam param) {
        // 根据身份证号获取用户ID、若不存在该用户则会同步插入用户信息
        Long accountId = checkExistsAccount(param);
        // 既有医生信息恢复处理的情况下、不做后续处理
        if (accountId == -1L)
            return;
        // 插入医生信息
        DoctorInfo info = new DoctorInfo();
        BeanUtils.copyProperties(param, info);
        info.setAccountId(accountId);
        int rows = doctorRepository.addNewDoctorInfo(info);
        // 数据插入结果校验
        if (rows <= 0) {
            // 数据插入失败
            log.warn("医生[{}]的信息未能存入数据库。", param.getName());
            throw new ServiceException(ServiceCode.ERROR_INSERT, MessageUtils.getMessage(MessageCode.DOCTOR_INSERT_FAIL));
        }
    }

    /**
     * 根据条件检索医生信息
     * @param param 检索条件
     * @return 检索到的医生信息
     */
    @Override
    public List<DoctorDetailedInfoVO>   getDoctorInfos(DoctorSearchParam param) {
        // 条件检索（根据传入参数检索在职医生数据、包括排班信息）
        List<DoctorDetailedInfoVO> result = LocalCache.doctors.stream()
                // 根据传入条件过滤医生信息
                .filter(s -> {
                    // 条件：医生ID
                    boolean isId = param.getId() == null ? true : param.getId() == s.getId();
                    // 条件：科室ID
                    boolean isDept = param.getDepartmentId() == null ? true : s.getDepartmentId() == param.getDepartmentId();
                    // 条件：诊室ID
                    boolean isConsult = param.getConsultId() == null ? true : s.getConsultId() == param.getConsultId();
                    // 条件：医生姓名
                    boolean isName = StringUtils.isEmpty(param.getDoctorName()) ? true :
                            StringUtils.equals(s.getName(), param.getDoctorName());
                    // 关键字拆分
                    List<String> keywords = ToolUtils.split(s.getKeywords());
                    List<String> points = ToolUtils.split(param.getKeywords());
                    // 条件：关键字（医生擅长病症）
                    boolean isKeywords = StringUtils.isEmpty(param.getKeywords()) ? true :
                            points.stream().filter(c -> keywords.contains(c)).count() > 0;
                    // 组合条件检索
                    return (isId && isDept && isConsult && isName && isKeywords);
                })
                .map(s -> {
                    DoctorDetailedInfoVO detail = new DoctorDetailedInfoVO();
                    BeanUtils.copyProperties(s, detail);
                    // 获取医生未来7天时间里的排班信息
                    detail.setSchedules(LocalCache.schedules.stream()
                            .filter(c -> c.getDoctorAccountId() == s.getAccountId())
                            .collect(Collectors.toList()));
                    return detail;
                })
                .collect(Collectors.toList());

        if (result == null || result.size() == 0)
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,
                    MessageUtils.getMessage(MessageCode.DOCTOR_INFO_NOT_EXISTS));

        return result;
    }

    /**
     * 根据ID查询医生信息
     * @param id
     * @return
     */
    @Override
    public DoctorStandardVO getStandardById(Long id) {
        DoctorStandardVO result = doctorRepository.getStandardById(id);
        if (result == null) {
            String message = "查询医生详情失败，医生数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        return result;
    }

    /**
     * 根据传入数据检查数据库是否存在相应的用户和医生信息
     * @param param 传入的数据
     * @return 检查结果
     */
    private Long checkExistsAccount(DoctorInfoParam param) {
        Long accountId = null;
        // 若不存在指定医生信息，则校验用户信息是否存在并获取用户ID
        AccountUser user = accountRepository.getAccountIdByUserId(param.getUserId());
        if (user == null) {
            // 用户信息不存在、同步添加用户信息
            AccountUser userInfo = new AccountUser()
                    .setUserId(param.getUserId())
                    .setUsername(param.getName())
                    // TODO 初始化密码设置、加密算法未确定
                    // .setPassword("")
                    .setCellPhone(param.getCellPhone())
                    .setGender(param.getGender())
                    .setAge(param.getAge())
                    .setRoleId(2) // 医生角色ID固定为2
                    .setState(0); // 初始化状态为0：正常
            log.debug("用户信息不存在、将同步添加用户信息：{}", userInfo);
            accountId = accountRepository.addNewAccount(userInfo);
        } else {
            accountId = user.getId();
            // 若用户角色ID不为2：医生、3：更高权限，或state不为0：正常；则修改role_id为2且state为0
            if ((user.getRoleId() != 2 && user.getRoleId() != 3) || user.getState() != 0) {
                AccountUser update = new AccountUser()
                        .setId(accountId)
                        .setRoleId(user.getRoleId() != 2 && user.getRoleId() != 3 ? 2 : null)
                        .setState(user.getState() != 0 ? 0 : null);
                int rows = accountRepository.updateRoleIdAndState(update);
                // 用户数据更新失败
                if (rows <= 0)
                    throw new ServiceException(ServiceCode.ERROR_UPDATE,
                            MessageUtils.getMessage(MessageCode.DOCTOR_ACCOUNT_SYNC_UPDATE_FAIL));
            }
        }
        // 未取得用户ID
        if (accountId == null || accountId == 0)
            throw new ServiceException(ServiceCode.ERROR_INSERT,
                    MessageUtils.getMessage(MessageCode.DOCTOR_ACCOUNT_SYNC_FAIL));

        // 检查医生表里是否存在对应信息
        // 尝试从缓存里获取信息
        DoctorInfoVO doctorInfo = LocalCache.doctors.stream()
                .filter(s -> StringUtils.equals(s.getUserId(), param.getUserId())).findFirst().orElse(null);
        // 缓存不存在信息则做数据库校验
        if (doctorInfo == null)
            doctorInfo = doctorRepository.getDoctorInfo(param.getUserId());
        // 数据库或缓存存在信息则确认当前存在指定医生信息
        if (doctorInfo != null) {
            if (doctorInfo.getState() != 0) {
                // 若是检索到已被删除的医生信息、则修改状态为正常
                log.debug("检索到对应已离职的医生信息，将做信息恢复处理...");
                doctorInfo.setState(0);
                updateStateById(doctorInfo.getId(), doctorInfo.getState());
                accountId = -1L;
            } else {
                // 若是检索到正常医生信息、则拒绝重复添加
                throw new ServiceException(ServiceCode.ALREADY_EXIST,
                        MessageUtils.getMessage(MessageCode.DOCTOR_INFO_EXISTS, doctorInfo.getName()));
            }
        }

        return accountId;
    }

    /**
     * 删除医生
     * @param id
     * @param state
     */
    private void updateStateById(Long id, Integer state) {
        DoctorStandardVO queryResult = doctorRepository.getStandardById(id);
        if (queryResult == null) {
            String message = "删除/恢复数据失败，尝试访问的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        // 检查数据状态是否冲突
        if (queryResult.getState().equals(state)) {
            String message = "此数据已被删除/恢复！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }

        Doctor updateDoctor = new Doctor();
        updateDoctor.setId(id);
        updateDoctor.setState(state);
        int rows = doctorRepository.updateDoctor(updateDoctor);
        if (rows != 1) {
            String message = "删除/恢复数据失败，服务器忙，请稍后再试！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_UPDATE, message);
        }
    }
}
