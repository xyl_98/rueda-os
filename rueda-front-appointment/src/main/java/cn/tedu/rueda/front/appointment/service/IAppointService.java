package cn.tedu.rueda.front.appointment.service;

import cn.tedu.rueda.common.pojo.authentication.CurrentPrincipal;
import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.front.appointment.pojo.param.AppointAddNewParam;
import cn.tedu.rueda.front.appointment.pojo.vo.*;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface IAppointService {

    void handleAppoint(AppointAddNewParam appointAddNewParam, CurrentPrincipal currentPrincipal);

    void updateById(AppointAddNewParam appointAddNewParam);

    AppointStandard2VO findAppointById(Long id);

    PageData<AppointListForDoctor2VO> findListByDoctorId(Long doctorId, Integer pageNum, Integer pageSize);

    PageData<AppointListForDoctor2VO> findListByDoctorId(Long doctorId, Integer pageNum);

    PageData<AppointListForUser2VO> findListByAccountId(Long accountId, Integer pageNum, Integer pageSize);

    PageData<AppointListForUser2VO> findListByAccountId(Long accountId, Integer pageNum);
}
