package cn.tedu.rueda.front.appointment.dao.persist.repository.impl;

import cn.tedu.rueda.front.appointment.dao.persist.mapper.AccountMapper;
import cn.tedu.rueda.front.appointment.dao.persist.repository.IPatientRepository;
import cn.tedu.rueda.front.appointment.pojo.query.PatientQuery;
import cn.tedu.rueda.front.appointment.pojo.vo.PatientStandardVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PatientRepositoryImpl implements IPatientRepository {

    @Autowired
    private AccountMapper mapper;

    @Override
    public List<PatientStandardVO> getPatient(PatientQuery patientQuery) {

        return mapper.getPatient(patientQuery);
    }
}
