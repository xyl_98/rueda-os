package cn.tedu.rueda.front.appointment.alipay;

import cn.tedu.rueda.common.web.JsonResult;
import cn.tedu.rueda.front.appointment.pojo.entity.Appoint;
import cn.tedu.rueda.front.appointment.pojo.param.AlipayParam;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePagePayRequest;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Component
public class AlipayTemplate {

    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "9021000124661932";

    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCS54ZYYQGUe1rAUFzaiUwcWFN7l3jowURzoyzDCe13FCR5j4gwBlPkGqJqSr3XmIM7TvDzJa2beEhIQ79LIgMUgvjp2Lm9/E1lEkBoqv6XTH5DinnvvzeLUH48CMoSiuDmb879cxEYPpiuwBxdppEJdL1UAz42ZMu+Po4n0NEto4D/Y/WFIcJQrVN+GiJI9wV1UmfRinAVewcofUzyk6DDJVRm3AbYUklRsHgBbInBiE5otm0M+YiMerw0deQO5psyeoXwzx0qvs4zbaoIGBf/ecO3ahKNPrKxU7oYHITV9y4xGAUpUsdeGy7LZSQE/AGQ5Y0xpQBKm5QQsYm5lVTDAgMBAAECggEBAI10hzae4iiZxz8bou2D+1fn7Ss7Jd/qCgHXhAiijJh3OtAa16Z20Vz6EJ6NLdRmNSds0jMvesYTwnZjVdYBADpGC20RlXlNczD5rC+dR91keMiMlPG4jMGHVqZ0SDAmrnz1+LiwjitsSxUqQ6Yh99LFKHG3VbaGxHGCRtwsBOAN9W+Wms6SmJoDZzpef2cTNQ7emlsa/E0gsYIsBdh8he7E1LXAbT4FjLix0gQD0gFLi988+8VxuF/7UGkcnAMpKInjGY+gJCdxuYdfLwMSTtUJBAmovN0WSnRhtQgX+WvWr22XB3lWK86RhHi8ANWnvGFyva/6KcN1wB2BqVyb50ECgYEAxDTqmDCCiVMvBT4ssYDTz+8G53mXiO1EHEW0b3tEesFZEQMlyi3DAyQEcNQbrdxx2ht0vxvAJUDuOa/NLaKHGkdm1U+zFsF2DOXf7UuP4QtjafbNDyDcKs3w6EzOTKmFJjF3dWq3n3OMt1E4O5K3AUnq+IOp/R5AyZAYKnt/tVECgYEAv6xIwYiXHNWf23zPohJv3QIAEyfOlw/SYfQiAZe4bvkdEOfuJFUhlC/mpNw9b4Eb4rBI0Kpllw0gSuheJYEAIfB5R3xXT2BLjNytlrEm3wxUn8rEoO+z3mwcerLGXRcM1ixnH3Zwfcr5WwwPMMtaA7RTZqRC59txPgoKNXEH89MCgYB+mBnpOhtvWVGUU2po2NNRbUsKHQTt3xC8ISlraKBlh8zc0igWy/HwW9PVrLVzNzHqZR6tk80k/ED4zXpYAG6lQP6TkUne29I8PjF+HOvnav6oxwnFwhDsOVY/vBa7lgEIBJCsHL//TIG1Yd0jauVTGotV7zXF0DhDFQrSlj5vEQKBgAZa8wqfucr19lfzunq4O8dLMq9v6XSuzJ3810fQPQhI8ELIPegsfnGIaRBcURkqYb9RcwigLcAlakWxkLWTomp9q7fIEqkKG1cvVf4iH5TTttaZhY2EhSVB1mvnE5awlfdf/4dWiQJSwZPYQYSvqWtoBj/oAGPprcaN1KrS9Z2LAoGBAL5EGQpV38GN38ZoM2Gq1S5PjTGzsxmbGorVjmObtAUKI/1qNFHRSSB0Er9XnpCT1HC6s4Uc8cqrOGS8Gt5XMAp9Lmp/3rX12OcW0Kx862TJy+hLK2kIfT+QaZmrJBjpv/B28CTixc43G+8mO0SCr/BnV145/6faU1nwdqo6ZW0Q";

    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjPVmwhqVwhksucrzyuelk2VePBs8YcG0iLc/lCISLA5k7azt45Z6DzuNX0LuA+jpVO2RFH+GYxmTgxkaFzli+bSTVZhyBkJVmwHIM8/Rr9oW+qcdfKb/4Ktk5GyD5cr3Z2uLPs5i/R6xGnJNFAQWddQZE8tynDcRTWkg32bemp/4BzaN+bk+RnVtvtVRlTvk0KfEZ3IQk9vlPcJ+By1fOg8SAC4np6cJL4zpua85aWqjtKFvD9Sf8PtzquXHnvlzVFE7LFVP5Yq5ayHUenDbkcLg8wv/s3+bu4tP3GVOneCECyYJU2dp6NXWAEijEhmsqkv8QelG8DDTXf+fdGilkwIDAQAB";

    // 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "https://o7820a9090.zicp.fun/pay/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http:localhost:49000/appoint-record";

    // 签名方式
    public static String sign_type = "RSA2";

    // 字符编码格式
    public static String charset = "utf-8";

    // 支付宝网关
    public static String gatewayUrl = "https://openapi-sandbox.dl.alipaydev.com/gateway.do";

    //订单超时时间
    public static String timeout = "3m";

    /**
     * 去支付
     */
    public String pay(AlipayParam alipayParam) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = alipayParam.getId().toString();
        //付款金额，必填
        String total_amount = alipayParam.getPrice().toString();
        String subject = alipayParam.getUsername() + alipayParam.getUserId();
        String body = alipayParam.getTitle();

        //组装订单信息，以JSON格式
        alipayRequest.setBizContent("{\"out_trade_no\":\"" + out_trade_no + "\","
                + "\"total_amount\":\"" + total_amount + "\","
                + "\"subject\":\"" + subject + "\","
                + "\"body\":\"" + body + "\","
                + "\"timeout_express\":\"" + timeout + "\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应：" + result);

        return result;

    }

    /**
     * 验证签名
     */
    public boolean signVerification(Map<String, String[]> requestParams) {
        Map<String, String> params = new HashMap<>();
        for (String name : requestParams.keySet()) {
            String[] values = requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name, valueStr);
        }

        try {
            //调用SDK验证签名
            return AlipaySignature.rsaCheckV1(params, alipay_public_key, charset, sign_type);
        } catch (AlipayApiException e) {
            e.printStackTrace();
            return false;
        }
    }
}
