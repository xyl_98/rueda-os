package cn.tedu.rueda.front.appointment.pojo.param;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class AppointAddNewParam implements Serializable {

    private Long id;
    private Long accountId;
    private String appointTime;
    private String username;
    private String userId;
    private String cellPhone;
    private String medicalHistory;
    private String doctorName;
    private Long doctorId;
    private String departmentName;
    private Long departmentId;
    private Long consultId;
    private BigDecimal price;
    private String title;
    private Integer payState;

}
