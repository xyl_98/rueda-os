package cn.tedu.rueda.front.appointment.dao.persist.repository.impl;

import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.common.util.PageInfoToPageDataConverter;
import cn.tedu.rueda.front.appointment.dao.persist.mapper.AppointMapper;
import cn.tedu.rueda.front.appointment.dao.persist.repository.IAppointRepository;
import cn.tedu.rueda.front.appointment.pojo.entity.Appoint;
import cn.tedu.rueda.front.appointment.pojo.vo.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Repository
public class AppointRepositoryImpl implements IAppointRepository {

    @Autowired
    private AppointMapper appointMapper;

    @Override
    public int insert(Appoint appoint) {
        return appointMapper.insert(appoint);
    }

    @Override
    public int updateById(Appoint appoint) {
        return appointMapper.updateById(appoint);
    }

    @Override
    public AppointStandardVO getStandardVOById(Long id) {
        return appointMapper.getStandardVOById(id);
    }

    @Override
    public PageData<AppointListForDoctor2VO> listByDoctorId(Long doctorId, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<AppointListForDoctorVO> list = appointMapper.ListByDoctorId(doctorId);
        PageData<AppointListForDoctorVO> pageData = PageInfoToPageDataConverter.convert(new PageInfo<>(list));
        List<AppointListForDoctorVO> list1 = pageData.getList();
        List<AppointListForDoctor2VO> appointListForDoctor2VOS = new ArrayList<>();
        for (AppointListForDoctorVO appointListForDoctorVO : list1) {
            LocalDateTime oneTime = appointListForDoctorVO.getAppointTime();
            String endTime = oneTime.plusHours(1).toString().split("T")[1];
            String startTime = oneTime.toString();
            String appointTime = startTime.replace("T", " ") + "-" + endTime;
            AppointListForDoctor2VO appointListForDoctor2VO = new AppointListForDoctor2VO();
            BeanUtils.copyProperties(appointListForDoctorVO, appointListForDoctor2VO, "appointTime");
            appointListForDoctor2VO.setAppointTime(appointTime);
            appointListForDoctor2VOS.add(appointListForDoctor2VO);
        }
        PageData<AppointListForDoctor2VO> appointListForDoctorVOPageData = new PageData<>();
        appointListForDoctorVOPageData.setList(appointListForDoctor2VOS)
                .setMaxPage(pageData.getMaxPage())
                .setPageNum(pageData.getPageNum())
                .setTotal(pageData.getTotal())
                .setPageSize(pageData.getPageSize());
        return appointListForDoctorVOPageData;
    }

    @Override
    public PageData<AppointListForUser2VO> listByAccountId(Long accountId, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<AppointListForUserVO> list = appointMapper.ListByAccountId(accountId);
        PageData<AppointListForUserVO> pageData = PageInfoToPageDataConverter.convert(new PageInfo<>(list));
        List<AppointListForUserVO> list1 = pageData.getList();
        List<AppointListForUser2VO> appointListForUser2VOS = new ArrayList<>();
        for (AppointListForUserVO appointListForUserVO : list1) {
            System.out.println("appointListForUserVO==" + appointListForUserVO);
            LocalDateTime oneTime = appointListForUserVO.getAppointTime();
            String endTime = oneTime.plusHours(1).toString().split("T")[1];
            String startTime = oneTime.toString();
            String appointTime = startTime.replace("T", " ") + "-" + endTime;
            AppointListForUser2VO appointListForUser2VO = new AppointListForUser2VO();
            BeanUtils.copyProperties(appointListForUserVO, appointListForUser2VO, "appointTime", "payState", "creatTime");
            appointListForUser2VO.setCreateTime(appointListForUserVO.getCreateTime().toString().replace("T", " "));
            Integer payState = appointListForUserVO.getPayState();
            if (payState == 1)
                appointListForUser2VO.setPayState("已支付");
            else
                appointListForUser2VO.setPayState("未支付");
            appointListForUser2VO.setAppointTime(appointTime);
            appointListForUser2VOS.add(appointListForUser2VO);
        }
        PageData<AppointListForUser2VO> appointListForUser2VOPageData = new PageData<>();
        appointListForUser2VOPageData.setList(appointListForUser2VOS)
                .setMaxPage(pageData.getMaxPage())
                .setPageNum(pageData.getPageNum())
                .setTotal(pageData.getTotal())
                .setPageSize(pageData.getPageSize());
        return appointListForUser2VOPageData;
    }
}
