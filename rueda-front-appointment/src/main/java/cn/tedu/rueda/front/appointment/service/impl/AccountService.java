package cn.tedu.rueda.front.appointment.service.impl;




import cn.tedu.rueda.common.enumerator.ServiceCode;
import cn.tedu.rueda.common.ex.ServiceException;
import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.front.appointment.dao.persist.repository.IAccountRepository;

import cn.tedu.rueda.front.appointment.dao.persist.repository.IPatientRepository;
import cn.tedu.rueda.front.appointment.pojo.entity.Account;
import cn.tedu.rueda.front.appointment.pojo.param.AccountUpdateParam;
import cn.tedu.rueda.front.appointment.pojo.query.PatientQuery;
import cn.tedu.rueda.front.appointment.pojo.vo.AccountDetailStandardVO;
import cn.tedu.rueda.front.appointment.pojo.vo.ConsultResultStandardVO;
import cn.tedu.rueda.front.appointment.service.IAccountService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;


@Service
public class AccountService implements IAccountService {


    @Autowired
    IAccountRepository repository;

   @Autowired
   IPatientRepository patientRepository;

   private PatientQuery patientQuery;


    @Override
    public AccountDetailStandardVO getAccountById(Long id) {

        AccountDetailStandardVO accountById = repository.getAccountById(id);
        if (accountById==null){
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,"用户不存在");
        }
        return accountById;
    }

    @Override
    public void updateById(Long id,AccountUpdateParam accountUpdateParam) {
        Account account = new Account().setId(id);
        AccountDetailStandardVO accountById = repository.getAccountById(id);
        if (accountById==null){
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,"账户不存在");
        }
        BeanUtils.copyProperties(accountUpdateParam,account);
        repository.updateById(account);
    }

    @Override
    public PageData<ConsultResultStandardVO> listConsultResultByAccountId(Long accountId,Integer pageNum) {

        return repository.listConsultResultByAccountId(accountId,pageNum);
    }

    @Override
    public PageData<ConsultResultStandardVO> listConsultResultByPatientId(Long patientId, Integer pageNum) {
        patientQuery=new PatientQuery().setPatientId(patientId);
        if (patientRepository.getPatient(patientQuery)==null){
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,"该患者不存在或已被删除!");
        }
        return repository.listConsultResultByPatientId(patientId,pageNum);
    }

    @Override
    public PageData<ConsultResultStandardVO> listConsultResultByUserId(String userId, Integer pageNum) {
        patientQuery=new PatientQuery().setUserId(userId);
        if (patientRepository.getPatient(patientQuery)==null){
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,"该患者不存在或已被删除!");
        }
        return repository.listConsultResultByUserId(userId,pageNum);
    }

    @Override
    public ConsultResultStandardVO getConsultResultById(Long id) {

        ConsultResultStandardVO consultResultById = repository.getConsultResultById(id);
        if (consultResultById==null){
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,"未查询到诊断结果");
        }

        return consultResultById;
    }



}
