package cn.tedu.rueda.front.appointment.dao.persist.repository;


import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.front.appointment.pojo.entity.Account;
import cn.tedu.rueda.front.appointment.pojo.entity.ConsultResult;
import cn.tedu.rueda.front.appointment.pojo.vo.AccountDetailStandardVO;
import cn.tedu.rueda.front.appointment.pojo.vo.ConsultResultStandardVO;

import java.util.List;

public interface IAccountRepository {

    /**
     * 显示用户详情
     * @param id
     * @return
     */
    AccountDetailStandardVO getAccountById(Long id);

    /**
     * 根据ID更新资料
     * @param account
     * @return
     */
    int updateById(Account account);
    /**
     * 根据账户ID查询诊断结果列表
     * @param accountId
     * @return
     */
    PageData<ConsultResultStandardVO> listConsultResultByAccountId(Long accountId,Integer pageNum);


    /**
     * 根据患者ID查询诊断结果列表
     * @param patientId
     * @return
     */
    PageData<ConsultResultStandardVO> listConsultResultByPatientId(Long patientId,Integer pageNum);

    /**
     * 根据身份证号查询诊断结果列表
     * @param userId
     * @return
     */
    PageData<ConsultResultStandardVO> listConsultResultByUserId(String userId,Integer pageNum);



    /**
     * 根据ID查询诊断结果
     * @param id
     * @return
     */
  ConsultResultStandardVO getConsultResultById(Long id);
}
