package cn.tedu.rueda.front.appointment.dao.persist.repository;

import cn.tedu.rueda.front.appointment.pojo.query.PatientQuery;
import cn.tedu.rueda.front.appointment.pojo.vo.PatientStandardVO;

import java.util.List;

public interface IPatientRepository {

    List<PatientStandardVO> getPatient(PatientQuery patientQuery);
}
