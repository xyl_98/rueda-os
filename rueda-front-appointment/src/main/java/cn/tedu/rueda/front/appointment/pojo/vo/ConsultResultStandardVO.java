package cn.tedu.rueda.front.appointment.pojo.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;

import java.time.LocalDateTime;


@Data
public class ConsultResultStandardVO {

    /**
     * ID
     */

    private Long id;

    /**
     * 账户ID
     */
    private Long accountId;

    /**
     * 患者ID
     */
    private Long patientId;

    /**
     * 就诊时间
     */
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime date;


    /**
     * 姓名
     */

    private String username;

    /**
     * 身份证号
     */
    private String userId;

    /**
     * 医生ID
     */
    private Long doctorId;

    /**
     * 医生名字
     */

    private String doctorName;

    /**
     * 科室ID
     */

    private Long departmentId;

    /**
     * 科室名
     */
    private String departmentName;

    /**
     * 诊断结果
     */
    private String consultResult;


    /**
     * 状态
     */
    private Integer state;

}
