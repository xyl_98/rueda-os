package cn.tedu.rueda.front.appointment.dao.persist.mapper;

import cn.tedu.rueda.front.appointment.pojo.entity.Account;
import cn.tedu.rueda.front.appointment.pojo.query.PatientQuery;
import cn.tedu.rueda.front.appointment.pojo.vo.AccountDetailStandardVO;
import cn.tedu.rueda.front.appointment.pojo.vo.ConsultResultStandardVO;
import cn.tedu.rueda.front.appointment.pojo.vo.PatientStandardVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @author
 * 处理账户相关
 */
@Repository
public interface AccountMapper extends BaseMapper<Account> {
    /**
     * 显示用户详情
     * @param id
     * @return
     */
    AccountDetailStandardVO getAccountById(Long id);



    /**
     * 根据账户ID查询诊断结果
     * @param accountId
     * @return
     */
    List<ConsultResultStandardVO> listConsultResultByAccountId(Long accountId);

    /**
     * 根据患者ID查询诊断列表
     * @param patientId
     * @return
     */
    List<ConsultResultStandardVO> listConsultResultByPatientId(Long patientId);


    /**
     * 根据身份证号查询诊断列表
     * @param userId
     * @return
     */
    List<ConsultResultStandardVO> listConsultResultByUserId(String userId);

    /**
     * 单个诊断结果
     * @param id
     * @return
     */
    ConsultResultStandardVO getConsultResultById(Long id);

    /**
     * 获取患者信息
     * @param patientQuery
     * @return
     */
    List<PatientStandardVO> getPatient(@Param("patientQuery") PatientQuery patientQuery);
}
