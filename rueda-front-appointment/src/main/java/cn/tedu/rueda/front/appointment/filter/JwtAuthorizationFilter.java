package cn.tedu.rueda.front.appointment.filter;

import cn.tedu.rueda.common.enumerator.ServiceCode;
import cn.tedu.rueda.common.pojo.authentication.CurrentPrincipal;
import cn.tedu.rueda.common.pojo.po.UserStatePO;
import cn.tedu.rueda.common.web.JsonResult;

import cn.tedu.rueda.front.appointment.dao.cache.IUserCacheRepository;
import com.alibaba.fastjson.JSON;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
@Component
public class JwtAuthorizationFilter extends OncePerRequestFilter {

    @Value("${tmall.jwt.secret-key}")
    String secretKey;

    @Autowired
    private IUserCacheRepository userCacheRepository;


    public JwtAuthorizationFilter() {
        log.debug("创建过滤器对象，JwtAuthorizationFilter");
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        log.debug("进入了doFilterInternal方法");

        String jwt = httpServletRequest.getHeader("Authorization");
        log.debug("获取客服端携带的jwt:" + jwt);

        if (!StringUtils.hasText(jwt)) {
            //方行
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }

        //解析jwt
        //设置响应内容的字符集
        httpServletResponse.setContentType("application/json;charset=utf-8");
        Claims claims = null;

        try {
            claims = Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(jwt)
                    .getBody();
        } catch (SignatureException e) {
            String message = "非法访问！";
            JsonResult jsonResult = JsonResult.fail(ServiceCode.ERR_JWT_SIGNATURE, message);
            String jsonResultString = JSON.toJSONString(jsonResult);
            PrintWriter printWriter = httpServletResponse.getWriter();
            printWriter.println(jsonResultString);
            printWriter.close();
            return;
        } catch (MalformedJwtException e){
            String message = "非法访问！";
            JsonResult jsonResult = JsonResult.fail(ServiceCode.ERR_JWT_MALFORMED, message);
            String jsonResultString = JSON.toJSONString(jsonResult);
            PrintWriter printWriter = httpServletResponse.getWriter();
            printWriter.println(jsonResultString);
            printWriter.close();
            return;
        }catch (ExpiredJwtException e) {
            String message = "您的登录信息已过期，请重新登录！";
            JsonResult jsonResult = JsonResult.fail(ServiceCode.ERR_JWT_EXPIRED, message);
            String jsonResultString = JSON.toJSONString(jsonResult);
            PrintWriter printWriter = httpServletResponse.getWriter();
            printWriter.println(jsonResultString);
            printWriter.close();
            return;
        } catch (Throwable e) {
            log.debug("全局异常处理器开始处理Throwable");
            log.debug("异常跟踪信息如下：", e);
            String message = "服务器忙，请稍后再试！【同学们，看到这句时，你应该检查服务器端的控制台，并在JwtAuthorizationFilter中补充catch代码块进行处理】";
            JsonResult jsonResult = JsonResult.fail(ServiceCode.ERROR_UNKNOWN, message);
            String jsonResultString = JSON.toJSONString(jsonResult);
            PrintWriter printWriter = httpServletResponse.getWriter();
            printWriter.println(jsonResultString);
            printWriter.close();
            return;
        }

        //获取jwt中的数据
        Long id = claims.get("id", Long.class);
        String username = claims.get("username", String.class);
        String nick = claims.get("nick", String.class);

        //创建当事人对象
        CurrentPrincipal principal = new CurrentPrincipal();
        principal.setId(id)
                .setNick(nick)
                .setUsername(username);


        //获取redis里当前登录用户对应信息
        UserStatePO userStatePO = userCacheRepository.getAuthority(id);
        if (userStatePO==null) {
            //方行
            filterChain.doFilter(httpServletRequest,httpServletResponse);
            return;
        }

        //当前用户JSON格式的权限列表转换成SimpleGrantedAuthority格式
        List<String> authorities = userStatePO.getAuthorities();
        Collection<GrantedAuthority> authority = new ArrayList<>();
        for (String auth : authorities) {
            authority.add(new SimpleGrantedAuthority(auth));
        }
        log.debug("过滤器读取redis权限");



        // 创建认证信息（Authentication）
        Authentication authentication = new UsernamePasswordAuthenticationToken(
                principal, null, authority);

        // 将Authentication存入到SecurityContext
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(authentication);

        // 放行
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
