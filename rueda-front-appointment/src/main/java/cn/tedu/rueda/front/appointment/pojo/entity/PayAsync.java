package cn.tedu.rueda.front.appointment.pojo.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 支付宝异步通知回调参数
 */
@Data
public class PayAsync implements Serializable {
    private String gmt_create;
    private String charset;
    private String gmt_payment;
    private String notify_time;
    private String subject;
    private String sign;
    private String buyer_id;//支付者的id
    private String body;//订单的信息
    private String invoice_amount;//支付金额
    private String version;
    private String notify_id;//通知id
    private String fund_bill_list;
    private String notify_type;//通知类型； trade_status_sync
    private Long out_trade_no;//订单号
    private String total_amount;//支付的总额
    private String trade_status;//交易状态  TRADE_SUCCESS
    private String trade_no;//流水号
    private String auth_app_id;//
    private String receipt_amount;//商家收到的款
    private String point_amount;//
    private String app_id;//应用id
    private String buyer_pay_amount;//最终支付的金额
    private String sign_type;//签名类型
    private String seller_id;//商家的id

    @Override
    public String toString() {
        return "PayAsync{" +
                "gmt_create='" + gmt_create + '\'' +
                ", charset='" + charset + '\'' +
                ", gmt_payment='" + gmt_payment + '\'' +
                ", notify_time='" + notify_time + '\'' +
                ", subject='" + subject + '\'' +
                ", sign='" + sign + '\'' +
                ", buyer_id='" + buyer_id + '\'' +
                ", body='" + body + '\'' +
                ", invoice_amount='" + invoice_amount + '\'' +
                ", version='" + version + '\'' +
                ", notify_id='" + notify_id + '\'' +
                ", fund_bill_list='" + fund_bill_list + '\'' +
                ", notify_type='" + notify_type + '\'' +
                ", out_trade_no='" + out_trade_no + '\'' +
                ", total_amount='" + total_amount + '\'' +
                ", trade_status='" + trade_status + '\'' +
                ", trade_no='" + trade_no + '\'' +
                ", auth_app_id='" + auth_app_id + '\'' +
                ", receipt_amount='" + receipt_amount + '\'' +
                ", point_amount='" + point_amount + '\'' +
                ", app_id='" + app_id + '\'' +
                ", buyer_pay_amount='" + buyer_pay_amount + '\'' +
                ", sign_type='" + sign_type + '\'' +
                ", seller_id='" + seller_id + '\'' +
                '}';
    }
}
