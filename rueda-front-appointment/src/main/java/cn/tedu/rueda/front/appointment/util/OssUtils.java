package cn.tedu.rueda.front.appointment.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;

import java.io.InputStream;

public class OssUtils {

    //地域节点
    private static final String  endpoint = "oss-cn-chengdu.aliyuncs.com";

    //阿里云账号AccessKey
    private static final String accessKeyId = "LTAI5t8dYwe1bAFXcTsLfGaz";

    private static final String accessKeySecret = "XdASHc5koPs7ZOnAkMxAvIiDQNlnNq";

    //bucket名称
    private static final String bucketName = "rueda-os";

    public static String upload(InputStream inputStream, String fileUrl) {
        OSS ossClient = null;
        try {
            //创建OSSClient实例
            ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

            //存储对象
            ossClient.putObject(bucketName, fileUrl, inputStream);
        } finally {
            if (ossClient != null) {
                // 关闭OSSClient
                ossClient.shutdown();
            }
        }
        //https://gulimall-product-service.oss-cn-chengdu.aliyuncs.com/2023-02-17/105c5b4d-e984-40f8-9eab-4e3487207552.png
        //把新的文件名响应出去
        return "https://"+bucketName+"."+endpoint+"/"+fileUrl;
    }
}
