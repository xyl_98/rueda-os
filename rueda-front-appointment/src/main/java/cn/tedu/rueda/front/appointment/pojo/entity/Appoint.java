package cn.tedu.rueda.front.appointment.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@TableName("rueda_appoint")
@Accessors(chain = true)
public class Appoint implements Serializable {

    /**
     * 预约ID
     **/
    @TableId(type = IdType.AUTO)
    private Long id;

    private Long accountId;

    private Double price;

    private String title;

    /**
     * 创建时间
     **/
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     **/
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 预约时间
     **/
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime appointTime;

    /**
     * 创建人
     **/
    private String createUser;

    /**
     * 更新人
     **/
    private String updateUser;

    /**
     * 患者名称
     **/
    private String username;

    /**
     * 身份证号
     **/
    private String userId;

    /**
     * 手机号
     **/
    private String cellPhone;

    /**
     * 就诊记录
     **/
    private String medicalHistory;

    /**
     * 医生姓名
     **/
    private String doctorName;

    /**
     * 医生ID
     **/
    private Long doctorId;

    /**
     * 科室名称
     **/
    private String departmentName;

    /**
     * 科室ID
     **/
    private Long departmentId;

    /**
     * 诊室ID
     **/
    private Long consultId;

    /**
     * 支付状态
     **/
    private Integer payState;

    /**
     * 状态
     **/
    private Integer state;

}
