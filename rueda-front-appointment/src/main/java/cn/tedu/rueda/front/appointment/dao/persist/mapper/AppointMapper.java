package cn.tedu.rueda.front.appointment.dao.persist.mapper;


import cn.tedu.rueda.front.appointment.pojo.entity.Appoint;
import cn.tedu.rueda.front.appointment.pojo.vo.AppointListForDoctorVO;
import cn.tedu.rueda.front.appointment.pojo.vo.AppointListForUserVO;
import cn.tedu.rueda.front.appointment.pojo.vo.AppointStandardVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 处理预约相关
 */
@Repository
public interface AppointMapper extends BaseMapper<Appoint> {

    AppointStandardVO getStandardVOById(Long id);

    List<AppointListForDoctorVO> ListByDoctorId(Long doctorId);

    List<AppointListForUserVO> ListByAccountId(Long accountId);
}
