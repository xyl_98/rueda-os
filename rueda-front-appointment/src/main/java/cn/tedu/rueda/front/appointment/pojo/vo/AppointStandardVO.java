package cn.tedu.rueda.front.appointment.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class AppointStandardVO implements Serializable {
    /**
     * 数据ID
     */
    private Long id;
    private LocalDateTime appointTime;
    private String username;
    private Long userId;
    private String cellPhone;
    private String medicalHistory;
    private String doctorName;
    private Long doctorId;
    private String departmentName;
    private Long departmentId;
    private Long consultId;
    private Integer payState;
    private Integer state;
    private Long accountId;
    private BigDecimal price;
    private String title;
}
