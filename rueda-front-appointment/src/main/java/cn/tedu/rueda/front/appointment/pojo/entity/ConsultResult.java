package cn.tedu.rueda.front.appointment.pojo.entity;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("rueda_consult_result")
public class ConsultResult {

    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 账户ID
     */
    private Long accountId;

    /**
     * 患者ID
     */
    private Long patientId;

    /**
     * 就诊时间
     */
    private LocalDateTime date;


    /**
     * 姓名
     */

    private String username;

    /**
     * 身份证号
     */
    private String userId;

    /**
     * 医生ID
     */
    private Long doctorId;

    /**
     * 医生名字
     */

    private String doctorName;

    /**
     * 科室ID
     */

    private Long departmentId;

    /**
     * 科室名
     */
    private String departmentName;

    /**
     * 诊断结果
     */
    private String consultResult;


    /**
     * 状态
     */
    private Integer state;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 更新人
     */
    private String updateUser;
}
