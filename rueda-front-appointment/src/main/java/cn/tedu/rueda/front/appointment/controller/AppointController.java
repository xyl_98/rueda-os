package cn.tedu.rueda.front.appointment.controller;

import cn.tedu.rueda.common.pojo.authentication.CurrentPrincipal;
import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.common.web.JsonResult;
import cn.tedu.rueda.front.appointment.pojo.param.AppointAddNewParam;
import cn.tedu.rueda.front.appointment.pojo.vo.*;
import cn.tedu.rueda.front.appointment.service.IAppointService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@Slf4j
@Api(tags = "2.预约模块")
@Validated
@RestController
@RequestMapping("/appoint")
public class AppointController {

    @Autowired
    IAppointService appointService;

    @ApiOperation("预约")
    @ApiOperationSupport(order = 100)
    @PostMapping("/go-appoint")
    public JsonResult handleAppoint(AppointAddNewParam appointAddNewParam,
                                    @ApiIgnore @AuthenticationPrincipal CurrentPrincipal currentPrincipal){
        appointService.handleAppoint(appointAddNewParam,currentPrincipal);
        return JsonResult.ok();
    }

    @ApiOperation(value = "根据ID查询预约记录")
    @ApiOperationSupport(order = 400)
    @ApiImplicitParam(name = "id", value = "预约id", required = true, dataType = "Long")
    @GetMapping("/{id:[0-9]+}")
    public JsonResult findCategoryById(@Range(min = 1, message = "请提交合法的ID值")
                                       @PathVariable Long id) {
        AppointStandard2VO data = appointService.findAppointById(id);
        return JsonResult.ok(data);
    }

    @ApiOperation("根据医生ID查询预约列表")
    @ApiOperationSupport(order = 410)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "doctorId", value = "医生ID", required = true, paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "query", defaultValue = "1", dataType = "int"),
            @ApiImplicitParam(name = "queryType", value = "查询类型", paramType = "query", example = "all")
    })
    @GetMapping("/list-by-doctor")
    public JsonResult findAppointListByDoctor(@Range(message = "请提交有效的医生ID值！") Long doctorId,
                                               @Range(min = 1, message = "请提交有效的页码值！") Integer page,
                                               String queryType) {
        Integer pageNum = page == null ? 1 : page;
        PageData<AppointListForDoctor2VO> pageData;
        if ("all".equals(queryType))
            pageData = appointService.findListByDoctorId(doctorId, pageNum, Integer.MAX_VALUE);
        else
            pageData = appointService.findListByDoctorId(doctorId, pageNum);
        return JsonResult.ok(pageData);
    }

    @ApiOperation("根据账号查询预约列表")
    @ApiOperationSupport(order = 420)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "accountId", value = "账户ID", required = true, paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "page", value = "页码", paramType = "query", defaultValue = "1", dataType = "int"),
            @ApiImplicitParam(name = "queryType", value = "查询类型", paramType = "query", example = "all")
    })
    @GetMapping("/list-by-user")
    public JsonResult findCategoryListByUser(@Range(message = "请提交账户ID！") Long accountId,
                                               @Range(min = 1, message = "请提交有效的页码值！") Integer page,
                                               String queryType) {
        Integer pageNum = page == null ? 1 : page;
        PageData<AppointListForUser2VO> pageData;
        if ("all".equals(queryType))
            pageData = appointService.findListByAccountId(accountId, pageNum, Integer.MAX_VALUE);
        else
            pageData = appointService.findListByAccountId(accountId, pageNum);
        return JsonResult.ok(pageData);
    }
}
