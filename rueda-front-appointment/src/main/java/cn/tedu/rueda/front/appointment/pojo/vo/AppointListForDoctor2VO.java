package cn.tedu.rueda.front.appointment.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class AppointListForDoctor2VO implements Serializable {
    /**
     * 数据ID
     */
    private Long id;
    private String appointTime;
    private Long accountId;
    private String username;
    private Long userId;
    private String cellPhone;
    private String medicalHistory;
    private Long consultId;
}
