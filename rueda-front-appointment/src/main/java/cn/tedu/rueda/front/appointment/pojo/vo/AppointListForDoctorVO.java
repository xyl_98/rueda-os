package cn.tedu.rueda.front.appointment.pojo.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class AppointListForDoctorVO implements Serializable {
    /**
     * 数据ID
     */
    private Long id;
    private LocalDateTime appointTime;
    private Long accountId;
    private String username;
    private Long userId;
    private String cellPhone;
    private String medicalHistory;
    private Long consultId;
}
