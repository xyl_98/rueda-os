package cn.tedu.rueda.front.appointment.dao.persist.repository.impl;

import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.common.util.PageInfoToPageDataConverter;
import cn.tedu.rueda.front.appointment.dao.persist.mapper.AccountMapper;
import cn.tedu.rueda.front.appointment.dao.persist.repository.IAccountRepository;
import cn.tedu.rueda.front.appointment.pojo.entity.Account;
import cn.tedu.rueda.front.appointment.pojo.vo.AccountDetailStandardVO;
import cn.tedu.rueda.front.appointment.pojo.vo.ConsultResultStandardVO;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class AccountRepositoryImpl implements IAccountRepository {

    @Autowired
    AccountMapper mapper;

    @Value("${rueda.dao.default-query-page-size:}")
    private Integer pageSize;

    @Override
    public AccountDetailStandardVO getAccountById(Long id) {


        return mapper.getAccountById(id);
    }

    @Override
    public int updateById(Account account) {
        return mapper.updateById(account);
    }

    @Override
    public PageData<ConsultResultStandardVO> listConsultResultByAccountId(Long accountId, Integer pageNum) {


        PageHelper.startPage(pageNum, pageSize);
        List<ConsultResultStandardVO> consultResultStandardVOS = mapper.listConsultResultByAccountId(accountId);
        PageInfo<ConsultResultStandardVO> pageInfo = new PageInfo<>(consultResultStandardVOS);
        PageData<ConsultResultStandardVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);

        return pageData;
    }

    @Override
    public PageData<ConsultResultStandardVO> listConsultResultByPatientId(Long patientId, Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        List<ConsultResultStandardVO> consultResultStandardVOS = mapper.listConsultResultByPatientId(patientId);
        PageInfo<ConsultResultStandardVO> pageInfo = new PageInfo<>(consultResultStandardVOS);
        PageData<ConsultResultStandardVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }

    @Override
    public PageData<ConsultResultStandardVO> listConsultResultByUserId(String userId, Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        List<ConsultResultStandardVO> consultResultStandardVOS = mapper.listConsultResultByUserId(userId);
        PageInfo<ConsultResultStandardVO> pageInfo = new PageInfo<>(consultResultStandardVOS);
        PageData<ConsultResultStandardVO> pageData = PageInfoToPageDataConverter.convert(pageInfo);
        return pageData;
    }


    @Override
    public ConsultResultStandardVO getConsultResultById(Long id) {

        return mapper.getConsultResultById(id);
    }
}
