package cn.tedu.rueda.front.appointment.dao.persist.repository;

import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.front.appointment.pojo.entity.Appoint;
import cn.tedu.rueda.front.appointment.pojo.vo.*;

public interface IAppointRepository {

    int insert(Appoint appoint);

    int updateById(Appoint appoint);

    AppointStandardVO getStandardVOById(Long id);

    PageData<AppointListForDoctor2VO> listByDoctorId(Long doctorId, Integer pageNum, Integer pageSize);

    PageData<AppointListForUser2VO> listByAccountId(Long accountId, Integer pageNum, Integer pageSize);
}
