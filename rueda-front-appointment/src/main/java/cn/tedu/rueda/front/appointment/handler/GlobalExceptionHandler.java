package cn.tedu.rueda.front.appointment.handler;

import cn.tedu.rueda.common.enumerator.ServiceCode;
import cn.tedu.rueda.common.ex.ServiceException;
import cn.tedu.rueda.common.web.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

/**
 * 全局异常处理器
 * 用于处理Controller抛出的ServiceException
 **/
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler
    public JsonResult handleServiceException(ServiceException e) {
        log.warn("全局异常处理器开始处理ServiceException");
        return JsonResult.fail(e);
    }

    @ExceptionHandler
    public JsonResult handleBindException(BindException e) {
        log.debug("全局异常处理器开始处理BindException");
//         StringJoiner stringJoiner = new StringJoiner("，", "参数错误，", "！");
//         e.getFieldErrors().forEach(fieldError -> stringJoiner.add(fieldError.getDefaultMessage()));
//         String message = stringJoiner.toString();
        FieldError fieldError = e.getFieldError();
        String message = fieldError.getDefaultMessage();
        return JsonResult.fail(ServiceCode.ERROR_BAD_REQUEST, message);
    }

    @ExceptionHandler
    public JsonResult handleConstraintViolationException(ConstraintViolationException e) {
        log.debug("全局异常处理器开始处理ConstraintViolationException");
        String message = null;
        Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
        for (ConstraintViolation<?> constraintViolation : constraintViolations) {
            message = constraintViolation.getMessage();
        }
        return JsonResult.fail(ServiceCode.ERROR_BAD_REQUEST, message);
    }

    @ExceptionHandler
    public JsonResult handleThrowable(Throwable e) {
        log.warn("全局异常处理器开始处理Throwable");
        log.warn("", e);
        String message = "服务器忙，请稍后再试";
        return JsonResult.fail(ServiceCode.ERROR_UNKNOWN,message);
    }
}
