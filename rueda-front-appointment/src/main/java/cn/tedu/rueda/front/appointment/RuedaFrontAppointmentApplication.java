package cn.tedu.rueda.front.appointment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"cn.tedu.rueda.*"})
public class RuedaFrontAppointmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(RuedaFrontAppointmentApplication.class, args);
    }

}
