package cn.tedu.rueda.front.appointment.pojo.vo;


import lombok.Data;


import java.util.List;

@Data
public class AccountDetailStandardVO {
    /**
     * 账户ID
     */

    private Long id;
    /**
     * 姓名
     */
    private String username;
    /**
     * 电话号码
     */
    private String cellPhone;

    /**
     * 密码
     */
    private String password;
    /**
     * 昵称
     */
    private String nick;
    /**
     * 身份证号
     */
    private String userId;
    /**
     * 角色ID
     */
    private Integer roleId;

    /**
     * 权限ID
     */
    private Integer authId;
    /**
     * 头像
     */
    private String sculpture;
    /**
     * 性别
     */
    private Integer gender;
    /**
     * 年龄
     */
    private Integer age;



}
