package cn.tedu.rueda.front.appointment.dao.cache;

import cn.tedu.rueda.common.consts.cache.RedisUserAuthorityKeyName;
import cn.tedu.rueda.common.pojo.po.UserStatePO;
import org.springframework.stereotype.Repository;

public interface IUserCacheRepository extends RedisUserAuthorityKeyName {

    /**
     * 用于用户验证的信息写入redis
     * @param id
     * @param userStatePO
     */
    void saveState(Long id, UserStatePO userStatePO);

    /**
     * 存入短信验证码到redis
     * @param phone
     * @param smsCode
     */
    void saveVerificationCode(String phone,String smsCode);

    /**
     * 读取redis里用户的信息
     * @param id
     * @return
     */
    UserStatePO getAuthority(Long id);

    /**
     * 获取短信验证码，并删除
     * @param phone
     * @return
     */
    String getSmsCode(String phone);

    /**
     * 删除redis里的当前用户的信息
     * @param id
     * @return boolean
     */
    boolean deleteAuthority(Long id);
}
