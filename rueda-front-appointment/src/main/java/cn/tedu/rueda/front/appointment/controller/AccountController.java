package cn.tedu.rueda.front.appointment.controller;


import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.common.web.JsonResult;
import cn.tedu.rueda.front.appointment.pojo.param.AccountUpdateParam;
import cn.tedu.rueda.front.appointment.pojo.vo.AccountDetailStandardVO;
import cn.tedu.rueda.front.appointment.pojo.vo.ConsultResultStandardVO;
import cn.tedu.rueda.front.appointment.service.IAccountService;

import cn.tedu.rueda.front.appointment.util.OssUtils;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;

import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;




import java.io.IOException;
import java.text.SimpleDateFormat;

import java.util.Date;

import java.util.UUID;


@RestController
@RequestMapping("/account")
@Api(tags = "1.用户模块")
@Validated
@Slf4j
public class AccountController {

    @Autowired
    IAccountService accountService;

    @PostMapping("/upload-sculpture")
    @ApiOperation("上传头像")
    @ApiOperationSupport(order = 300)
    @ApiImplicitParam(name = "file", value = "图片文件", dataType = "file")

    public JsonResult uploadSculpture(MultipartFile file) throws IOException {
        /**
         * 获取图片全名
         */
        String filename = file.getOriginalFilename();
        log.debug("文件名:" + filename);
        /**
         * 得到图片后缀名
         */
        String suffix = filename.substring(filename.lastIndexOf("."));
        /**
         * UUID生成唯一图片名
         */
        filename = UUID.randomUUID() + suffix;
        /**
         * 得到日期格式路径
         */
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd/");
        String datePath = simpleDateFormat.format(new Date());

        String filePath = datePath + filename;

        /**
         * 图片上传至Oss,并得到图片完整路径
         */
        String ossFilePath = OssUtils.upload(file.getInputStream(), filePath);
        /**
         * 图片返回前端
         */
        return JsonResult.ok(ossFilePath);
    }

    @PostMapping("/{id}/update")
    @ApiOperation("更新个人资料")
    @ApiOperationSupport(order = 420)
    @ApiImplicitParam(name = "id", value = "账户ID", required = true, dataType = "long")
    public JsonResult updateById(@PathVariable @Range(min = 1, message = "请提供有效ID") Long id, AccountUpdateParam accountUpdateParam) {
        log.debug("更新参数:" + accountUpdateParam);
        accountService.updateById(id, accountUpdateParam);
        return JsonResult.ok();
    }


    @GetMapping("/{id}/personal")
    @ApiOperation("个人中心")
    @ApiOperationSupport(order = 400)
    @ApiImplicitParam(name = "id", value = "账户ID", required = true, dataType = "long")
    public JsonResult getById(@Range(min = 1, message = "请提供有效ID") @PathVariable Long id) {
        AccountDetailStandardVO accountById = accountService.getAccountById(id);
        log.debug("个人信息:" + accountById);
        return JsonResult.ok(accountById);
    }


    @GetMapping("/{accountId}/{pageNum}/consult-result-account")
    @ApiOperation("根据账户ID查询诊断列表")
    @ApiOperationSupport(order = 410)
    @ApiImplicitParams({@ApiImplicitParam(name = "pageNum", value = "页码", required = true, dataType = "int")
            , @ApiImplicitParam(name = "accountId", value = "账户ID", required = true, dataType = "long")})
    public JsonResult listConsultResultByAccountId(@PathVariable Long accountId, @Range(min = 1, message = "请提供有效页码") @PathVariable Integer pageNum) {
        PageData<ConsultResultStandardVO> consultResultStandardVOS = accountService.listConsultResultByAccountId(accountId, pageNum);
        log.debug("已分页的数据:{}", consultResultStandardVOS.toString());
        return JsonResult.ok(consultResultStandardVOS);
    }

    @GetMapping("/{patientId}/{pageNum}/consult-result-patient")
    @ApiOperation("根据患者ID查询诊断列表")
    @ApiOperationSupport(order = 420)
    @ApiImplicitParams({@ApiImplicitParam(name = "pageNum", value = "页码", required = true, dataType = "int")
            , @ApiImplicitParam(name = "patientId", value = "患者ID", required = true, dataType = "long")})
    public JsonResult listConsultResultByPatientId(@PathVariable Long patientId, @Range(min = 1, message = "请提供有效页码") @PathVariable Integer pageNum) {
        PageData<ConsultResultStandardVO> consultResultStandardVOS = accountService.listConsultResultByPatientId(patientId, pageNum);
        log.debug("已分页的数据:{}", consultResultStandardVOS.toString());
        return JsonResult.ok(consultResultStandardVOS);
    }

    @GetMapping("/{pageNum}/consult-result-userId")
    @ApiOperation("根据身份证号查询诊断列表")
    @ApiOperationSupport(order = 430)
    @ApiImplicitParams({@ApiImplicitParam(name = "pageNum", value = "页码", required = true, dataType = "int")
            , @ApiImplicitParam(name = "userId", value = "身份证号", required = true)})
    public JsonResult listConsultResultByUserId(@RequestParam("userId") String userId, @Range(min = 1, message = "请提供有效页码") @PathVariable Integer pageNum) {
        log.debug("身份证号:{}",userId);
        PageData<ConsultResultStandardVO> consultResultStandardVOS = accountService.listConsultResultByUserId(userId,pageNum);
        log.debug("已分页的数据:{}", consultResultStandardVOS.toString());
        return JsonResult.ok(consultResultStandardVOS);
    }


    @GetMapping("/{id}/consult-result")
    @ApiOperation("根据ID查询诊断结果")
    @ApiOperationSupport(order = 430)
    @ApiImplicitParam(name = "id", value = "ID", required = true, dataType = "long")
    public JsonResult listConsultResultById(@Range(min = 1, message = "请提供有效ID") @PathVariable Long id) {
        ConsultResultStandardVO consultResultById = accountService.getConsultResultById(id);
        return JsonResult.ok(consultResultById);
    }


}
