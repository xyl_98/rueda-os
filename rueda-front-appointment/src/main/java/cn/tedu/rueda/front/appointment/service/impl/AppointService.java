package cn.tedu.rueda.front.appointment.service.impl;

import cn.tedu.rueda.common.constant.Const;
import cn.tedu.rueda.common.enumerator.ServiceCode;
import cn.tedu.rueda.common.ex.ServiceException;
import cn.tedu.rueda.common.pojo.authentication.CurrentPrincipal;
import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.front.appointment.dao.persist.repository.IAppointRepository;
import cn.tedu.rueda.front.appointment.pojo.entity.Appoint;
import cn.tedu.rueda.front.appointment.pojo.param.AppointAddNewParam;
import cn.tedu.rueda.front.appointment.pojo.vo.*;
import cn.tedu.rueda.front.appointment.service.IAppointService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.format.DateTimeFormatters;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.ApiIgnore;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@Service
public class AppointService implements IAppointService {

    @Value("${rueda.dao.default-query-page-size}")
    private Integer defaultQueryPageSize;

    @Autowired
    IAppointRepository appointRepository;

    @Override
    public void handleAppoint(AppointAddNewParam appointAddNewParam, CurrentPrincipal currentPrincipal) {
        log.debug("预约时间：{}", appointAddNewParam.getAppointTime());
        String appointTime = appointAddNewParam.getAppointTime();
        String[] time = appointTime.split("~");
        System.out.println(time[0]);
        Appoint appoint = new Appoint();
        BeanUtils.copyProperties(appointAddNewParam, appoint);
        appoint.setAccountId(currentPrincipal.getId());
        appoint.setCreateUser(currentPrincipal.getUsername());
        appoint.setAppointTime(LocalDateTime.parse(time[0], DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
        appoint.setState(1);
        appoint.setTitle("预约挂号");
        appoint.setPrice(9.9);
        int rows = appointRepository.insert(appoint);
        if (rows != 1) {
            throw new ServiceException(ServiceCode.ERROR_INSERT, "预约失败！");
        }
    }

    @Override
    public void updateById(AppointAddNewParam appointAddNewParam) {
        Appoint appoint = new Appoint();
        BeanUtils.copyProperties(appointAddNewParam, appoint);
        appoint.setPayState(1);
        appointRepository.updateById(appoint);
    }

    @Override
    public AppointStandard2VO findAppointById(Long id) {
        AppointStandardVO appointStandardVO = appointRepository.getStandardVOById(id);
        LocalDateTime oneTime = appointStandardVO.getAppointTime();
        String endTime = oneTime.plusHours(1).toString().split("T")[1];
        String startTime = oneTime.toString();
        String appointTime = startTime.replace("T", " ") + "-" + endTime;
        AppointStandard2VO appointStandard2VO = new AppointStandard2VO();
        BeanUtils.copyProperties(appointStandardVO,appointStandard2VO,"appointTime");
        appointStandard2VO.setAppointTime(appointTime);
        if (appointStandardVO == null) {
            String message = "查询失败，尝试访问的数据不存在！";
            log.error(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        return appointStandard2VO;
    }


    @Override
    public PageData<AppointListForDoctor2VO> findListByDoctorId(Long doctorId, Integer pageNum, Integer pageSize) {
        return appointRepository.listByDoctorId(doctorId, pageNum, pageSize);
    }

    @Override
    public PageData<AppointListForDoctor2VO> findListByDoctorId(Long doctorId, Integer pageNum) {
        return appointRepository.listByDoctorId(doctorId, pageNum, defaultQueryPageSize);
    }

    @Override
    public PageData<AppointListForUser2VO> findListByAccountId(Long accountId, Integer pageNum, Integer pageSize) {
        return appointRepository.listByAccountId(accountId, pageNum, pageSize);
    }

    @Override
    public PageData<AppointListForUser2VO> findListByAccountId(Long accountId, Integer pageNum) {
        return appointRepository.listByAccountId(accountId, pageNum, defaultQueryPageSize);
    }
}
