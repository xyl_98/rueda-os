package cn.tedu.rueda.front.appointment.pojo.query;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
@Data
@Accessors(chain = true)
public class PatientQuery implements Serializable {

    private String userId;
    private Long patientId;

}
