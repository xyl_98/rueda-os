package cn.tedu.rueda.front.appointment.controller;

import cn.tedu.rueda.common.web.JsonResult;
import cn.tedu.rueda.front.appointment.alipay.AlipayTemplate;
import cn.tedu.rueda.front.appointment.pojo.entity.Appoint;
import cn.tedu.rueda.front.appointment.pojo.entity.PayAsync;
import cn.tedu.rueda.front.appointment.pojo.param.AlipayParam;
import cn.tedu.rueda.front.appointment.pojo.param.AppointAddNewParam;
import cn.tedu.rueda.front.appointment.pojo.vo.AppointStandard2VO;
import cn.tedu.rueda.front.appointment.pojo.vo.AppointStandardVO;
import cn.tedu.rueda.front.appointment.service.IAppointService;
import com.alipay.api.AlipayApiException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * 支付Controller
 */
@Slf4j
@RestController
@RequestMapping("/pay")
public class PayController {

    @Autowired
    private AlipayTemplate alipayTemplate;

    @Autowired
    private IAppointService appointService;

    /**
     * 查询当前需要支付的商品信息
     */
    @GetMapping(value = "/getPayOrder/{id:[0-9]+}")
    @ResponseBody
    public JsonResult getPayOrder(@PathVariable Long id) {
        //TODO 这里模拟从数据库查询出来的数据
        AppointStandard2VO appointById = appointService.findAppointById(id);
        log.debug("appointById={}",appointById);
        return JsonResult.ok(appointById);
    }

    /**
     * 跳转到支付宝支付页，支付宝的sdk返回的是一个页面，这里直接返回页面即可
     */
    @GetMapping(value = "/aliPayOrder", produces = "text/html")
//    @ResponseBody
    public String aliPayOrder(AlipayParam alipayParam) throws AlipayApiException {
        String result = alipayTemplate.pay(alipayParam);
        System.out.println(result);
        return result;
    }

    /**
     * 支付宝支付结果异步通知
     */
    @PostMapping("/notify")
    @ResponseBody
    public String aliPayedNotify(HttpServletRequest request, PayAsync payAsync) {
        try {
            System.out.println("异步通知支付结果");
            //验签
            if (!alipayTemplate.signVerification(request.getParameterMap())) {
                return "fail";
            }

            //查看一下支付宝的回调信息
            System.out.println(payAsync);

            //如果是支付成功状态
            if ("TRADE_SUCCESS".equals(payAsync.getTrade_status()) || "TRADE_FINISHED".equals(payAsync.getTrade_status())) {
                //TODO 操作数据库修改订单状态。。这里省略
                AppointAddNewParam appointAddNewParam = new AppointAddNewParam();
                appointAddNewParam.setId(payAsync.getOut_trade_no()).setPayState(1);
                appointService.updateById(appointAddNewParam);
                return "success";
            }

            //如果是未付款交易超时关闭，或支付完成后全额退款
            if ("TRADE_CLOSED".equals(payAsync.getTrade_status())) {
                //TODO 操作数据库修改订单状态。。这里省略
                AppointAddNewParam appointAddNewParam = new AppointAddNewParam();
                appointAddNewParam.setId(payAsync.getOut_trade_no()).setPayState(0);
                appointService.updateById(appointAddNewParam);
                return "success";
            }
            return "fail";
        } catch (Exception e) {
            return "error";
        }
    }
}
