package cn.tedu.rueda.front.appointment.pojo.param;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
public class AlipayParam implements Serializable {

    private Long id;
    private String username;
    private String userId;
    private String title;
    private BigDecimal price;

}
