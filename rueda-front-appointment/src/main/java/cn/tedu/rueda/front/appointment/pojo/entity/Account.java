package cn.tedu.rueda.front.appointment.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 实体类 账户
 * @author 吴俊辉
 */
@Data
@TableName("rueda_account")
@Accessors(chain = true)
public class Account implements Serializable {
    /**
     * 账户ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 姓名
     */
    private String username;
    /**
     * 电话号码
     */
    private String cellPhone;
    /**
     * 密码
     */
    private String password;
    /**
     * 昵称
     */
    private String nick;
    /**
     * 身份证号
     */
    private String userId;
    /**
     * 角色ID
     */
    private Integer roleId;
    /**
     * 权限ID
     */
    private Integer authId;
    /**
     * 头像
     */
    private String sculpture;
    /**
     * 性别
     */
    private Integer gender;
    /**
     * 年龄
     */
    private Integer age;
    /**
     * 状态
     */
    private Integer state;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    /**
     * 创建人
     */
    private String createUser;
    /**
     * 更新人
     */
    private String updateUser;
}
