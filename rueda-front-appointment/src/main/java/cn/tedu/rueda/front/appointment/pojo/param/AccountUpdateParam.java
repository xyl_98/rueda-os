package cn.tedu.rueda.front.appointment.pojo.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.validation.annotation.Validated;


import javax.validation.constraints.Pattern;

@Data
@Validated
@Accessors(chain = true)
public class AccountUpdateParam {

    @ApiModelProperty("昵称")
    @Pattern(regexp = "^.{2,8}$",message = "请提交合法昵称")
    private String nick;
    @ApiModelProperty("图片路径")
    @Pattern(regexp = "^.{1,1000}$", message = "非法路径!")
    private String sculpture;
}
