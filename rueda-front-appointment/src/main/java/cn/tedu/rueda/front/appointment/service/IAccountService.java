package cn.tedu.rueda.front.appointment.service;

import cn.tedu.rueda.common.pojo.authentication.CurrentPrincipal;
import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.front.appointment.pojo.param.AccountUpdateParam;
import cn.tedu.rueda.front.appointment.pojo.vo.AccountDetailStandardVO;
import cn.tedu.rueda.front.appointment.pojo.vo.ConsultResultStandardVO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface IAccountService {
    /**
     * 显示账户信息
     * @param id
     * @return
     */
    AccountDetailStandardVO getAccountById(Long id);

    /**
     * 更新个人资料
     * @param id
     * @param accountUpdateParam
     */
    void updateById(Long id, AccountUpdateParam accountUpdateParam);

    /**
     * 根据账户ID获取诊断列表
     * @param accountId
     * @param pageNum
     * @return
     */
    PageData<ConsultResultStandardVO> listConsultResultByAccountId(Long accountId, Integer pageNum);

    /**
     * 根据患者ID获取诊断列表
     * @param patientId
     * @param pageNum
     * @return
     */
    PageData<ConsultResultStandardVO> listConsultResultByPatientId(Long patientId, Integer pageNum);


    /**
     * 根据患者ID获取诊断列表
     * @param userId
     * @param pageNum
     * @return
     */
    PageData<ConsultResultStandardVO> listConsultResultByUserId(String userId, Integer pageNum);

    /**
     * 显示诊断结果详细信息
     * @param id
     * @return
     */
    ConsultResultStandardVO getConsultResultById(Long id);


}
