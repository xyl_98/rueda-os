package cn.tedu.rueda.front.appointment.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("cn.tedu.rueda.front.appointment.dao.persist.mapper")
public class MyBatisConfig {
}
