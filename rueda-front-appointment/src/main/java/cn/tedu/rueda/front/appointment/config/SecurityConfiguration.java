package cn.tedu.rueda.front.appointment.config;

import cn.tedu.rueda.common.enumerator.ServiceCode;
import cn.tedu.rueda.common.web.JsonResult;

import cn.tedu.rueda.front.appointment.filter.JwtAuthorizationFilter;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Configuration;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.io.PrintWriter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthorizationFilter jwtAuthorizationFilter;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.formLogin();


        //使用无状态的session机制
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class);
        http.cors();
        http.csrf().disable();

        // 处理无认证信息却尝试访问需要通过认证的资源时的异常
        http.exceptionHandling().authenticationEntryPoint((request, response, e) -> {
            String message = "当前未登录，或登录信息已过期，请登录！";
            JsonResult jsonResult = JsonResult.fail(ServiceCode.ERROR_UNAUTHORIZED, message);
            String jsonResultString = JSON.toJSONString(jsonResult);
            response.setContentType("application/json; charset=utf-8");
            PrintWriter printWriter = response.getWriter();
            printWriter.println(jsonResultString);
            printWriter.close();
        });

        //白名单:
        String[] url = {"/doc.html",
                "/favicon.ico", "/**/*.js", "/**/*.css", "/swagger-resources", "/v2/api-docs","/success.html","/pay/notify"};
        http.authorizeRequests()
                //不需要登陆访问
                .antMatchers(url).permitAll()
                //需要登录才能访问
                .anyRequest().authenticated();

    }
}
