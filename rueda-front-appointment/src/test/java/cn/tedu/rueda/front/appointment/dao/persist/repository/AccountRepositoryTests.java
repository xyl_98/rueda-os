package cn.tedu.rueda.front.appointment.dao.persist.repository;

import cn.tedu.rueda.front.appointment.pojo.query.PatientQuery;
import cn.tedu.rueda.front.appointment.pojo.vo.PatientStandardVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class AccountRepositoryTests {
    @Autowired
    private IPatientRepository patientRepository;

    @Test
    void get(){
        PatientQuery patientQuery = new PatientQuery().setUserId("555555222222221111");
        List patients = patientRepository.getPatient(patientQuery);
        System.out.println(patients);
    }
}
