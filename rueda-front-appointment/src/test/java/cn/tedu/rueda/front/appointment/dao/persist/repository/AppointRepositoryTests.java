package cn.tedu.rueda.front.appointment.dao.persist.repository;

import cn.tedu.rueda.front.appointment.dao.persist.mapper.AppointMapper;
import cn.tedu.rueda.front.appointment.pojo.entity.Appoint;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class AppointRepositoryTests {

    @Autowired
    AppointMapper appointMapper;

    @Test
    void insert() {
        Appoint appoint = new Appoint()
                .setUsername("张三");
        int rows = appointMapper.insert(appoint);
        System.out.println(rows);
    }
}
