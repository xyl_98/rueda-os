/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.5.20-MariaDB : Database - rueda_os
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rueda_os` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `rueda_os`;

/*Table structure for table `rueda_schedule` */

DROP TABLE IF EXISTS `rueda_schedule`;

CREATE TABLE `rueda_schedule` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(20) DEFAULT NULL COMMENT '修改人',
  `doctor_account_id` bigint(20) unsigned DEFAULT NULL COMMENT '医生ID',
  `schedule_id` tinyint(1) unsigned DEFAULT NULL COMMENT '排班时间 ,0-6,分别对应周一至周天',
  `is_leave` tinyint(1) unsigned DEFAULT 0 COMMENT '是否在班 ,0为是,1为否',
  `check_state` tinyint(1) unsigned DEFAULT 0 COMMENT '审核状态 ,0为未审核,1为审核通过,2为审核\r\n不通过',
  `check_name` varchar(20) DEFAULT NULL COMMENT '审核人',
  `check_time` datetime DEFAULT NULL COMMENT '审核时间',
  `check_description` varchar(200) DEFAULT NULL COMMENT '审核描述',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='排班表';

/*Data for the table `rueda_schedule` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
