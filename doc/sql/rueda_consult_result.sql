/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.5.20-MariaDB : Database - rueda_os
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rueda_os` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `rueda_os`;

/*Table structure for table `rueda_consult_result` */

DROP TABLE IF EXISTS `rueda_consult_result`;

CREATE TABLE `rueda_consult_result` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) unsigned DEFAULT NULL COMMENT '账户ID',
  `patient_id` bigint(20) unsigned DEFAULT NULL COMMENT '患者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_user` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(20) DEFAULT NULL COMMENT '更新人',
  `date` date DEFAULT NULL COMMENT '诊断日期',
  `username` varchar(20) DEFAULT NULL COMMENT '姓名',
  `user_id` char(18) DEFAULT NULL COMMENT '身份证号',
  `doctor_id` bigint(20) unsigned DEFAULT NULL COMMENT '医生ID',
  `doctor_name` varchar(20) DEFAULT NULL COMMENT '医生姓名',
  `department_id` bigint(20) unsigned DEFAULT NULL COMMENT '科室ID',
  `department_name` varchar(20) DEFAULT NULL COMMENT '科室名',
  `consult_result` varchar(5000) DEFAULT NULL COMMENT '诊断结果',
  `state` tinyint(1) DEFAULT 0 COMMENT '状态,0为未删除,1为已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `rueda_consult_result` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
