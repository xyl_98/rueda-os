/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.5.20-MariaDB : Database - rueda_os
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rueda_os` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `rueda_os`;

/*Table structure for table `rueda_appoint` */

DROP TABLE IF EXISTS `rueda_appoint`;

CREATE TABLE `rueda_appoint` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `account_id` bigint(20) unsigned DEFAULT NULL COMMENT '账户ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `appoint_time` datetime DEFAULT NULL COMMENT '预约时间',
  `create_user` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(20) DEFAULT NULL COMMENT '修改人',
  `username` varchar(20) DEFAULT NULL COMMENT '挂号人姓名',
  `user_id` char(18) DEFAULT NULL COMMENT '身份证号',
  `cell_phone` char(11) DEFAULT NULL COMMENT '电话号码',
  `medical_history` varchar(500) DEFAULT NULL COMMENT '过往病史',
  `doctor_name` varchar(20) DEFAULT NULL COMMENT '医生名字',
  `doctor_id` bigint(20) unsigned DEFAULT NULL COMMENT '医生ID',
  `department_name` varchar(30) DEFAULT NULL COMMENT '科室名',
  `department_id` bigint(20) unsigned DEFAULT NULL COMMENT '科室ID',
  `consult_id` bigint(20) DEFAULT NULL COMMENT '诊室ID',
  `title` varchar(5000) DEFAULT NULL COMMENT '标题',
  `pay_state` tinyint(1) unsigned DEFAULT 0 COMMENT '是否支付,0表示未支付,1表示为已支付',
  `price` decimal(10,0) unsigned DEFAULT NULL COMMENT '价格',
  `state` tinyint(1) DEFAULT 0 COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='挂号记录表';

/*Data for the table `rueda_appoint` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
