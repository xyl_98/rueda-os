/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.5.20-MariaDB : Database - rueda_os
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rueda_os` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `rueda_os`;

/*Table structure for table `rueda_doctor` */

DROP TABLE IF EXISTS `rueda_doctor`;

CREATE TABLE `rueda_doctor` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `account_id` bigint(20) unsigned DEFAULT NULL COMMENT '账户ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `name` varchar(20) DEFAULT NULL COMMENT '医生名字',
  `department_id` bigint(20) unsigned DEFAULT NULL COMMENT '所属科室ID',
  `consult_id` bigint(20) DEFAULT NULL COMMENT '诊室ID',
  `keywords` varchar(200) DEFAULT NULL COMMENT '关键词',
  `description` varchar(4000) DEFAULT NULL COMMENT '描述',
  `state` tinyint(1) unsigned DEFAULT 0 COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='医生表';

/*Data for the table `rueda_doctor` */

insert  into `rueda_doctor`(`id`,`account_id`,`create_time`,`update_time`,`name`,`department_id`,`consult_id`,`keywords`,`description`,`state`) values (1,NULL,'2023-08-03 19:25:51','2023-08-03 19:25:51','李医生',3,1,'关键词1,关键词2,关键词3','描述',0),(2,NULL,'2023-08-03 19:25:51','2023-08-03 19:25:51','王医生',5,2,'关键词1,关键词2,关键词3','描述',0),(3,NULL,'2023-08-03 19:25:51','2023-08-03 19:25:51','李医生',10,3,'关键词1,关键词2,关键词3','描述',0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
