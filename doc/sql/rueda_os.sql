/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.5.20-MariaDB : Database - rueda_os
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`rueda_os` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin */;

USE `rueda_os`;

/*Table structure for table `rueda_account` */

DROP TABLE IF EXISTS `rueda_account`;

CREATE TABLE `rueda_account` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_user` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(20) DEFAULT NULL COMMENT '更新人',
  `username` varchar(20) DEFAULT NULL COMMENT '姓名',
  `cell_phone` char(11) DEFAULT NULL COMMENT '电话号码',
  `password` varchar(128) DEFAULT NULL COMMENT '密码',
  `nick` varchar(20) DEFAULT NULL COMMENT '昵称',
  `user_id` char(18) DEFAULT NULL COMMENT '身份证号',
  `role_id` tinyint(4) unsigned DEFAULT 1 COMMENT '角色,1为普通用户,2为管理员用户(医生),3为超级管理',
  `auth_id` tinyint(4) unsigned DEFAULT 1 COMMENT '(权限码,默认为1,固定为1(普通用户),2(管理员),3(超级管理员))',
  `sculpture` varchar(5000) DEFAULT NULL COMMENT '头像',
  `gender` tinyint(1) unsigned DEFAULT 0 COMMENT '性别,0表示男.1表示女',
  `age` tinyint(3) unsigned DEFAULT NULL COMMENT '年龄',
  `state` tinyint(1) unsigned DEFAULT 0 COMMENT '状态,0表示未删除,1表示删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `rueda_account` */

insert  into `rueda_account`(`id`,`create_time`,`update_time`,`create_user`,`update_user`,`username`,`cell_phone`,`password`,`nick`,`user_id`,`role_id`,`auth_id`,`sculpture`,`gender`,`age`,`state`) values (1,'2023-08-03 19:05:50','2023-08-06 19:12:42',NULL,NULL,'小明','12233334444','123456','明明','111111111111111111',1,1,'https://rueda-os.oss-cn-chengdu.aliyuncs.com/2023/08/06/a752d4c5-7752-4136-a51e-5a48cb5f66c7.webp',0,23,0),(2,'2023-08-03 19:05:50','2023-08-03 19:05:50',NULL,NULL,'张三','13333334444','123456','三三','211111111111111111',1,1,NULL,1,23,0),(3,'2023-08-03 19:05:50','2023-08-03 19:05:50',NULL,NULL,'李四','14433334444','123456','小四','311111111111111111',1,1,NULL,0,23,0),(4,'2023-08-03 19:05:50','2023-08-03 19:05:50',NULL,NULL,'王五','15533334444','123456','小五','411111111111111111',1,1,NULL,1,23,0),(5,'2023-08-03 19:05:50','2023-08-03 19:05:50',NULL,NULL,'赵六','16633334444','123456','小六','511111111111111111',1,1,NULL,0,23,0),(6,'2023-08-03 19:05:50','2023-08-03 19:05:50',NULL,NULL,'孙七','17733334444','123456','小七','611111111111111111',1,1,NULL,1,23,0),(7,'2023-08-03 19:09:47','2023-08-03 19:09:47',NULL,NULL,'李医生','18833334444','123456','李哥','121111111111111111',2,2,NULL,0,50,0),(8,'2023-08-03 19:09:47','2023-08-03 19:09:47',NULL,NULL,'王医生','19933334444','123456','王姐','221111111111111111',2,2,NULL,1,50,0),(9,'2023-08-03 19:09:47','2023-08-03 19:09:47',NULL,NULL,'赵医生','10033334444','123456','赵哥','321111111111111111',2,2,NULL,0,50,0),(10,'2023-08-03 19:09:47','2023-08-03 19:09:47',NULL,NULL,'杨主任','11122224444','123456','老杨','421111111111111111',3,3,NULL,0,60,0),(13,'2023-08-18 15:00:45','2023-08-18 15:00:45','坤坤',NULL,'坤坤','11122223333','$2a$10$aP7evC8qBTWIMxBRUm.zv.eH3tZqYuqyp4lLcVeAE/lwCRt8YnXo.','鸡哥','555555222222221111',1,1,NULL,0,NULL,0);

/*Table structure for table `rueda_account_role` */

DROP TABLE IF EXISTS `rueda_account_role`;

CREATE TABLE `rueda_account_role` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `account_id` bigint(20) unsigned DEFAULT NULL COMMENT '用户id',
  `role_id` bigint(20) unsigned DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户权限关系表';

/*Data for the table `rueda_account_role` */

insert  into `rueda_account_role`(`Id`,`account_id`,`role_id`) values (1,13,1);

/*Table structure for table `rueda_appoint` */

DROP TABLE IF EXISTS `rueda_appoint`;

CREATE TABLE `rueda_appoint` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `account_id` bigint(20) unsigned DEFAULT NULL COMMENT '账户ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `appoint_time` datetime DEFAULT NULL COMMENT '预约时间',
  `create_user` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(20) DEFAULT NULL COMMENT '修改人',
  `username` varchar(20) DEFAULT NULL COMMENT '挂号人姓名',
  `user_id` char(18) DEFAULT NULL COMMENT '身份证号',
  `cell_phone` char(11) DEFAULT NULL COMMENT '电话号码',
  `medical_history` varchar(500) DEFAULT NULL COMMENT '过往病史',
  `doctor_name` varchar(20) DEFAULT NULL COMMENT '医生名字',
  `doctor_id` bigint(20) unsigned DEFAULT NULL COMMENT '医生ID',
  `department_name` varchar(30) DEFAULT NULL COMMENT '科室名',
  `department_id` bigint(20) unsigned DEFAULT NULL COMMENT '科室ID',
  `consult_id` bigint(20) DEFAULT NULL COMMENT '诊室ID',
  `title` varchar(5000) DEFAULT NULL COMMENT '标题',
  `pay_state` tinyint(1) unsigned DEFAULT 0 COMMENT '是否支付,0表示未支付,1表示为已支付',
  `price` decimal(10,0) unsigned DEFAULT NULL COMMENT '价格',
  `state` tinyint(1) DEFAULT 0 COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='挂号记录表';

/*Data for the table `rueda_appoint` */

/*Table structure for table `rueda_authority` */

DROP TABLE IF EXISTS `rueda_authority`;

CREATE TABLE `rueda_authority` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(20) DEFAULT NULL COMMENT '修改人',
  `name` varchar(50) DEFAULT NULL COMMENT '权限名',
  `state` tinyint(1) unsigned DEFAULT 0 COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='权限表';

/*Data for the table `rueda_authority` */

insert  into `rueda_authority`(`id`,`create_time`,`update_time`,`create_user`,`update_user`,`name`,`state`) values (1,'2023-08-03 18:56:48','2023-08-03 18:56:48',NULL,NULL,'前台登录',0),(2,'2023-08-03 18:57:04','2023-08-03 18:57:04',NULL,NULL,'前台登录,后台登陆,患者管理',0),(3,'2023-08-03 18:57:16','2023-08-03 18:57:16',NULL,NULL,'全部',0);

/*Table structure for table `rueda_consult` */

DROP TABLE IF EXISTS `rueda_consult`;

CREATE TABLE `rueda_consult` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(20) DEFAULT NULL COMMENT '修改人',
  `department_id` bigint(20) unsigned DEFAULT NULL COMMENT '所属科室',
  `address` varchar(50) DEFAULT NULL COMMENT '地址',
  `name` varchar(30) DEFAULT NULL COMMENT '诊室名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `rueda_consult` */

/*Table structure for table `rueda_consult_result` */

DROP TABLE IF EXISTS `rueda_consult_result`;

CREATE TABLE `rueda_consult_result` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) unsigned DEFAULT NULL COMMENT '账户ID',
  `patient_id` bigint(20) unsigned DEFAULT NULL COMMENT '患者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_user` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(20) DEFAULT NULL COMMENT '更新人',
  `date` date DEFAULT NULL COMMENT '诊断日期',
  `username` varchar(20) DEFAULT NULL COMMENT '姓名',
  `user_id` char(18) DEFAULT NULL COMMENT '身份证号',
  `doctor_id` bigint(20) unsigned DEFAULT NULL COMMENT '医生ID',
  `doctor_name` varchar(20) DEFAULT NULL COMMENT '医生姓名',
  `department_id` bigint(20) unsigned DEFAULT NULL COMMENT '科室ID',
  `department_name` varchar(20) DEFAULT NULL COMMENT '科室名',
  `consult_result` varchar(5000) DEFAULT NULL COMMENT '诊断结果',
  `state` tinyint(1) DEFAULT 0 COMMENT '状态,0为未删除,1为已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `rueda_consult_result` */

/*Table structure for table `rueda_department` */

DROP TABLE IF EXISTS `rueda_department`;

CREATE TABLE `rueda_department` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(30) DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(30) DEFAULT NULL COMMENT '修改人',
  `name` varchar(50) DEFAULT NULL COMMENT '科室名',
  `parent_id` bigint(20) unsigned DEFAULT NULL COMMENT '父级id',
  `level` tinyint(4) unsigned DEFAULT NULL COMMENT '层级',
  `is_parent` tinyint(1) unsigned DEFAULT 0 COMMENT '是否为父级',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `state` tinyint(1) unsigned DEFAULT 0 COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `rueda_department` */

insert  into `rueda_department`(`id`,`create_time`,`update_time`,`create_user`,`update_user`,`name`,`parent_id`,`level`,`is_parent`,`description`,`state`) values (1,'2023-08-03 19:14:07','2023-08-03 19:14:07',NULL,NULL,'内科',0,1,1,NULL,0),(2,'2023-08-03 19:14:07','2023-08-03 19:14:07',NULL,NULL,'外科',0,1,1,NULL,0),(3,'2023-08-03 19:16:02','2023-08-03 19:16:02',NULL,NULL,'呼吸内科',1,2,0,NULL,0),(4,'2023-08-03 19:16:02','2023-08-03 19:16:02',NULL,NULL,'消化内科',1,2,0,NULL,0),(5,'2023-08-03 19:16:02','2023-08-03 19:16:02',NULL,NULL,'心血管内科',1,2,0,NULL,0),(6,'2023-08-03 19:16:02','2023-08-03 19:16:02',NULL,NULL,'神经内科',1,2,0,NULL,0),(7,'2023-08-03 19:17:37','2023-08-03 19:17:37',NULL,NULL,'烧伤外科',2,2,0,NULL,0),(8,'2023-08-03 19:17:37','2023-08-03 19:17:37',NULL,NULL,'骨科',2,2,0,NULL,0),(9,'2023-08-03 19:17:37','2023-08-03 19:17:37',NULL,NULL,'肝胆外科',2,2,0,NULL,0),(10,'2023-08-03 19:17:37','2023-08-03 19:17:37',NULL,NULL,'肛肠内科',2,2,0,NULL,0);

/*Table structure for table `rueda_doctor` */

DROP TABLE IF EXISTS `rueda_doctor`;

CREATE TABLE `rueda_doctor` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `account_id` bigint(20) unsigned DEFAULT NULL COMMENT '账户ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `name` varchar(20) DEFAULT NULL COMMENT '医生名字',
  `department_id` bigint(20) unsigned DEFAULT NULL COMMENT '所属科室ID',
  `consult_id` bigint(20) DEFAULT NULL COMMENT '诊室ID',
  `keywords` varchar(200) DEFAULT NULL COMMENT '关键词',
  `description` varchar(4000) DEFAULT NULL COMMENT '描述',
  `state` tinyint(1) unsigned DEFAULT 0 COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='医生表';

/*Data for the table `rueda_doctor` */

insert  into `rueda_doctor`(`id`,`account_id`,`create_time`,`update_time`,`name`,`department_id`,`consult_id`,`keywords`,`description`,`state`) values (1,NULL,'2023-08-03 19:25:51','2023-08-03 19:25:51','李医生',3,1,'关键词1,关键词2,关键词3','描述',0),(2,NULL,'2023-08-03 19:25:51','2023-08-03 19:25:51','王医生',5,2,'关键词1,关键词2,关键词3','描述',0),(3,NULL,'2023-08-03 19:25:51','2023-08-03 19:25:51','李医生',10,3,'关键词1,关键词2,关键词3','描述',0);

/*Table structure for table `rueda_patient` */

DROP TABLE IF EXISTS `rueda_patient`;

CREATE TABLE `rueda_patient` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_user` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(20) DEFAULT NULL COMMENT '更新人',
  `name` varchar(20) DEFAULT NULL COMMENT '姓名',
  `age` int(10) unsigned zerofill DEFAULT NULL COMMENT '年龄',
  `gender` tinyint(3) unsigned DEFAULT NULL COMMENT '性别,0表示男,1表示女',
  `cell_phone` char(11) DEFAULT NULL COMMENT '电话号码',
  `consult_result` varchar(5000) DEFAULT NULL COMMENT '诊断结果',
  `description` varchar(5000) DEFAULT NULL COMMENT '描述',
  `user_id` char(18) DEFAULT NULL COMMENT '身份证号',
  `account_id` bigint(20) unsigned DEFAULT NULL COMMENT '账号ID',
  `department_id` bigint(20) unsigned DEFAULT NULL COMMENT '所属科室ID',
  `department_name` varchar(20) DEFAULT NULL COMMENT '所属科室',
  `medical_history` varchar(5000) DEFAULT NULL COMMENT '病史',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `rueda_patient` */

insert  into `rueda_patient`(`id`,`create_time`,`update_time`,`create_user`,`update_user`,`name`,`age`,`gender`,`cell_phone`,`consult_result`,`description`,`user_id`,`account_id`,`department_id`,`department_name`,`medical_history`) values (1,'2023-08-03 19:56:26','2023-08-03 19:56:26',NULL,NULL,'小明',0000000023,0,'12233334444',NULL,NULL,'111111111111111111',1,3,'呼吸内科',NULL),(2,'2023-08-03 19:56:26','2023-08-03 19:56:26',NULL,NULL,'张三',0000000023,1,'13333334444',NULL,NULL,'211111111111111111',2,4,'消化内科',NULL),(3,'2023-08-03 19:56:26','2023-08-03 19:56:26',NULL,NULL,'李四',0000000023,0,'14433334444',NULL,NULL,'311111111111111111',4,5,'心血管内科',NULL),(4,'2023-08-03 19:56:26','2023-08-03 19:56:26',NULL,NULL,'王五',0000000023,1,'15533334444',NULL,NULL,'411111111111111111',5,6,'神经内科',NULL),(5,'2023-08-03 19:56:26','2023-08-03 19:56:26',NULL,NULL,'赵六',0000000023,0,'16633334444',NULL,NULL,'511111111111111111',6,7,'烧伤外科',NULL),(6,'2023-08-03 19:56:26','2023-08-03 19:56:26',NULL,NULL,'孙七',0000000023,1,'17733334444',NULL,NULL,'611111111111111111',7,8,'骨科',NULL);

/*Table structure for table `rueda_role` */

DROP TABLE IF EXISTS `rueda_role`;

CREATE TABLE `rueda_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `name` varchar(20) DEFAULT NULL COMMENT '角色名',
  `description` varchar(200) DEFAULT NULL COMMENT '描述',
  `create_user` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(20) DEFAULT NULL COMMENT '修改人',
  `state` tinyint(1) unsigned DEFAULT 0 COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

/*Data for the table `rueda_role` */

insert  into `rueda_role`(`id`,`create_time`,`update_time`,`name`,`description`,`create_user`,`update_user`,`state`) values (1,'2023-08-03 18:55:35','2023-08-03 18:55:35','普通用户',NULL,NULL,NULL,0),(2,'2023-08-03 18:55:58','2023-08-03 18:55:58','管理员',NULL,NULL,NULL,0),(3,'2023-08-03 18:56:09','2023-08-03 18:56:09','超级管理员',NULL,NULL,NULL,0);

/*Table structure for table `rueda_role_authority` */

DROP TABLE IF EXISTS `rueda_role_authority`;

CREATE TABLE `rueda_role_authority` (
  `Id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `role_id` bigint(20) unsigned DEFAULT NULL COMMENT '角色id',
  `authority_id` bigint(20) unsigned DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='角色权限关系表';

/*Data for the table `rueda_role_authority` */

insert  into `rueda_role_authority`(`Id`,`role_id`,`authority_id`) values (1,1,1),(2,2,2),(3,3,3);

/*Table structure for table `rueda_schedule` */

DROP TABLE IF EXISTS `rueda_schedule`;

CREATE TABLE `rueda_schedule` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '编号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `create_user` varchar(20) DEFAULT NULL COMMENT '创建人',
  `update_user` varchar(20) DEFAULT NULL COMMENT '修改人',
  `doctor_account_id` bigint(20) unsigned DEFAULT NULL COMMENT '医生ID',
  `schedule_id` tinyint(1) unsigned DEFAULT NULL COMMENT '排班时间 ,0-6,分别对应周一至周天',
  `is_leave` tinyint(1) unsigned DEFAULT 0 COMMENT '是否在班 ,0为是,1为否',
  `check_state` tinyint(1) unsigned DEFAULT 0 COMMENT '审核状态 ,0为未审核,1为审核通过,2为审核\r\n不通过',
  `check_name` varchar(20) DEFAULT NULL COMMENT '审核人',
  `check_time` datetime DEFAULT NULL COMMENT '审核时间',
  `check_description` varchar(200) DEFAULT NULL COMMENT '审核描述',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='排班表';

/*Data for the table `rueda_schedule` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
