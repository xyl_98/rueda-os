瑞达医院综合业务系统-
端口号:
rueda-passport:48081
rueda-front-appointment:48181
rueda-admin-patientcare:48281
rueda-admin-doctorcare:48282
rueda-admin-departmentcare:48283

支付账号：kasfan6450@sandbox.com
密码：111111

任务分工:
@李彦震 @何建军:后台患者管理功能
@王朋 @范苏鲡:后台科室管理
@王鹏 @任文娟:后台医生管理
@杨大伟 @徐瑞洋:登录模块
@杨大伟 @任文娟:后台前端页面
@张益浩 @吴俊辉:前台前端页面

项目具体要求
# 功能细节

## 登录模块(单点登录)

### 一.登录,注册,登出等

#### 1.UserController('/passport')

##### (1)注册

@PostMapping('/reg')

handleReg()

需要创建对应实体类,所需属性见数据库表详情

前端发送信息来时用对应Param类来接收

所需属性包括但不限于cellPhone(String),password(String),username(用户真实姓名,String),gender(String),userId(身份证号,String),age(Integer),(可能包括)smsCode(验证码,Integer)

##### (2)登录

@PostMapping('/login')

handleLogin()

前端发送信息来时用对应Param类来接收

所需属性包括但不限于cellPhone(String),password(String),(可能包括)smsCode(验证码,Integer)

##### (3)登出

@PostMapping('/logout')

handleLogout()

## C端

### 一.预约挂号(pc端,移动端待定)

#### 1.AccountCtroller('/account')

##### (1)上传头像

@PostMapping("/upload")

uploadSculpture()

##### (2)个人中心

@GetMapping("/{id}")

用于个人主页显示,需创建对应VO类,

所需属性包括但不限于id(Long),nick(String),username(String),gender(Integer),sculpture(头像,String)

##### (3)更新资料

@PostMapping("/update")

属性包括但不限于nick(String),Sculpture(String)

#### 2.AppointController('/appoint')(用于前端页面显示)

##### (1)显示科室

@GetMapping('/department')

listDepartment()

需创建对应VO类,用于返回给前端

所需属性包括但不限于id(Long),parentId(Integer),level(层级,Integer),name(科室名,String),type(类别名:一般指内科和外科等,String),description(科室描述,String)

##### (2)显示医生

@GetMapping('/doctor/{departmentId}')

listDoctorByDepartmentId()

需要创建对应VO类,用于返回给前端

所需属性包括但不限于id(Long),name(String),departmentId(Long),departmentName(科室名String),consultId(Long),consultName(诊室名,String),keywords(String),description(详情描述),scheduleStart(排班开始时间,Date),scheduleEnd(排班结束时间,Date),is_leave(是否请假,Integer)

##### (3)预约

@PostMapping('/go-appoint')

handleAppoint()

需要创建对应实体类,所需属性见数据库表详情

前端发送的信息用对应Param类来接收

需要前端输入的信息有

doctorId(Long),departmentId(科室ID,Long),appointTime(预约时间,Date),consultId(诊室Id,Long),payState(支付状态,Integer)

## B端

### 一.患者管理(医生,主任)

#### 1.PatientController("/patient")

##### (1)患者建档

@PostMapping("/add-new")

addNew()

需要创建对应实体类,具体属性见数据库表字段

需要创建对应Param类来接收,所需属性包括但不限于:name(String),age(Integer),gender(Integer),userId(身份证号,String),doctorId(Long),departmentId(Long),medicalHistory(病例,String)

##### (2)显示患者具体数据

注:主任显示全部患者信息,医生则只显示自己所处科室的患者信息

@GetMapping("/{id}")

listPatient()

创建对应VO类用于返回给前端

所需属性包括但不限于id(Long),name(String),gender(Integer),cellPhone(String),userId(String),description(描述,String),departmentId(Long),medicalHistory(病例,String)

### 二.医生管理(主任)

#### 1.DoctorController("/doctor")

##### (1)添加医生

@PostMapping("/add-new")

addNewDoctor(),

需创建对应Param类来接收,

所需属性包括但不限于username(String),password(String),cellPhone(String),userId(),role(String),authCode(Integer),gender(IInteger),age(Integer)

注:医生属于管理员账号,账号为手机号.由主任直接发放并授权登录.

##### (2)查询列表

@GetMapping

listAllDoctor(),需创建对应VO类用于返回给前端,属性包括但不限于:

id(Long),name(String),authCode(Integer),departmentId(Long),isLeave(是否在班,Integer),scheduleId(哪一天在班,Integer)

##### (3)更新医生信息

@PostMapping("/update-doctor")

updateById()

需要创建对应Param类来接收,属性包括但不限于authCode(Integer),scheduleId(Integer),isLeave(Integer)

##### (4)删除功能

@PostMapping("/deleteById")

deleteById()

注:此删除为逻辑删除

### 三.科室管理(主任)

#### 1.DepartmentController("/department")

##### (1)新增科室

@PostMapping("/add-new")

addNewDepartment()

需创建实体类Department

需创建DepartmentAddNewParam类来接收前端输入的信息,属性包括但不限于parentId(父级ID,Integer),name(String),level(层级,Integer)

##### (2)删除科室

@PostMapping("/{id}")

deleteById()

##### (3)显示科室

@GetMapping("/")

listDepartment()

注:只显示出父级ID不为零的数据


