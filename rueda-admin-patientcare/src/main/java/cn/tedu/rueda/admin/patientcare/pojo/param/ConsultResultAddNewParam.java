package cn.tedu.rueda.admin.patientcare.pojo.param;



import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;


@Data
@Validated
public class ConsultResultAddNewParam {


    /**
     * 账户ID
     */
    @ApiModelProperty(value = "账户ID")
    private Long accountId;

    /**
     * 患者ID
     */
    @ApiModelProperty(value = "患者ID")
    @NotNull
    @Range(min=1,message = "请提交有效患者ID")
    private Long patientId;

    /**
     * 就诊时间
     */
    @ApiModelProperty(value = "诊断结果提交时间",required = true)
    @NotNull
    private String date;


    /**
     * 姓名
     */
    @ApiModelProperty(value = "患者姓名",required = true)
    @NotNull
    @Pattern(regexp = "^[\\u4e00-\\u9fa5]{1,10}$",message = "请填写合法姓名")
    private String username;

    /**
     * 身份证号
     */
    @ApiModelProperty(value = "身份证号",required = true)
    @NotNull
    @Pattern(regexp = "^\\d{18}$",message = "请提交合法身份证号")
    private String userId;

    /**
     * 医生ID
     */
    @ApiModelProperty(value = "医生ID",required = true)
    @NotNull
    @Range(min = 1,message = "请提交合法ID")
    private Long doctorId;

    /**
     * 医生名字
     */
    @ApiModelProperty(value = "医生姓名")
    @Pattern(regexp = "^[\\u4e00-\\u9fa5]{1,10}$",message = "请提交合法医生姓名")
    private String doctorName;

    /**
     * 科室ID
     */
    @ApiModelProperty(value = "科室ID",required = true)
    @NotNull
    @Range(min = 1,message = "请提交合法ID")
    private Long departmentId;

    /**
     * 科室名
     */
    @ApiModelProperty(value = "科室名称")
    @Pattern(regexp = "^[\\u4e00-\\u9fa5]{1,100}$",message = "科室名不得超过一百字!")
    private String departmentName;

    /**
     * 诊断结果
     */
    @ApiModelProperty(value = "诊断结果",required = true)
    @NotNull
    @Pattern(regexp = "^.{1,5000}$",message = "诊断结果不能超过5000字!")
    private String consultResult;

}
