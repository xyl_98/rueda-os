package cn.tedu.rueda.admin.patientcare.dao.cache.repository;

import cn.tedu.rueda.common.consts.cache.ConsultResultCacheConsts;
import cn.tedu.rueda.common.pojo.po.PatientcareSimplePO;

/**
 * 处理患者信息列表的缓存接口
 */
public interface IPatientcareRepository extends ConsultResultCacheConsts {

    /**
     * 缓存患者信息
     *
     * @param patientcareSimplePO
     */
    void save(PatientcareSimplePO patientcareSimplePO);

}
