package cn.tedu.rueda.admin.patientcare.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("cn.tedu.rueda.admin.patientcare.dao.persist.mapper")
public class MyBatisConfig {
}
