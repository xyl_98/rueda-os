package cn.tedu.rueda.admin.patientcare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RuedaAdminPatientcareApplication {

    public static void main(String[] args) {
        SpringApplication.run(RuedaAdminPatientcareApplication.class, args);
    }

}
