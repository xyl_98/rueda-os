package cn.tedu.rueda.admin.patientcare.pojo.param;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("rueda_patient")
public class PatientUpdateInfoParam implements Serializable {

    @ApiModelProperty("患者ID")
    @NotNull(message = "请提交患者ID")
    private Long id;

    @ApiModelProperty("录入患者信息人")
    private String create_user;

    @ApiModelProperty("修改患者信息人")
    private String update_user;

    @ApiModelProperty("患者姓名")
    private String name;

    @ApiModelProperty("患者年龄")
    private Integer age;

    @ApiModelProperty("患者性别，0表示男，1表示女")
    @Range(min = 0, max = 1, message = "患者性别值不合法")
    private Integer gender;

    @ApiModelProperty("患者电话号码")
    private String cellPhone;

    @ApiModelProperty("患者诊断结果")
    private String consultResult;

    @ApiModelProperty("患者描述")
    private String description;

    @ApiModelProperty("患者身份证号码")
    private String userId;


    @ApiModelProperty("患者所属科室ID")
    @NotNull(message = "请提交科室ID")
    @Range(min = 1, max = 10, message = "科室ID需要在1-10之间的数字")
    private Integer departmentId;

    @ApiModelProperty("患者所属科室名称")
    private String departmentName;

    @ApiModelProperty("患者过往病史")
    private String medicalHistory;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}