package cn.tedu.rueda.admin.patientcare.controller;


import cn.tedu.rueda.admin.patientcare.pojo.entity.Patient;
import cn.tedu.rueda.admin.patientcare.pojo.param.ConsultResultAddNewParam;
import cn.tedu.rueda.admin.patientcare.pojo.param.PatientAddNewParam;
import cn.tedu.rueda.admin.patientcare.pojo.param.PatientUpdateInfoParam;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientListItemVO;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientStandardVO;
import cn.tedu.rueda.admin.patientcare.service.IPatientService;
import cn.tedu.rueda.common.enumerator.ServiceCode;
import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.common.web.JsonResult;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.spring.web.json.Json;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 处理患者信息相关请求的控制器
 */


@RestController
@Slf4j
@RequestMapping("/patient")
@Api(tags = "1. 患者信息管理")
@Validated
public class PatientController {

    /**
     *自动装配 IPatientService 接口
     */
    @Autowired
    private IPatientService patientService;

    /**
     * 新增患者信息功能
     * @param patientAddNewParam
     * @return
     */
    @PostMapping("/add-new")
    @ApiOperation("新增患者")
    @ApiOperationSupport(order = 100)
    public JsonResult addNew(@Valid PatientAddNewParam patientAddNewParam){
        patientService.addNew(patientAddNewParam);
        return JsonResult.ok();
    }

    @GetMapping("")
    @ApiOperation("查询患者列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", defaultValue = "1", paramType = "query", dataType = "long"),
    })
    public JsonResult list(@Range(min = 1, message = "请提交有效的页码值！") Integer page){
        log.debug("开始处理【查询患者信息列表】的请求，页码：{}", page);
        Integer pageNum = page == null ? 1 : page;
        PageData<PatientListItemVO> pageData = patientService.list(pageNum);
        return JsonResult.ok(pageData);
    }

    @GetMapping("/list-by-department/{departmentId}/{page}")
    @ApiOperation("根据科室id查询患者列表信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "departmentId", value = "科室ID", required = true, paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "page", value = "页码", defaultValue = "1", paramType = "query", dataType = "long")
    })
    public JsonResult listByDepartmentId(@Range(message = "请提交有效的科室ID值！") @PathVariable(value = "departmentId") Long departmentId,
                                          @Range(min = 1, message = "请提交有效的页码值！") @PathVariable("page") Integer page){
        log.debug("开始处理【根据科室id查询患者列表信息】的请求，科室id：{}，页码：{}", departmentId, page);
        Integer pageNum = page == null ? 1 : page;
        PageData<PatientListItemVO> pageData = patientService.listByDepartmentId(departmentId, pageNum);
        log.debug("返回数据：" + pageData);
        return JsonResult.ok(pageData);
    }

    @PostMapping("/add-result")
    @ApiOperation("新增患者诊断结果")
    public JsonResult addNewConsultResult(@Valid ConsultResultAddNewParam consultResultAddNewParam){
        log.debug("就诊结果:{}",consultResultAddNewParam.toString());
        patientService.addNewConsultResult(consultResultAddNewParam);
        return JsonResult.ok();
    }

    @GetMapping("/list-by-name")
    @ApiOperation("根据名字查询患者列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "页码", defaultValue = "1", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "name", value = "患者姓名", required = true, paramType = "query", dataType = "String")
    })
    public JsonResult listByName(@Range(min = 1, message = "请提交有效的页码值！") Integer page,
                                 @RequestParam String name){
        log.debug("开始处理【根据名字查询患者信息列表】的请求，页码：{}，患者姓名：{}", page, name);
        Integer pageNum = page == null ? 1 : page;
        PageData<PatientListItemVO> pageData = patientService.listByName(name, pageNum);
        return JsonResult.ok(pageData);
    }

    @ApiOperation("根据USERID查询用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "String")
    })
    @GetMapping("/userId/{userId}")
    public JsonResult getStandardByUserId(@PathVariable(value = "userId") String userId) {
        log.debug("开始处理【根据USERID查询用户】的请求，参数：{}", userId);
        PatientStandardVO tag = patientService.getStandardByUserId(userId);
        return JsonResult.ok(tag);
    }

    @PostMapping("/{userId:[0-9]+}/delete")
    @ApiOperation("根据USERID删除患者")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户身份证号", required = true, dataType = "String")
    })
    public JsonResult delete(@PathVariable(value = "userId") String userId) {
        log.debug("开始处理【根据ID删除用户】的请求，用户ID: {}", userId);
        patientService.deletePatientByUserId(userId);
        return JsonResult.ok();
    }

    @ApiOperation("根据ID查询用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "用户ID", required = true, dataType = "Long")
    })
    @GetMapping("/standard/{id:[0-9]+}")
    public JsonResult getStandardById(@PathVariable @Range(min = 1, message = "获取用户详情失败，请提交合法的ID值！") Long id) {
        log.debug("开始处理【根据ID查询用户】的请求，参数：{}", id);
        PatientStandardVO tag = patientService.getStandardById(id);
        return JsonResult.ok(tag);
    }

    @PostMapping("/{id}/update")
    @ApiOperation("根据ID修改患者信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "ID", required = true,  dataType = "long")
    })
    public JsonResult update(@PathVariable Long id,@AuthenticationPrincipal(expression = "username") String username,
                             @Valid PatientUpdateInfoParam patientUpdateInfoParam) {
        log.debug("开始处理【根据ID修改患者信息】的请求，患者ID: {}", id);
        patientService.updateById(id,username,patientUpdateInfoParam); // 设置待修改的患者ID

        return JsonResult.ok();
    }
}
