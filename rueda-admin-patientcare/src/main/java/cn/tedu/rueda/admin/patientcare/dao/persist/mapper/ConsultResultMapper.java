package cn.tedu.rueda.admin.patientcare.dao.persist.mapper;

import cn.tedu.rueda.admin.patientcare.pojo.entity.ConsultResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsultResultMapper extends BaseMapper<ConsultResult> {
}
