package cn.tedu.rueda.admin.patientcare.dao.persist.mapper;

import cn.tedu.rueda.admin.patientcare.pojo.entity.Patient;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientListItemVO;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientStandardVO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 处理患者信息数据的Mapper接口
 */
@Repository
public interface PatientMapper extends BaseMapper<Patient> {

    /**
     * 根据患者姓名查询患者信息
     *
     * @param userId
     * @return
     */
    PatientStandardVO getStandardByUserId(String userId);

    /**
     * 查询所有患者信息
     * @return 患者信息列表
     */
    List<PatientListItemVO> list();

    /**
     * 根据科室id查询患者信息
     * @param departmentId 科室id
     * @return 患者信息列表
     */
    List<PatientListItemVO> listByDepartmentId(Long departmentId);

    /**
     * 根据名字查询患者信息
     * @return 患者信息列表
     */
    List<PatientListItemVO> listByName(String name);

    /**
     * 根据患者id查询患者信息
     *
     * @param id
     * @return
     */
    PatientStandardVO getStandardById(Long id);

    int insertPatient (Patient patient);

}
