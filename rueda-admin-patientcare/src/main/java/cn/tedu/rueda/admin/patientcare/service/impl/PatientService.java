package cn.tedu.rueda.admin.patientcare.service.impl;

import cn.tedu.rueda.admin.patientcare.dao.persist.repository.IPatientRepository;
import cn.tedu.rueda.admin.patientcare.pojo.entity.Patient;
import cn.tedu.rueda.admin.patientcare.pojo.param.ConsultResultAddNewParam;
import cn.tedu.rueda.admin.patientcare.pojo.param.PatientAddNewParam;
import cn.tedu.rueda.admin.patientcare.pojo.param.PatientUpdateInfoParam;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientListItemVO;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientStandardVO;
import cn.tedu.rueda.admin.patientcare.service.IPatientService;
import cn.tedu.rueda.common.enumerator.ServiceCode;
import cn.tedu.rueda.common.ex.ServiceException;
import cn.tedu.rueda.common.pojo.vo.PageData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;



/**
 * 处理患者新增信息业务实现类
 */

@Slf4j
@Service
public class PatientService implements IPatientService {
    @Value("${rueda.dao.default-query-page-size}")
    private Integer defaultQueryPageSize;
    @Autowired
    private IPatientRepository patientRepository;

    /**
     * 新增患者信息
     * @param patientAddNewParam 患者信息数据
     */
    @Override
    public void addNew(PatientAddNewParam patientAddNewParam) {
        String userId = patientAddNewParam.getUserId();
        PatientStandardVO standardById = patientRepository.getStandardByUserId(userId);
        // 判断患者信息是否存在
        if (standardById != null){
            String message = "患者信息已存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_CONFLICT, message);
        }
        // 判断科室id是否正确
        if (patientAddNewParam.getDepartmentId() == null){
            String message = "请选择正确的科室";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_FORBIDDEN, message);
        }

        // 创建实现类
        Patient patient = new Patient();
        // 把客服端传过来的数据复制给 patient
        BeanUtils.copyProperties(patientAddNewParam, patient);
        int rows = patientRepository.insertPatient(patient);
        if (rows == 1){
            log.debug("新增患者信息成功！+ " , rows);
        }

    }



    @Override
    public PageData<PatientListItemVO> list(Integer pageNum) {
        log.debug("开始处理【查询患者列表】的业务，页码：{}", pageNum);
        return patientRepository.list(pageNum, defaultQueryPageSize);
    }

    @Override
    public PageData<PatientListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始处理【查询患者列表】的业务，页码：{}, 每页记录数：{}", pageNum, pageSize);
        return patientRepository.list(pageNum, pageSize);
    }

    @Override
    public PageData<PatientListItemVO> listByDepartmentId(Long departmentId, Integer pageNum) {

        log.debug("开始处理【根据科室id查询患者列表】的业务，科室id：{}，页码：{}", departmentId, pageNum);
        return patientRepository.listByDepartmentId(departmentId, pageNum, defaultQueryPageSize);
    }

    @Override
    public PageData<PatientListItemVO> listByDepartmentId(Long departmentId, Integer pageNum, Integer pageSize) {
        log.debug("开始处理【根据科室id查询患者列表】的业务，科室id：{}，页码：{}，每页记录数：{}", departmentId, pageNum, pageSize);
        return patientRepository.listByDepartmentId(departmentId, pageNum, pageSize);
    }

    @Override
    public void addNewConsultResult(ConsultResultAddNewParam consultResultAddNewParam) {
        if (patientRepository.getStandardById(consultResultAddNewParam.getPatientId())==null){
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND,"患者不存在或已被删除!");
        }
        patientRepository.insertConsultResult(consultResultAddNewParam);
    }

    @Override
    public PageData<PatientListItemVO> listByName(String name,Integer pageNum) {
        log.debug("开始处理【查询患者列表】的业务，页码：{}", pageNum);
        return patientRepository.listByName(name,pageNum, defaultQueryPageSize);
    }

    @Override
    public PageData<PatientListItemVO> listByName(String name,Integer pageNum, Integer pageSize) {
        log.debug("开始处理【查询患者列表】的业务，页码：{}, 每页记录数：{}", pageNum, pageSize);
        return patientRepository.listByName(name,pageNum, pageSize);
    }

    @Override
    public PatientStandardVO getStandardByUserId(String userId) {
        log.debug("开始处理【根据ID查询用户】业务，参数：{}", userId);
        PatientStandardVO currentUser = patientRepository.getStandardByUserId(userId);
        if (currentUser == null) {
            String message = "获取用户详情失败，尝试访问的用户数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        return currentUser;
    }

    @Override
    public void deletePatientByUserId(String userId) {
        log.debug("开始处理【根据ID删除用户】的业务，参数：{}", userId);

        // 先根据 userId 查询要删除的用户信息
        Object queryResult = patientRepository.getStandardByUserId(userId);
        if (queryResult == null) {
            String message = "删除用户失败，尝试访问的数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }

        // 执行删除操作
        int i = patientRepository.deletePatientByUserId(userId);
        if (i==0){
            String message = "删除失败，服务器忙，请稍后再尝试！";
            throw new ServiceException(ServiceCode.ERROR_UNKNOWN,message);
        }
    }

    @Override
    public PatientStandardVO getStandardById(Long id) {
        log.debug("开始处理【根据ID查询用户】业务，参数：{}", id);
        PatientStandardVO currentUser = patientRepository.getStandardById(id);
        if (currentUser == null) {
            String message = "获取用户详情失败，尝试访问的用户数据不存在！";
            log.warn(message);
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, message);
        }
        return currentUser;
    }

    @Override
    public void updateById(Long id,String name, PatientUpdateInfoParam patientUpdateInfoParam) {
        Patient patient = new Patient();
        PatientStandardVO patientById = patientRepository.getStandardById(id);
        if (patientById == null) {
            throw new ServiceException(ServiceCode.ERROR_NOT_FOUND, "患者不存在");
        }
        BeanUtils.copyProperties(patientUpdateInfoParam, patient);
        patient.setId(id);
        patient.setUpdateUser(name);
        patientRepository.updateById(patient);
    }

}
