package cn.tedu.rueda.admin.patientcare.dao.persist.repository.impl;

import cn.tedu.rueda.admin.patientcare.dao.persist.mapper.ConsultResultMapper;
import cn.tedu.rueda.admin.patientcare.dao.persist.mapper.PatientMapper;
import cn.tedu.rueda.admin.patientcare.dao.persist.repository.IPatientRepository;
import cn.tedu.rueda.admin.patientcare.pojo.entity.ConsultResult;
import cn.tedu.rueda.admin.patientcare.pojo.entity.Patient;
import cn.tedu.rueda.admin.patientcare.pojo.param.ConsultResultAddNewParam;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientListItemVO;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientStandardVO;
import cn.tedu.rueda.common.pojo.vo.PageData;
import cn.tedu.rueda.common.util.PageInfoToPageDataConverter;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.github.classgraph.utils.WorkQueue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.util.List;

/**
 * 处理患者数据的实现类
 */

@Slf4j
@Repository
public class PatientRepository implements IPatientRepository {

    @Autowired
    PatientMapper patientMapper;

    @Autowired
    ConsultResultMapper consultResultMapper;

    /**
     * 新增患者信息
     * @param patient
     * @return
     */
    @Override
    public int insertPatient(Patient patient) {
        log.debug("开始执行【患者信息新增】的数据访问，参数：{} " + patient);
        return patientMapper.insertPatient(patient);
    }

    /**
     * 根据患者userId删除患者信息
     * @param userId
     * @return
     */
    @Override
    public int deletePatientByUserId(String userId) {
        log.debug("开始执行【根据userid删除患者信息】的数据访问，参数：{}", userId);
        QueryWrapper<Patient> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",userId);
        return patientMapper.delete(wrapper);
    }

    /**
     * 根据患者ID修改患者信息
     *
     * @param patient
     * @return
     */
    @Override
    public int updateById(Patient patient) {
        log.debug("开始执行【更新患者信息】的数据访问，参数：{}", patient);
        return patientMapper.updateById(patient);
    }

    /**
     * 根据患者身份证号查找患者信息
     * @param userId

     */
    @Override
    public PatientStandardVO getStandardByUserId(String userId) {
        return patientMapper.getStandardByUserId(userId);
    }

    @Override
    public PageData<PatientListItemVO> list(Integer pageNum, Integer pageSize) {
        log.debug("开始执行【查询患者列表】数据访问，页码：{}，每页记录数：{}", pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        // 调用Mapper接口中list方法
        List<PatientListItemVO> list = patientMapper.list();
        // 创建一个pageInfo实现类，将list传进去
        PageInfo<PatientListItemVO> pageInfo = new PageInfo<>(list);
        log.debug("--------------------------------------" + list);
        // 调用API将pageInfo传进去
        return PageInfoToPageDataConverter.convert(pageInfo);
    }

    @Override
    public PageData<PatientListItemVO> listByDepartmentId(Long departmentId, Integer pageNum, Integer pageSize) {
        log.debug("开始执行【根据科室id查询患者列表】数据访问，科室id：{}，页码：{}，每页记录数：{}", departmentId, pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        // 调用Mapper接口中listByDepartmentId方法
        List<PatientListItemVO> list = patientMapper.listByDepartmentId(departmentId);
        // 创建一个pageInfo实现类，将list传进去
        PageInfo<PatientListItemVO> pageInfo = new PageInfo<>(list);
        log.debug("---------------------------------" + list);
        // 调用API将pageInfo传进去
        return PageInfoToPageDataConverter.convert(pageInfo);
    }

    @Override
    public PageData<PatientListItemVO> listByName(String name,Integer pageNum, Integer pageSize) {
        log.debug("开始执行【根据名字查询患者列表】数据访问，页码：{}，每页记录数：{}", pageNum, pageSize);
        PageHelper.startPage(pageNum, pageSize);
        // 调用Mapper接口中list方法
        List<PatientListItemVO> list = patientMapper.listByName(name);
        // 创建一个pageInfo实现类，将list传进去
        PageInfo<PatientListItemVO> pageInfo = new PageInfo<>(list);
        log.debug("--------------------------------------" + list);
        // 调用API将pageInfo传进去
        return PageInfoToPageDataConverter.convert(pageInfo);
    }

    /**
     * 根据患者id查找患者信息
     * @param id
     * @return
     */
    @Override
    public PatientStandardVO getStandardById(Long id) {
        return patientMapper.getStandardById(id);
    }
    /**
     * 插入就诊结果
     *
     * @param consultResultAddNewParam
     * @return
     */
    @Override
    public int insertConsultResult(ConsultResultAddNewParam consultResultAddNewParam) {
        ConsultResult consultResult = new ConsultResult();
        BeanUtils.copyProperties(consultResultAddNewParam, consultResult);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime time = LocalDateTime.parse(consultResultAddNewParam.getDate(), dateTimeFormatter);
        consultResult.setDate(time);
        return consultResultMapper.insert(consultResult);
    }


}
