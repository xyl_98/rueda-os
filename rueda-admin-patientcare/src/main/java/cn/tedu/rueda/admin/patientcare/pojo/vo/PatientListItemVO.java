package cn.tedu.rueda.admin.patientcare.pojo.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 患者列表项VO：患者信息
 */
@Data
public class PatientListItemVO implements Serializable {

    /**
     * 患者id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 录入患者信息人
     */
    private String createUser;

    /**
     * 修改患者信息人
     */
    private String updateUser;

    /**
     * 患者姓名
     */
    private String name;

    /**
     * 患者年龄
     */
    private Integer age;

    /**
     * 患者性别  0表示男  1表示女
     */
    private Integer gender;

    /**
     * 患者电话号码
     */
    private String cellPhone;

    /**
     * 患者诊断结果
     */
    private String consultResult;

    /**
     * 患者描述
     */
    private  String description;

    /**
     * 患者身份证号码
     */
    private String userId;

    /**
     * 患者账号id
     */
    private Long accountId;

    /**
     * 患者所属于科室id
     */
    private Long departmentId;

    /**
     * 患者所属科室名称
     */
    private String departmentName;

    /**
     * 患者过往病史
     */
    private String medicalHistory;

    /**
     * 医生姓名
     */
//    private String doctorName;

    /**
     * 医生表关键词
     */
//    private String keywords;

    /**
     * 对医生的描述
     */
//    private String doctorDescription;

    /**
     * 医生状态
     */
//    private Integer state;

    /**
     * 创建患者信息时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm.ss")
    private Date createTime;

    /**
     * 最后修改患者信息时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm.ss")
    private Date updateTime;

}
