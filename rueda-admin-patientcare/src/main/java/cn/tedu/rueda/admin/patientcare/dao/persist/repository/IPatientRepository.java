package cn.tedu.rueda.admin.patientcare.dao.persist.repository;

import cn.tedu.rueda.admin.patientcare.pojo.entity.Patient;
import cn.tedu.rueda.admin.patientcare.pojo.param.ConsultResultAddNewParam;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientListItemVO;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientStandardVO;
import cn.tedu.rueda.common.pojo.vo.PageData;

/**
 *  处理患者数据的存储库接口
 */

public interface IPatientRepository {

    /**
     * 新增患者信息
     * @param patient
     * @return
     */
    int insertPatient(Patient patient);

    /**
     * 根据患者userId删除患者信息
     * @param userId
     * @return
     */
    int deletePatientByUserId(String userId);

    /**
     * 根据患者ID修改患者信息
     * @param patient
     * @return
     */
     int updateById(Patient patient);

    /**
     * 根据患者身份证号查找患者信息
     * @param userId
     * @return
     */
      PatientStandardVO getStandardByUserId(String userId);

    /**
     * 查询患者信息
     *
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 患者数据信息列表
     */
    PageData<PatientListItemVO> list(Integer pageNum, Integer pageSize);

    /**
     * 根据科室id查询患者信息
     *
     * @param departmentId 科室id
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 患者列表
     */
    PageData<PatientListItemVO> listByDepartmentId(Long departmentId, Integer pageNum, Integer pageSize);


    /**
     * 插入就诊结果
     * @param consultResultAddNewParam
     * @return
     */
    int insertConsultResult(ConsultResultAddNewParam consultResultAddNewParam);
    /**
     * 查询患者信息
     *
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 患者数据信息列表
     */
    PageData<PatientListItemVO> listByName(String name,Integer pageNum, Integer pageSize);

    /**
     * 根据患者id查找患者信息
     * @param id
     * @return
     */
    PatientStandardVO getStandardById(Long id);
}
