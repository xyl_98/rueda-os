package cn.tedu.rueda.admin.patientcare.dao.cache.repository.impl;

import cn.tedu.rueda.admin.patientcare.dao.cache.repository.IUserCacheRepository;
import cn.tedu.rueda.common.pojo.po.UserStatePO;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import java.time.Duration;
import java.time.LocalDateTime;

@Slf4j
@Repository
public class UserCacheRepositoryImpl implements IUserCacheRepository {
    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 用于用户验证的信息写入redis
     * @param id
     * @param userStatePO
     */
    @Override
    public void saveState(Long id, UserStatePO userStatePO) {
        ValueOperations<String,UserStatePO> opsForValue = redisTemplate.opsForValue();
        String key = KEY_PREFIX_AUTHORITY+id;
        opsForValue.set(key,userStatePO);
    }

    /**
     * redis存入短信验证码
     * @param phone
     * @param code
     */
    @Override
    public void saveVerificationCode(String phone, String code) {
        ValueOperations<String,String> opsForValue = redisTemplate.opsForValue();

        //创建验证码过期时间
        LocalDateTime start = LocalDateTime.now();
        LocalDateTime end = start.plusSeconds(60);
        Duration between = Duration.between(start, end);

        opsForValue.setIfAbsent(phone,code,between);
    }

    /**
     * 读取redis里用户的信息
     * @param id
     * @return UserStatePO
     */
    @Override
    public UserStatePO getAuthority(Long id) {
        log.debug("进入读取redis");
        ValueOperations<String,UserStatePO> opsForValue = redisTemplate.opsForValue();
        String key = KEY_PREFIX_AUTHORITY+id;
        UserStatePO userStatePO = opsForValue.get(key);
        log.debug("redis读取的数据"+userStatePO);
        return userStatePO;
    }

    /**
     * 获取redis短信验证码，并删除
     * @param phone
     * @return
     */
    @Override
    public String getSmsCode(String phone) {
        ValueOperations<String,String> opsForValue = redisTemplate.opsForValue();
        String smsCode = opsForValue.get(phone);
        log.debug("redis读取的短信验证码数据");
        redisTemplate.delete(phone);
        log.debug("redis中删除短信验证码");
        return smsCode;
    }

    /**
     * 删除redis里的当前用户的信息
     * @param id
     * @return boolean
     */
    @Override
    public boolean deleteAuthority(Long id) {
        return redisTemplate.delete(KEY_PREFIX_AUTHORITY+id);
    }
}
