package cn.tedu.rueda.admin.patientcare.startup;

import cn.tedu.rueda.admin.patientcare.service.IPatientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 患者信息列表数据的缓存预热类
 *
 */
@Slf4j
@Component
public class PatientcareRunner implements ApplicationRunner {

    @Autowired
    private IPatientService patientService;


    @Override
    public void run(ApplicationArguments args)  {
        log.debug("开始处理【患者列表数据缓存预热】，无参数");
    }
}
