package cn.tedu.rueda.admin.patientcare.config;

import com.alibaba.fastjson.JSON;
import cn.tedu.rueda.admin.patientcare.filter.JwtAuthorizationFilter;
import cn.tedu.rueda.common.enumerator.ServiceCode;
import cn.tedu.rueda.common.web.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.io.PrintWriter;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthorizationFilter jwtAuthorizationFilter;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.formLogin();
        http.cors();
        //使用无状态的session机制
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        //把自定义的jwt过滤器添加到Security的过滤器链中
        http.addFilterBefore(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class);

        //关闭跨域攻击
        http.csrf().disable();

        // 处理无认证信息却尝试访问需要通过认证的资源时的异常
        http.exceptionHandling().authenticationEntryPoint((request, response, e) -> {
            String message = "当前未登录，或登录信息已过期，请登录！";
            JsonResult jsonResult = JsonResult.fail(ServiceCode.ERROR_UNAUTHORIZED, message);
            String jsonResultString = JSON.toJSONString(jsonResult);
            response.setContentType("application/json; charset=utf-8");
            PrintWriter printWriter = response.getWriter();
            printWriter.println(jsonResultString);
            printWriter.close();
        });

        //白名单:
        String[] url = {"/doc.html", "/passport/simple-get", "/passport/login", "/passport/simple-post", "/passport/smsCode",
                "/favicon.ico", "/**/*.js", "/**/*.css", "/swagger-resources", "/v2/api-docs", "/passport/reg/patient", "/passport/login-smsCode"};
        http.authorizeRequests()
                //不需要登陆访问
                .antMatchers(url).permitAll()
                //需要登录才能访问
                .anyRequest().authenticated();

    }
}
