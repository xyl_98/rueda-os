package cn.tedu.rueda.admin.patientcare.service;

import cn.tedu.rueda.admin.patientcare.pojo.param.ConsultResultAddNewParam;
import cn.tedu.rueda.admin.patientcare.pojo.param.PatientAddNewParam;
import cn.tedu.rueda.admin.patientcare.pojo.param.PatientUpdateInfoParam;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientListItemVO;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientStandardVO;
import cn.tedu.rueda.common.pojo.vo.PageData;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 处理患者新增信息业务的接口
 */

@Transactional
public interface IPatientService {

    /**
     * 新增患者信息
     * @param patientAddNewParam 患者信息数据
     */
    void addNew(PatientAddNewParam patientAddNewParam);

    /**
     * 查询患者列表，使用默认的每页记录记录数
     * @param pageNum 页码
     * @return 患者列表
     */
    PageData<PatientListItemVO> list(Integer pageNum);

    /**
     * 查询患者列表信息
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 患者信息列表
     */
    PageData<PatientListItemVO> list(Integer pageNum, Integer pageSize);

    /**
     * 根据科室id查询患者信息列表， 将使用默认的每页记录数据
     * @param departmentId 科室id
     * @param pageNum 页码
     * @return 患者信息
     */
    PageData<PatientListItemVO> listByDepartmentId(Long departmentId, Integer pageNum);

    /**
     * 根据科室id查询患者信息列表
     *
     * @param departmentId 科室id
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 患者信息
     */
    PageData<PatientListItemVO> listByDepartmentId(Long departmentId, Integer pageNum, Integer pageSize);

    /**
     * 新增诊断结果
     * @param consultResultAddNewParam
     * @return
     */
    void addNewConsultResult(ConsultResultAddNewParam consultResultAddNewParam);

    /**
     * 查询患者列表，使用默认的每页记录记录数
     * @param pageNum 页码
     * @return 患者列表
     */
    PageData<PatientListItemVO> listByName(String name,Integer pageNum);

    /**
     * 查询患者列表信息
     * @param pageNum 页码
     * @param pageSize 每页记录数
     * @return 患者信息列表
     */
    PageData<PatientListItemVO> listByName(String name,Integer pageNum, Integer pageSize);

    /**
     * 根据ID查询用户
     *
     * @param userId 用户身份证
     * @return 匹配的用户信息
     */
    PatientStandardVO getStandardByUserId(String userId);

    /**
     * 删除用户
     *
     * @param userId 用户身份证号
     */
    void deletePatientByUserId(String userId);

    /**
     * 根据ID查询用户
     *
     * @param id 用户身份证
     * @return 匹配的用户信息
     */
    PatientStandardVO getStandardById(Long id);

    void updateById(Long id,String name,PatientUpdateInfoParam patientUpdateInfoParam);
}
