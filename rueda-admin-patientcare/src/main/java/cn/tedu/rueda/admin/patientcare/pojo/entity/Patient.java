package cn.tedu.rueda.admin.patientcare.pojo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;


import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("rueda_patient")
public class Patient implements Serializable {
    /**
     * 患者id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 录入患者信息人
     */
    private String createUser;

    /**
     * 修改患者信息人
     */
    private String updateUser;

    /**
     * 患者姓名
     */
    private String name;

    /**
     * 患者年龄
     */
    private Integer age;

    /**
     * 患者性别  0表示男  1表示女
     */
    private Integer gender;

    /**
     * 患者电话号码
     */
    private String cellPhone;

    /**
     * 患者诊断结果
     */
    private String consultResult;

    /**
     * 患者描述
     */
    private  String description;

    /**
     * 患者身份证号码
     */
    private String userId;

    /**
     * 患者账号id
     */
    private Integer accountId;

    /**
     * 患者所属于科室id
     */
    private Integer departmentId;

    /**
     * 患者所属科室名称
     */
    private String departmentName;

    /**
     * 患者过往病史
     */
    private String medicalHistory;

    /**
     * 创建患者信息时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 修改患者信息时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
