package cn.tedu.rueda.admin.patientcare.patient;

import cn.tedu.rueda.admin.patientcare.dao.persist.repository.IPatientRepository;
import cn.tedu.rueda.admin.patientcare.pojo.entity.Patient;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientListItemVO;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientStandardVO;
import cn.tedu.rueda.common.pojo.vo.PageData;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class PatientTests {

    @Autowired
    IPatientRepository repository;

    @Test
    void deletePatientByUserId(){
        String userId = "12367890";
        int rows = repository.deletePatientByUserId(userId);
        System.out.println("删除成功！" + rows);
    }

    @Test
    void updateById(){
        Patient patient = new Patient();
        patient.setId(1L);
        patient.setName("小明");
        int rows = repository.updateById(patient);
        System.out.println("修盖患者信息成功！" + rows);
    }

    @Test
    void getStandardByUserId(){
        String user_id = "511111111111111111";
        PatientStandardVO result = repository.getStandardByUserId(user_id);
        System.out.println("查询成功！" + result);
    }

    @Test
    void list(){
        PageData<PatientListItemVO> list = repository.list(2, 4);
        System.out.println(list);
    }

    @Test
    void xxx(){
        PageData<PatientListItemVO> patientListItemVOPageData = repository.listByDepartmentId(3L, 1, 4);
        System.out.println(patientListItemVOPageData);
    }

    @Test
    void listByName(){
        PageData<PatientListItemVO> list = repository.listByName("小明",1,5);
        System.out.println(list);
    }
    @Test
    void getStandardById(){
        Long id = 1L;
        PatientStandardVO result = repository.getStandardById(id);
        System.out.println("查询成功！" + result);
    }
}
