package cn.tedu.rueda.admin.patientcare.service;

import cn.tedu.rueda.admin.patientcare.pojo.param.PatientAddNewParam;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientListItemVO;
import cn.tedu.rueda.common.ex.ServiceException;
import cn.tedu.rueda.common.pojo.vo.PageData;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class PatientTests {

    @Autowired
    IPatientService patientService;

    @Test
    void addNew() {
        PatientAddNewParam patientAddNewParam = new PatientAddNewParam();
        patientAddNewParam.setName("王五");
        patientAddNewParam.setAccountId(0);
        patientAddNewParam.setConsultResult("12356");
        patientAddNewParam.setDescription("w3tyu");
        patientAddNewParam.setCellPhone("12345678123");
        patientAddNewParam.setDepartmentName("哈哈");
        patientAddNewParam.setMedicalHistory("fghjkl");
        patientAddNewParam.setUserId("1236789");
        patientAddNewParam.setAge(0);
        patientAddNewParam.setGender(0);
        patientAddNewParam.setDepartmentId(3);
        try{
            patientService.addNew(patientAddNewParam);
            System.out.println("新增患者，成功！");
        } catch (ServiceException e){
            System.out.println(e.getMessage());
        }
    }

    @Test
    void list(){
        PageData<PatientListItemVO> list = patientService.list(3, 1);
        System.out.println(list);
    }

    @Test
    void listByDepartmentId(){
        PageData<PatientListItemVO> patientListItemVOPageData = patientService.listByDepartmentId(3L, 2);
        System.out.println(patientListItemVOPageData);
    }

    @Test
    void ssss(){
        PageData<PatientListItemVO> aa = patientService.listByName("患者一号",1);
        System.out.println(aa);
    }

}
