package cn.tedu.rueda.admin.patientcare;

import cn.tedu.rueda.admin.patientcare.dao.persist.mapper.PatientMapper;
import cn.tedu.rueda.admin.patientcare.pojo.vo.PatientListItemVO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class RuedaAdminPatientcareApplicationTests {

    @Autowired
    PatientMapper patientMapper;

    @Test
    void contextLoads() {
    }

    @Test
    void list(){
        List<PatientListItemVO> list = patientMapper.list();
        System.out.println(list);
    }

    @Test
    void listByDepartmentId(){
        List<PatientListItemVO> patientListItemVOS = patientMapper.listByDepartmentId(2L);
        System.out.println(patientListItemVOS);
    }
    @Test
    void listByName(){
        List<PatientListItemVO> list = patientMapper.listByName("小张");
        System.out.println(list);
    }
}
